#!/bin/sh
#############################################################################
#
# Licensed Materials - Property of IBM
#
# "Restricted Materials of IBM" 
#
# (C) COPYRIGHT IBM Corp. 1993, 2019 All Rights Reserved.
#
# US Government Users Restricted Rights - Use, duplication or
# disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
#
#############################################################################


#

# Current DB2 installation directory
RUNDIR=`dirname $0`
cd ${RUNDIR?}
RUNDIR=`/bin/pwd`
cd ${RUNDIR?}/../../
RUNDIR=`/bin/pwd`
DB2DIR=${RUNDIR?}

# Moved to client_install: w971839swp
#
#  rm -f ${DB2DIR?}/lib64/libca_api.so     
#  rm -f ${DB2DIR?}/lib64/libcatrace.so
#

# On AIX, libdb2blu_ns.a/libdb2blu_s.a gets renamed to libdb2blu.a during instance creation time 
# hence will not  have entry in .metadata, we need to remove forcefully

exit 0
