********************************************************************/
*                                                                  */
* Program name: amtsamp                                            */
*                                                                  */
* Description: Sample MQSC source defining MQSeries queues for AMI */
*                                                                  */
********************************************************************/
*  @START_COPYRIGHT@                                               */
*                                                                  */
*  Statement:     Licensed Materials - Property of IBM             */
*                                                                  */
*                 (C) Copyright IBM Corporation. 1999, 2001        */
*                                                                  */
*  @END_COPYRIGHT@                                                 */
********************************************************************/
* Function:                                                        */
*                                                                  */
*                                                                  */
*   amtsamp is a sample MQSC file to create or reset the           */
*   MQSeries queues required by the AMI samples.                   */
*                                                                  */
*                                                                  */
*   This file, or a similar one, can be processed when the MQM     */
*   is started - it creates the objects if missing, or resets      */
*   their attributes to the prescribed values.                     */
*                                                                  */
*   This file should be modified to replace the definitions        */
*   below with those required by the installation where it         */
*   will be used.                                                  */
*                                                                  */
*   To enable these definitions to operate locally with the        */
*   sample programs provided, sender services are defined to       */
*   have queues of type QLOCAL (rather than QREMOTE) and all       */
*   queue are defined with both PUT and GET ENABLED.               */
*                                                                  */
*                                                                  */
*   e.g.                                                           */
*   runmqsc qmgrname < amtsamp.tst                                 */
*                                                                  */
********************************************************************/

********************************************************************/
*                                                                  */
*       Sender/receiver service queue for AMT.SAMPLE.SENDER,       */
*       AMT.SAMPLE.RECEIVER, AMT.SAMPLE.REQUEST.SENDER  and        */
*       AMT.SAMPLE.REQUEST.RECEIVER                                */
*                                                                  */
********************************************************************/

    DEFINE QLOCAL('AMT.SAMPLE.REQUEST.QUEUE') REPLACE +

*   Queue description
           DESCR('Input-Output')   +
*   Put operations allowed
           PUT(ENABLED)     +
*   Default priority
           DEFPRTY(0)       +
*   Default non-persistent
           DEFPSIST(NO)     +
*   Get operations allowed
           GET(ENABLED)


********************************************************************/
*                                                                  */
*       Receiver service queue for AMT.SAMPLE.RESPONSE.RECEIVER    */
*                                                                  */
********************************************************************/

    DEFINE QLOCAL('AMT.SAMPLE.RESPONSE.QUEUE') REPLACE     +

*   Queue description
           DESCR('Input-Output for receiving responses')   +
*   Put operations allowed
           PUT(ENABLED)     +
*   Default priority
           DEFPRTY(0)       +
*   Default non-persistent
           DEFPSIST(NO)     +
*   Get operations allowed
           GET(ENABLED)


********************************************************************/
*                                                                  */
*       Receiver service queue for AMT.SAMPLE.SUBSCRIBER.RECEIVER  */
*                                                                  */
********************************************************************/

    DEFINE QLOCAL('AMT.SAMPLE.SUBSCRIBER.RECEIVER.QUEUE') REPLACE  +

*   Queue description
           DESCR('For receiving publications')                     +
*   Put operations allowed
           PUT(ENABLED)     +
*   Default priority
           DEFPRTY(0)       +
*   Default non-persistent
           DEFPSIST(NO)     +
*   Get operations allowed
           GET(ENABLED)

********************************************************************/
*                                                                  */
*       Sender/receiver service queue for                          */
*       AMT.SAMPLE.SIMULATED.GROUP.SENDER   and                    */
*       AMT.SAMPLE.SIMULATED.GROUP.RECEIVER.                       */
*                                                                  */
********************************************************************/

    DEFINE QLOCAL('AMT.SAMPLE.SIMULATED.GROUP.QUEUE') REPLACE +

*   Queue description
           DESCR('Simulated Group Messages Input-Output')   +
*   Put operations allowed
           PUT(ENABLED)     +
*   Default priority
           DEFPRTY(0)       +
*   Default non-persistent
           DEFPSIST(NO)     +
*   Get operations allowed
           GET(ENABLED)

************** End of amtsamp.tst *********************************/

