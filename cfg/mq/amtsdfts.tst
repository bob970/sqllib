********************************************************************/
*                                                                  */
* Program name: amtsdfts                                           */
*                                                                  */
* Description: Sample MQSC source defining MQSeries queues for AMI */
*                                                                  */
********************************************************************/
*  @START_COPYRIGHT@                                               */
*                                                                  */
*  Statement:     Licensed Materials - Property of IBM             */
*                                                                  */
*                 (C) Copyright IBM Corporation. 1999, 2000        */
*                                                                  */
*  @END_COPYRIGHT@                                                 */
********************************************************************/
*                                                                  */
* Function:                                                        */
*                                                                  */
*   amtsdfts is a sample MQSC file to create or reset the          */
*   MQSeries queues required by the AMI system default objects.    */
*                                                                  */
*                                                                  */
*   This file, or a similar one, can be processed when the MQM     */
*   is started - it creates the objects if missing, or resets      */
*   their attributes to the prescribed values.                     */
*                                                                  */
*   This file should be modified to replace the definitions        */
*   below with those required by the installation where it         */
*   will be used.                                                  */
*                                                                  */
*   To enable these definitions to operate locally with the        */
*   sample programs provided, sender services are defined to       */
*   have queues of type QLOCAL (rather than QREMOTE) and all       */
*   queue are defined with both PUT and GET ENABLED.               */
*                                                                  */
*                                                                  */
*   e.g.                                                           */
*   runmqsc qmgrname <amqsdfts.tst                                 */
*                                                                  */
********************************************************************/

********************************************************************/
*                                                                  */
*       Receiver service queue for SYSTEM.DEFAULT.RECEIVER         */
*                                                                  */
********************************************************************/

    DEFINE QLOCAL('SYSTEM.DEFAULT.RECEIVER') REPLACE +

*   Queue description
           DESCR('Input-Output')   +
*   Put operations allowed
           PUT(ENABLED)     +
*   Default priority
           DEFPRTY(0)       +
*   Default non-persistent
           DEFPSIST(NO)     +
*   Get operations allowed
           GET(ENABLED)

********************************************************************/
*                                                                  */
*       Sender service queue for SYSTEM.DEFAULT.SENDER             */
*                                                                  */
********************************************************************/

    DEFINE QALIAS('SYSTEM.DEFAULT.SENDER') REPLACE +

*   Destination queue
           TARGQ('SYSTEM.DEFAULT.RECEIVER')   +
*   Queue description
           DESCR('Input-Output')   +
*   Put operations allowed
           PUT(ENABLED)     +
*   Default priority
           DEFPRTY(0)       +
*   Default non-persistent
           DEFPSIST(NO)     +
*   Get operations allowed
           GET(ENABLED)

********************************************************************/
*                                                                  */
*       Sender service queue for SYSTEM.DEFAULT.PUBLISHER          */
*                                                                  */
********************************************************************/

    DEFINE QLOCAL('SYSTEM.DEFAULT.PUBLISHER') REPLACE +

*   Queue description
           DESCR('Input-Output')   +
*   Put operations allowed
           PUT(ENABLED)     +
*   Default priority
           DEFPRTY(0)       +
*   Default non-persistent
           DEFPSIST(NO)     +
*   Get operations allowed
           GET(ENABLED)

********************************************************************/
*                                                                  */
*       Sender service queue for SYSTEM.DEFAULT.SUBSCRIBER         */
*                                                                  */
********************************************************************/

    DEFINE QLOCAL('SYSTEM.DEFAULT.SUBSCRIBER') REPLACE +

*   Queue description
           DESCR('Input-Output')   +
*   Put operations allowed
           PUT(ENABLED)     +
*   Default priority
           DEFPRTY(0)       +
*   Default non-persistent
           DEFPSIST(NO)     +
*   Get operations allowed
           GET(ENABLED)

********************************************************************/
*                                                                  */
*       Receiver service queue for SYSTEM.DEFAULT.SUBSCRIBER       */
*                                                                  */
********************************************************************/

    DEFINE QLOCAL('SYSTEM.DEFAULT.SUBSCRIBER.RECEIVER') REPLACE +

*   Queue description
           DESCR('Input-Output')   +
*   Put operations allowed
           PUT(ENABLED)     +
*   Default priority
           DEFPRTY(0)       +
*   Default non-persistent
           DEFPSIST(NO)     +
*   Get operations allowed
           GET(ENABLED)

************** End of amtsdfts.tst *********************************/
