-- **************************************************************************************************************************
-- *
-- *  Licensed Materials - Property of IBM
-- *  COPYRIGHT:  P#2 P#1
-- *             (C) COPYRIGHT IBM CORPORATION Y1, Y2. All Right Reserved
-- *  US Government Users Restricted Rights - Use, duplication or
-- *  disclosure restricted by IBM Corporation
-- *
-- *  The source code for this program is not published or otherwise divested of
-- *  its trade secrets, irrespective of what has been deposited with the U.S.
-- *  Copyright Office.
-- **************************************************************************************************************************
-----------------------------------------------------------------------------------------------------------------------------
-- This is a new SQL script file that can be used to set up the new DB2 MQ/AMI configuration objects.
-- The content of this file will be documented in the DB2 User/Configuration Guide by the ID team.
--
-- Warning:
--
--    Run this script against your database only if you are setting up/configuring your database for MQ UDFs the first time,
--    Otherwise, you may need to update and adjust accordingly if you already have these database objects in your database;
--
--    Do not edit/update this script that will cause the table struture being modified.  That will cause the program crash.
--
-- To run/use the script:
--
-- 	First, connect to the target database;
--      Then, issue 'db2 -tvf amtsetup.sql'
-----------------------------------------------------------------------------------------------------------------------------

-- schema pre-selected for AMI configuration related objects (do not change this !!!)
--
set schema db2mq;

-- AMI Service Points Table (MQService):
-- This table hosts various types of Service points entries and their attributes. Most attributes have default values if not entered. 

drop table MQService;

--
-- No. of columns (5 + 2 reserved)
--
--
create table MQService (
serviceName varchar(48) not null, 
-- the following is kept in the table but is not currently supported 
serviceType varchar(48) CHECK (serviceType IN ('Native','MQSeries_Integrator_V2','MQSeries_Integrator_V1','MQRFH2','MQRFH')) default 'Native' NOT NULL,
queueName varchar(48) NOT NULL, 
queueMgrName varchar(48) default '' NOT NULL,
-- the following is kept in the table but is not currently supported
defaultFormat varchar(8) default '' NOT NULL,
ccsid varchar(6) default '' NOT NULL, 
Description varchar(400) default '',
primary key(serviceName) 
);

--	
-- The following SQLs initialize the tables with pre-defined Service Points (notice that most of the columns are using default) for default configuration
--
insert into MQService (ServiceName, queueName, queueMgrName, Description) values ('DB2.DEFAULT.SERVICE',   'DB2MQ_DEFAULT_Q', 'DB2MQ_DEFAULT_MQM', 'DB2 MQ UDFs default service');
insert into MQService (ServiceName, queueName, queueMgrName) values ('AMT.SAMPLE.SERVICE',   'AMT.SAMPLE.REQUEST.QUEUE', ' ');

insert into MQService (ServiceName, queueName, Description) values ('DB2.DEFAULT.PUBLISHER',  'SYSTEM.BROKER.DEFAULT.STREAM', 'DB2 MQ UDFs default Publisher Service');
insert into MQService (ServiceName, queueName) values ('AMT.SAMPLE.PUBLISHER',    'SYSTEM.BROKER.DEFAULT.STREAM');
insert into MQService (ServiceName, queueName) values ('AMT.SAMPLE.SUBSCRIBER',  'SYSTEM.BROKER.CONTROL.QUEUE');
insert into MQService (ServiceName, queueName) values ('AMT.SAMPLE.SUBSCRIBER.RECEIVER',   'AMT.SAMPLE.SUBSCRIBER.RECEIVER.QUEUE');
insert into MQService (ServiceName, queueName) values ('AMT.SAMPLE.RESPONSE.RECEIVER',   'AMT.SAMPLE.RESPONSE.QUEUE');


-- AMI Pub/Sub Service Table (MQPubSub):
-- This table is used to define the Publisher/Subscriber Service Points used in MQ Publish/Subscribe Functions.
--

drop table MQPubSub;

create table MQPubSub (
PubSubName varchar(48) not null unique,
broker varchar(48) not null,
receiver varchar(48) default '' NOT NULL,
-- 'P' for Publisher, 'S' for Subscriber
type char(1) CHECK (type IN ('P', 'S')) NOT NULL,
description varchar(400) default '',
constraint fk_MQService foreign key (broker) references MQService (ServiceName) on delete restrict,
constraint ck_receiver check (receiver is not null OR type = 'P')
);

--
-- The following SQLs initialize the tables with pre-defined Publisher/Subscriber Service Points for default configuration
--
insert into MQPubSub (PubSubName, broker, receiver, type, description) values ('DB2.DEFAULT.SUBSCRIBER', 'AMT.SAMPLE.SUBSCRIBER', 'AMT.SAMPLE.SUBSCRIBER.RECEIVER', 'S', 'DB2 MQPublish default Publisher service');
insert into MQPubSub (PubSubName, broker, receiver, type) values ('AMT.SAMPLE.SUBSCRIBER', 'AMT.SAMPLE.SUBSCRIBER', 'AMT.SAMPLE.SUBSCRIBER.RECEIVER',  'S');

--
--insert into MQPubSub values ('AMT.SAMPLE.SUBSCRIBER', 'AMT.SAMPLE.SUBSCRIBER', 'AMT.SAMPLE.SUBSCRIBER.RECEIVER',  'S', NULL, '');
--insert into MQPubSub values ('AMT.SAMPLE.PUBLISHER',  'AMT.SAMPLE.PUBLISHER', '', 'P', '', '');

insert into MQPubSub (PubSubName, broker, receiver, type, description) values ('DB2.DEFAULT.PUBLISHER', 'AMT.SAMPLE.PUBLISHER', 'NA', 'P', 'DB2 MQSubscribe default Subscriber service');
insert into MQPubSub (PubSubName, broker, receiver, type) values ('AMT.SAMPLE.PUBLISHER',  'AMT.SAMPLE.PUBLISHER', 'NA', 'P');


-- AMI Policies Table (MQPolicy):
-- This table defines various AMI policy entries.  The columns are grouped according to function for easier update.
--

drop table MQPolicy;

create table MQPolicy (
policyName varchar(48) not null, 
connectionName varchar(48) default '' NOT NULL, 
-- name above must be NOT NULL only if mode below is 'Logic' (see constraint at the end of this statement)
-- 'R' for Real, 'L' for Logical
connectionMode char(1) CHECK (connectionMode IN ('R', 'L')) default 'L' NOT NULL,

-- send (13 from 20)
-- 'T' for 'AsTransport' below
snd_priority char(1) check (snd_priority IN ('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'T')) default 'T' NOT NULL,
snd_persistent char(1) check (snd_persistent IN ('Y', 'N', 'T')) default 'T' NOT NULL,
-- number 0 to indicate 'Unlimited'
snd_expiry integer check (snd_expiry >= 0) default 0  NOT NULL,
--
--
-- Attention:  testing binding with C type for Integer column now!!!
snd_retrycount integer check (snd_retrycount >= 0) default 0 NOT NULL,
--
snd_retry_interval integer check (snd_retry_interval >= 0 ) default 1000 NOT NULL,

snd_newCorrelID char(1) check (snd_newCorrelID IN ('Y', 'N')) default 'N' NOT NULL,
-- 'M' for 'MessageID' and 'C' for CorrelID'
snd_responseCorrelID char(1) check (snd_responseCorrelID IN ('M', 'C')) default 'M' NOT NULL,
-- 'Q' for 'DLQ' and 'D' for 'Discard'
snd_exceptionAction char(1) check (snd_exceptionAction IN ('Q', 'D')) default 'D' NOT NULL,
-- 'R' for 'Report', 'D' for 'Report_With_Data',  and 'F' for 'Report_With_Full_Data'
snd_reportData char(1) check (snd_reportData IN ('R', 'D', 'F')) default 'R' NOT NULL,
snd_rtException char(1) check (snd_rtException IN ('Y', 'N')) default 'N' NOT NULL,
snd_rtCOA char(1) check (snd_rtCOA IN ('Y', 'N')) default 'N' NOT NULL,
snd_rtCOD char(1) check (snd_rtCOD IN ('Y', 'N')) default 'N' NOT NULL,
snd_rtExpiry char(1) check (snd_rtExpiry IN ('Y', 'N')) default 'N' NOT NULL,

-- receive (5 from 11)
-- a value of -1 indicates 'Unlimited' here
rcv_waitInterval integer check (rcv_waitInterval >= -1 ) default 30 NOT NULL,
rcv_convert char(1) check (rcv_convert IN ('Y', 'N')) default 'Y' NOT NULL,
rcv_handlePoisonMsg char(1) check (rcv_handlePoisonMsg IN ('Y', 'N')) default 'Y' NOT NULL,
rcv_rcvTruncatedMsg char(1) check (rcv_rcvTruncatedMsg IN ('Y', 'N')) default 'N' NOT NULL,
rcv_openShared  char(1) check (rcv_openShared IN ('Y', 'N')) default 'Y' NOT NULL,

-- publish (7)
pub_retain  char(1) check (pub_retain IN ('Y', 'N')) default 'N' NOT NULL,
pub_othersOnly char(1) check (pub_othersOnly IN ('Y', 'N')) default 'N' NOT NULL,
pub_suppressReg char(1) check (pub_suppressReg IN ('Y', 'N')) default 'Y' NOT NULL,
pub_pubLocal char(1) check (pub_pubLocal IN ('Y', 'N')) default 'N' NOT NULL,
pub_direct char(1) check (pub_direct IN ('Y', 'N')) default 'N' NOT NULL, 
pub_anonymous char(1) check (pub_anonymous IN ('Y', 'N')) default 'N' NOT NULL,
pub_correlasID char(1) check (pub_correlasID IN ('Y', 'N')) default 'N' NOT NULL,

-- subscribe (7 from 9)
sub_subLocal char(1) check (sub_subLocal IN ('Y', 'N')) default 'N' NOT NULL,
sub_NewPubsOnly char(1) check (sub_NewPubsOnly IN ('Y', 'N')) default 'N' NOT NULL,
sub_PubOnReqOnly char(1) check (sub_PubOnReqOnly IN ('Y', 'N')) default 'N' NOT NULL,
sub_informIfRet char(1) check (sub_informIfRet IN ('Y', 'N')) default 'Y' NOT NULL,
sub_unsubAll char(1) check (sub_unsubAll IN ('Y', 'N')) default 'N' NOT NULL,
sub_anonymous char(1) check (sub_anonymous IN ('Y', 'N')) default 'N' NOT NULL,
sub_correlAsID char(1) check (sub_correlAsID IN ('Y', 'N')) default 'N' NOT NULL,

-- others (not including Policy Handler features)
Description varchar(400) default '',
constraint cross_edit_conn check (connectionName <> '' OR connectionMode = 'R'),
primary key (policyName)
);

--
-- The following SQLs initialize the tables with pre-defined AMI Policies (notice that most of the columns are using default) for default configuration
--
insert into MQPolicy (policyName, connectionName, Description) values ('AMT.SAMPLE.POLICY', 'defaultConnection', 'AMT Sample Programs Default Policy');
insert into MQPolicy (policyName, connectionName, Description) values ('AMT.SAMPLE.PUB.SUB.POLICY', 'defaultConnection', 'AMT PUB/SUB Sample');
insert into MQPolicy (policyName, connectionName, Description) values ('DB2.DEFAULT.POLICY', 'db2mqConnection', 'DB2 UDF Default Connection');

-- the following is for my test purpose only (for using the 'Real' vs. 'Logical' connection mode
insert into MQPolicy (policyName, connectionName, connectionMode, Description) values ('MYT.TEST.POLICY', 'qm', 'R', 'Testing Real Connection Mode');


-- AMI Host Table (AMTHost):
-- This table is used to replaced the previously amthost.xml file from the original AMI product extention

drop table MQHost;

-- queueMgr cannot be null (to avoid using the null indicator in SQLColBind().  Use '' to indicate default queue manager
create table MQHost (connectionName varchar(48) not null unique, queueMgrName varchar(48) not null default '');

--
-- The following SQLs initialize the tables with pre-defined entries for default configurations 
--
insert into MQHost values ('defaultConnection', 'DB2MQ_DEFAULT_MQM');
insert into MQHost values ('db2mqConnection', 'DB2MQ_DEFAULT_MQM');

---------------------------------------------------------------------------------------------------------------------------------------------------
-- the following table and triggers are created so that we can have a record of timestamp to find out the latest update to any of MQ tables above
-- (since DB2 does not have similar last update timestamp readily available for us to use)
--
--  These are used in all versions of MQ UDFs
---------------------------------------------------------------------------------------------------------------------------------------------------

drop table MQUpdated;

create table MQUpdated (update_time timestamp);
insert into MQUpdated values (current timestamp);

drop trigger SPInsert;
drop trigger SPDelete;
drop trigger SPUpdate;

CREATE TRIGGER SPInsert AFTER INSERT ON db2mq.MQService FOR EACH ROW MODE DB2SQL UPDATE MQUpdated SET update_time = current timestamp;
CREATE TRIGGER SPDelete AFTER DELETE ON db2mq.MQService FOR EACH ROW MODE DB2SQL UPDATE MQUpdated SET update_time = current timestamp;
CREATE TRIGGER SPUpdate AFTER UPDATE ON db2mq.MQService FOR EACH ROW MODE DB2SQL UPDATE MQUpdated SET update_time = current timestamp;


drop trigger PSubInsert;
drop trigger PSubDelete;
drop trigger PSubUpdate;

CREATE TRIGGER PSubInsert AFTER INSERT ON db2mq.MQPubSub FOR EACH ROW MODE DB2SQL UPDATE MQUpdated SET update_time = current timestamp;
CREATE TRIGGER PSubDelete AFTER DELETE ON db2mq.MQPubSub FOR EACH ROW MODE DB2SQL UPDATE MQUpdated SET update_time = current timestamp;
CREATE TRIGGER PSubUpdate AFTER UPDATE ON db2mq.MQPubSub FOR EACH ROW MODE DB2SQL UPDATE MQUpdated SET update_time = current timestamp;

drop trigger PolInsert;
drop trigger PolDelete;
drop trigger PolUpdate;

CREATE TRIGGER PolInsert AFTER INSERT ON db2mq.MQPolicy FOR EACH ROW MODE DB2SQL UPDATE MQUpdated SET update_time = current timestamp;
CREATE TRIGGER PolDelete AFTER DELETE ON db2mq.MQPolicy FOR EACH ROW MODE DB2SQL UPDATE MQUpdated SET update_time = current timestamp;
CREATE TRIGGER PolUpdate AFTER UPDATE ON db2mq.MQPolicy FOR EACH ROW MODE DB2SQL UPDATE MQUpdated SET update_time = current timestamp;

-- In order to support the message size of 32,000 through SQL stored procedure (as a workaround), the following table space/buffer pool may need to 
-- be created (i.e. page size of 32 K, vs. the default 4K) using the following SQL statement or similar (notice that the container must be empty
-- when the statement is issued)
--create bufferpool bf1 size 1000 pagesize 32k;
-- The following create statement can be re-issued after the container directory is deleted
--create temporary tablespace ts2 pagesize 32 k managed by system using ('/home2/tongm/mytblspace') bufferpool bf1;


COMMIT;
