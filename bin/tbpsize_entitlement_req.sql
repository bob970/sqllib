----------------------------------------------------------------------------
-- @copyright(external)
-- Licensed Materials - Property of IBM
-- 5724-E34
--
-- (c) Copyright IBM Corp.  2011.   All Rights Reserved.
-- US Government Users Restricted Rights
-- Use, duplication or disclosure restricted by GSA ADP Schedule
-- Contract with IBM Corporation.
--
-- IBM grants you ("Licensee") a non-exclusive, royalty free, license to use,
-- copy and redistribute the Non-Sample Header file software in source and binary code form,
-- provided that i) this copyright notice, license and disclaimer  appear on all copies of
-- the software; and ii) Licensee does not utilize the software in a manner
-- which is disparaging to IBM.
--
-- This software is provided "AS IS."  IBM and its Suppliers and Licensors expressly disclaim all warranties, whether  EXPRESS OR IMPLIED,
-- INCLUDING ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR WARRANTY OF  NON-INFRINGEMENT.  IBM AND ITS SUPPLIERS AND  LICENSORS SHALL NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE THAT RESULT FROM USE OR DISTRIBUTION OF THE SOFTWARE OR THE COMBINATION OF THE SOFTWARE WITH ANY OTHER CODE.  IN NO EVENT WILL IBM OR ITS SUPPLIERS  AND LICENSORS BE LIABLE FOR ANY LOST REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL, INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER CAUSED AND REGARDLESS OF THE THEORY OF LIABILITY, ARISING OUT OF THE USE OF OR INABILITY TO USE SOFTWARE, EVEN IF IBM HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
--
-- @endCopyright
----------------------------------------------------------------------------
--
-- SOURCE FILE NAME: tbpsize_entitlement_req.sql
--
-- PURPOSE: This SQL script queries an administrative view (ADMINTABINFO) to summarize 
--          the size of user data stored in all user tables of the currently
--          connected database.  The USER_DATA_L_ENTITLEMENT_REQ_TB column reports
--          the size of user data in terabytes, rolled up to the next terabyte.  For example, 
--          USER_DATA_L_SIZE_TB of NULL, 0, 1 (KB) yields a results of 1 for the
--          USER_DATA_L_ENTITLEMENT_REQ_TB, and USER_DATA_L_SIZE_TB of 1.0001 
--          yields a result of 2 for the USER_DATA_L_ENTITLEMENT_REQ_TB.
--          
--          The size of user data are obtained by adding the logical sizes of data
--          objects, long objects, LOB objects, XML objects and COL objects.  
--          The calculation of the sizes of these objects includes the
--          meta-data stored inside user tables along with usre data, but not system or 
--          catalog tables.
--          Examples of such meta-data are compression 
--          dictionaries, data record headers, and free space control records. 
--         
--          Tables created by users, unless explicitly stated to be excluded, 
--          are always included in the user table data size calculation.  
--          Examples of such tables are tables created by users to facilitate 
--          explain utilities and event monitoring.
--          The result includes the following types of table objects: Created temporary
--          tables, Hierarchy tables, Detached tables, Materialized query tables, 
--          Untyped tables, and Typed tables. 
--
--          The sizes of following objects are not included in the result:
--            index objects.
---           table alias, nickname, and view.
--
-----------------------------------------------------------------------------


echo -- ;
echo -- DB2 LEVEL;
echo -- ;
echo ;
select substr(SERVICE_LEVEL, 1, 20) SERVICE_LEVEL, 
       substr(BLD_LEVEL, 1, 20) BLD_LEVEL, 
       substr(PTF, 1, 20) PTF
from SYSIBMADM.ENV_INST_INFO ;

echo -- ;
echo -- SUMMARY OF USER TABLE DATA SIZES;
echo -- ;
echo ;
select CURRENT SERVER as DBNAME, 
       CURRENT TIMESTAMP as CURRENT_TIMESTAMP,
       S.USER_DATA_L_SIZE_KB, 
       DEC( (S.USER_DATA_L_SIZE_KB/1073741824.0), 31, 11 ) as USER_DATA_L_SIZE_TB,
       COALESCE( CEIL( DEC( (S.USER_DATA_L_SIZE_KB/1073741824.0), 31, 11 ) ), 1 ) as USER_DATA_L_ENTITLEMENT_REQ_TB
from ( select ( sum(A.DATA_OBJECT_L_SIZE) + 
                sum(A.LONG_OBJECT_L_SIZE) + 
                sum(A.LOB_OBJECT_L_SIZE) + 
                sum(XML_OBJECT_L_SIZE) +
                sum(A.COL_OBJECT_L_SIZE) ) as USER_DATA_L_SIZE_KB 
       from SYSIBMADM.ADMINTABINFO as A, 
            ( select TABSCHEMA, TABNAME, OWNER, OWNERTYPE, TYPE, STATUS, TABLEID, TBSPACEID 
              from SYSCAT.TABLES 
              where OWNERTYPE = 'U' and TYPE IN ('G', 'H', 'L', 'S', 'T', 'U')  ) as T 
       where A.TABNAME = T.TABNAME and A.TABSCHEMA = T.TABSCHEMA ) as S ;

echo -- ;
echo -- BREAKDOWN OF USER TABLE DATA SIZES (INFORMATIONAL: NOT REQUIRED TO RUN TO COLLECT THE SIZE OF USER DATA);
echo -- ;
echo ;
select substr(A.TABSCHEMA, 1, 18) SCHEMA, 
       substr(A.TABNAME, 1, 18) TABLENAME, 
       sum(A.DATA_OBJECT_L_SIZE) as DATA_OBJECT_L_SIZE_KB, 
       sum(A.LONG_OBJECT_L_SIZE) as LONG_OBJECT_L_SIZE_KB, 
       sum(A.LOB_OBJECT_L_SIZE) as LOB_OBJECT_L_SIZE_KB, 
       sum(XML_OBJECT_L_SIZE) as XML_OBJECT_L_SIZE_KB, 
       sum(A.COL_OBJECT_L_SIZE) as COL_OBJECT_L_SIZE_KB, 
       ( sum(A.DATA_OBJECT_L_SIZE) +
         sum(A.LONG_OBJECT_L_SIZE) +
         sum(A.LOB_OBJECT_L_SIZE) +
         sum(XML_OBJECT_L_SIZE) +
         sum(A.COL_OBJECT_L_SIZE) ) as USER_DATA_L_SIZE_KB,
       T.COMPRESSION
from SYSIBMADM.ADMINTABINFO as A, 
     ( select TABSCHEMA, TABNAME, OWNER, OWNERTYPE, TYPE, STATUS, COMPRESSION, TABLEID, TBSPACEID from SYSCAT.TABLES 
       where OWNERTYPE = 'U' and TYPE IN ('G', 'H', 'L', 'S', 'T', 'U')  ) as T 
where A.TABNAME = T.TABNAME and A.TABSCHEMA = T.TABSCHEMA group by A.TABSCHEMA, A.TABNAME, T.COMPRESSION order by A.TABSCHEMA, A.TABNAME ;
