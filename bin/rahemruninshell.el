;; emacs load file for use with rah when RAHMONITOR=emacs
;;-----------------------------------------------------------------------
;; (C) COPYRIGHT International Business Machines Corp. 1994
;; All Rights Reserved
;;
;; US Government Users Restricted Rights - Use, duplication or
;; disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
;;
;; NAME: rah
;;
;; FUNCTION: rah runs a specified command at all hosts specified in a list
;;
;; rah is supplied as-is as an example of how to perform this function.
;; There is no warranty or support.
;;-----------------------------------------------------------------------
;; (setq debug-on-error t)
(setq lsysnm (system-name))
;; find leading part up to first period
(cond
 ( (setq ix (string-match "\\." lsysnm))
     (setq ssysnm (substring lsysnm 0 ix)) )
 ( t (setq ssysnm lsysnm) )
)
(setq identi (concat ssysnm ":" (getenv "shortcommand")))
;; start user-specified shell
(setq shell-symbol (intern (getenv "RAHEMSHELFUNC")))
;; if not a valid function then assume he started a shell from init file
(if (fboundp shell-symbol)
    (funcall (symbol-function shell-symbol))
)
;; ensure a shell was started and find its name
(cond
 (
  (setq shell-process (car (process-list)))
  ;; run the rahcommand in that shell
  (progn
   (process-send-string shell-process (concat (getenv "fullcommand") (char-to-string ?\n)))
   (set-buffer (process-buffer shell-process))
   (rename-buffer identi)
   (switch-to-buffer identi)
  )
 )
 (t (message "no shell process started, unable to run command"))
)
