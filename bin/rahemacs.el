;; emacs load file for use with rah when RAHMONITOR=emacs
;;-----------------------------------------------------------------------
;; (C) COPYRIGHT International Business Machines Corp. 1994
;; All Rights Reserved
;;
;; US Government Users Restricted Rights - Use, duplication or
;; disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
;;
;; NAME: rah
;;
;; FUNCTION: rah runs a specified command at all hosts specified in a list
;;
;; rah is supplied as-is as an example of how to perform this function.
;; There is no warranty or support.
;;-----------------------------------------------------------------------
;; (setq debug-on-error t)
(setq lsysnm (system-name))
;; find leading part up to first period
(cond
 ( (setq ix (string-match "\\." lsysnm))
     (setq ssysnm (substring lsysnm 0 ix)) )
 ( t (setq ssysnm lsysnm) )
)
(setq identi (concat ssysnm ":" (getenv "shortcommand")))
;; start process to tail the RAHBUFIL in a new buffer
(process-kill-without-query (start-process identi (generate-new-buffer identi) "tail" "-f" (getenv "RAHBUFIL")))
(switch-to-buffer identi)
(message "terminate this window by Ctl-X Ctl-C")
