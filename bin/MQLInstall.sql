--------------------------------------------------------------------------
-- (C) COPYRIGHT International Business Machines Corp. 1997
-- All Rights Reserved
--
-- US Government Users Restricted Rights - Use, duplication or
-- disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
--
-- NAME: MQLInstall.sql
--
-- FUNCTION: SQL script used to install DB2 MQListener.
--	     e.g. db2 -tvf MQLInstall.sql
--------------------------------------------------------------------------

DROP TABLE SYSMQL.Listeners;

CREATE TABLE SYSMQL.Listeners (
    configurationName VARCHAR(18) NOT NULL DEFAULT '',
    queueManager VARCHAR(48) NOT NULL DEFAULT '',
    inputQueue VARCHAR(48) NOT NULL,
    dbName VARCHAR(8) NOT NULL,
    dbUserID VARCHAR(30) NOT NULL DEFAULT '',
    dbPassword VARCHAR(40) FOR BIT DATA NOT NULL DEFAULT '',
    procNode VARCHAR(30) NOT NULL DEFAULT '',
    procSchema VARCHAR(30) NOT NULL,
    procName VARCHAR(128) NOT NULL,
    procType INTEGER NOT NULL DEFAULT 1,
    mqCoordinated CHAR NOT NULL CHECK(mqCoordinated = 'T' OR mqCoordinated = 'F') DEFAULT 'F',
    numInstances INTEGER NOT NULL CONSTRAINT posNumInstances CHECK (numInstances > 0  AND numInstances <= 128) DEFAULT 1,
    waitMillis INTEGER NOT NULL CONSTRAINT posWaitMillis CHECK (waitMillis >= 0) DEFAULT 0,
    minQueueDepth INTEGER NOT NULL CONSTRAINT posMinQueueDepth CHECK (minQueueDepth >= 1) DEFAULT 1,
    UNIQUE (configurationName, queueManager, inputQueue) 
);

-- BIND mqlConfig.bnd@

-- BIND mqlRun.bnd@

-- GRANT EXECUTE ON PACKAGE mqlRun TO PUBLIC@
