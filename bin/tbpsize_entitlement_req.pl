#!/usr/bin/env perl

my $terabyte_query = "./tbpsize_entitlement_req.sql";

unless (-e $terabyte_query) {
  $terabyte_query = $ENV{'INST_DIR'} . "/bin/tbpsize_entitlement_req.sql";
} 

unless (-e $terabyte_query) {
	print "tbpsize_entitlement_req.sql is not found, please re-run db2licm -t command from <instance_home>/sqllib/bin\n";
	exit;
}

# Establish connection with supplied arguments
if ($numargs = @ARGV == 3) {
	$dbname = $ARGV[0];
	$userid = $ARGV[1];
	$passwd = $ARGV[2];
	system("db2 connect to $dbname user $userid using $passwd");
	system("db2 -t   -f $terabyte_query");
	system("db2 connect reset");
	system("db2 terminate");
} else 
{
  die "Usage: tbpsize_entitlement_req [dbAlias] [userId] [passwd] \n" ;
} 
exit;

