-- -*- sql -*-
--
-- Sample DDL to create three workload management
-- event monitors.
--
--   -> assumes db2start issued
--   -> assumes connection to a database exists
--   -> assumes called by "db2 -tf wlmevmon.ddl"
--   -> Other notes:
--       - All target tables will be created in the table space named
--         USERSPACE1.   Change this if necessary.
--       - Any specified table spaces must exist prior to executing this DDL.
--         Furthermore they should reside across all partitions; otherwise
--         monitoring information may be lost.  Also, make sure they have space
--         to contain data from the event monitors.
--       - If the target table spaces are DMS table spaces, the PCTDEACTIVATE parameter
--         specifies how full the table space must be before the event monitor
--         automatically deactivates.  Change the value if necessary. It is recommended
--         that when the target table space has auto-resize enabled the PCTDEACTIVATE
--         be set to 100. Remove PCTDEACTIVATE for any specified System Managed (SMS)
--         table spaces.
--       - If AUTOSTART is specified, the event monitor will automatically
--         activate when the database activates.  If MANUALSTART is specified
--         instead, the event monitor must be explicitly activated through
--         a SET EVENT MONITOR statement after the database is activated.
--

--
-- To remind users how to use this file!
--
ECHO                              ;
ECHO ******* IMPORTANT ********** ;
ECHO                              ;
ECHO USAGE: db2 -tf wlmevmon.ddl  ;
ECHO                              ;
ECHO ******* IMPORTANT ********** ;
ECHO                              ;
ECHO                              ;

--
--
-- Set autocommit off
--
UPDATE COMMAND OPTIONS USING C OFF;

--
--  Define the activity event monitor named DB2ACTIVITIES
--
CREATE EVENT MONITOR DB2ACTIVITIES
    FOR ACTIVITIES
    WRITE TO TABLE
    ACTIVITY (TABLE ACTIVITY_DB2ACTIVITIES
              IN USERSPACE1
              PCTDEACTIVATE 100),
    ACTIVITYSTMT (TABLE ACTIVITYSTMT_DB2ACTIVITIES
                  IN USERSPACE1
                  PCTDEACTIVATE 100),
    ACTIVITYVALS (TABLE ACTIVITYVALS_DB2ACTIVITIES
                  IN USERSPACE1
                  PCTDEACTIVATE 100),
    ACTIVITYMETRICS (TABLE ACTIVITYMETRICS_DB2ACTIVITIES
                  IN USERSPACE1
                  PCTDEACTIVATE 100),
    CONTROL (TABLE CONTROL_DB2ACTIVITIES
             IN USERSPACE1
             PCTDEACTIVATE 100)
    AUTOSTART;

--
--  Define the statistics event monitor named DB2STATISTICS
--
CREATE EVENT MONITOR DB2STATISTICS
    FOR STATISTICS
    WRITE TO TABLE
    SCSTATS (TABLE SCSTATS_DB2STATISTICS
             IN USERSPACE1
             PCTDEACTIVATE 100),
    SCMETRICS (TABLE SCMETRICS_DB2STATISTICS
               IN USERSPACE1
               PCTDEACTIVATE 100),
    WCSTATS (TABLE WCSTATS_DB2STATISTICS
             IN USERSPACE1
             PCTDEACTIVATE 100),
    WLSTATS (TABLE WLSTATS_DB2STATISTICS
             IN USERSPACE1
             PCTDEACTIVATE 100),
    WLMETRICS (TABLE WLMETRICS_DB2STATISTICS
               IN USERSPACE1
               PCTDEACTIVATE 100),
    QSTATS  (TABLE QSTATS_DB2STATISTICS
             IN USERSPACE1
             PCTDEACTIVATE 100),
    HISTOGRAMBIN (TABLE HISTOGRAMBIN_DB2STATISTICS
                  IN USERSPACE1
                  PCTDEACTIVATE 100),
    OSMETRICS (TABLE OSMETRICS_DB2STATISTICS
               IN USERSPACE1
               PCTDEACTIVATE 100),
    SUPERCLASSMETRICS (TABLE SUPERCLASSMETRICS_DB2STATISTICS
               IN USERSPACE1
               PCTDEACTIVATE 100),
    SUPERCLASSSTATS (TABLE SUPERCLASSSTATS_DB2STATISTICS
               IN USERSPACE1
               PCTDEACTIVATE 100),
    CONTROL (TABLE CONTROL_DB2STATISTICS
             IN USERSPACE1
             PCTDEACTIVATE 100)
    AUTOSTART;

--
--  Define the threshold violation event monitor named DB2THRESHOLDVIOLATIONS
--
CREATE EVENT MONITOR DB2THRESHOLDVIOLATIONS
    FOR THRESHOLD VIOLATIONS
    WRITE TO TABLE
    THRESHOLDVIOLATIONS (TABLE THRESHOLDVIOLATIONS_DB2THRESHOLDVIOLATIONS
                         IN USERSPACE1
                         PCTDEACTIVATE 100),
    CONTROL (TABLE CONTROL_DB2THRESHOLDVIOLATIONS
             IN USERSPACE1
             PCTDEACTIVATE 100)
    AUTOSTART;

--
-- Commit work
--
COMMIT WORK;
