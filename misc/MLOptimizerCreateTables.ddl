-- -*- sql -*-
--
-- DDL to create ML Optimizer tables
--
--   -> assumes db2start issued
--   -> assumes connection to a database exists
--   -> assumes invoked by "db2 -tf MLOptimizerTables.DDL"
--

--
--  TABLE MODELS
--
CREATE TABLE SYSTOOLS.TABLE_MODELS( VERSIONID INTEGER NOT NULL,
                                    MODELID INTEGER,
                                    SCHEMANAME VARCHAR(128 OCTETS) NOT NULL,
                                    TABLENAME VARCHAR(128 OCTETS) NOT NULL,
                                    COLIDS VARCHAR(256 OCTETS),
                                    MODEL BLOB(2G),
                                    TIMEOFCREATION TIMESTAMP WITH DEFAULT CURRENT TIMESTAMP,
                                    ISTRAINED BOOLEAN WITH DEFAULT FALSE,
                                    METAMODEL BLOB(2G),
                                    PRIMARY KEY (VERSIONID,
                                                 SCHEMANAME,
                                                 TABLENAME) ENFORCED)
                                    IN SYSTOOLSPACE
                                    ORGANIZE BY ROW;
