==============
DB2 NoSQL JSON
==============

DB2 NoSQL JSON is a driver-based solution which facilitates using DB2 as a JSON 
document store.

* Embraces the flexibility and scalability of emerging NoSQL solutions without
  sacrificing traditional enterprise grade qualities of service.
  
* Extends NoSQL capabilities with traditional qualities of service and unique
  RDBMS capabilities  e.g. ACID properties

* Keeps it simple
  ** Simple API for basic JSON queries and updates
  ** Extensions as needed for specialized functions
  ** Little friction for model and schema changes 
  
* Keeps it fast
  ** Leverage the strengths of DB2 technology  e.g.
     indexing, buffering, scalability, access path optimization  


The NoSQL JSON package includes a Java API and a command line processor that 
interprets and executes commands dynamically.

---------------------------------------
Minimum Server Requirements for DB2 LUW
---------------------------------------
* IBM DB2 10.5 FP1+ for Linux, UNIX, and Windows

----------------------------------------
Minimum Server Requirements for DB2 z/OS
----------------------------------------
* IBM DB2 10.1 for z/OS, with pre-conditioning APAR PM97869 and enabling APAR PM98357

---------------------------------------
Minimum Client Requirements for DB2 LUW
---------------------------------------
* IBM DB2 10.5 FP1+ for Linux, UNIX, and Windows
    ** Includes the required IBM Data Server Driver for JDBC db2jcc.jar
    ** Includes the DB2 NoSQL JSON package:
        *** DB2 NoSQL JSON library nosqljson.jar
        *** Rhino JavaScript engine js.jar,
            required if using the NoSQL command line processor NoSQLCmdLine
        *** MongoDB Java driver mongo-2.8.0.jar,
            required if importing data from MongoDB
* Java Runtime Environment 1.5+

----------------------------------------
Minimum Client Requirements for DB2 z/OS
----------------------------------------
* IBM DB2 10.5 FP3+ for Linux, UNIX, and Windows
* Java Runtime Environment 1.5+

--------------------------------
Server Configuration for DB2 LUW
--------------------------------
* Create a UTF8 database to store JSON data.
  Due to limitations on row and index length, it is recommended to use a 32K page size.
  Example:
  db2 create database jsondb automatic storage yes using codeset utf-8 territory us pagesize 32 K
  
  ** Although not recommended, if using a database with less than 32 K pagesize, manually create a temporary tablespace with 32 K  pagesize  e.g.
  Example: 
  CREATE BUFFERPOOL NOSQLSYSTOOLSBP ALL DBPARTITIONNUMS SIZE AUTOMATIC PAGESIZE 32K
  CREATE TEMPORARY TABLESPACE NOSQLSYSTOOLSTEMP PAGESIZE 32K MANAGED BY AUTOMATIC STORAGE BUFFERPOOL NOSQLSYSTOOLSBP
  
* To support remote clients, ensure that DB2 is configured for TCP/IP communication.
  This task can be completed as part of the DB2 Setup Wizard.
  If this task has not been completed, refer to section
  "Configuring DB2 server communications (TCP/IP)" in the DB2 LUW InfoCenter.
  Example for DB2 LUW:
    db2set DB2COMM=TCPIP
    db2 update dbm cfg using svcename 50000
    db2stop
    db2start 
  
* Note the database server hostname / ip address, port number, and database
  name.  It will be needed for client configuration.

---------------------------------
Server Configuration for DB2 z/OS
---------------------------------
* Contact the database administrator to run the install job to setup JSON.
* To support remote clients, ensure that DB2 is configured for TCP/IP communication.

--------------------------------
Client Configuration for DB2 LUW
--------------------------------
* Start the NoSQL command line processor by going to json\bin, and running the
  db2nosql.bat / db2nosql.sh script.
  ** Ensure that "java" is in the PATH.
  ** The script assumes that the JDBC driver db2jcc.jar is already included in 
     the CLASSPATH environment variable.  If not, add db2jcc.jar to the 
     CLASSPATH or the -cp in the script.
  ** The command line processor requires information needed to connect to a
     database.
  ** Ensure that the user has the proper admin privileges to create
     tables and functions, which are required to enable NoSQL JSON functionality.
  ** The script assumes the database is on localhost:50000 unless told
     otherwise by the -hostName and -port options.
  ** The script uses jline-0.9.93.jar, which should be in the default SQLLIB/tools (server)
     or IBM DATA SERVER DRIVER\tools (driver) folder. It allows the command line to retrieve
     user input history. The usage is similar to the bash shell on a unix environment 
     (up and down arrow). If the script is outside of the default json\bin folder, then
     the location of jline-0.9.93.jar must be added to the CLASSPATH manually. If jline-0.9.93.jar
     cannot be found, then history retrieval will not work on non-windows platforms.
     

* In the NoSQL command line prompt, type "enable(true)" to create required 
  tables and functions on the database to enable NoSQL JSON functionality.
  Type "quit" to quit NoSQLCmdLine.
  Notes:
  -Ensure that the user has been granted the proper admin privileges to create 
   tables and functions, which are required to enable NoSQL JSON functionality.
  -Type "enable(false)" to display the SQL commands without executing them.
  -If the enable command fails, type "debug(true)" to enable debugging
   messages, and rerun enable to check for errors.
  -If needed for cleanup purposes, type "disable(true)" to drop JSON setup
   tables and functions.

* Example
   ./db2nosql.sh -user myuser -hostName myhost -port 50000 -db jsondb -password mypasswd
   nosql>enable(true)
   Database Artifacts created successfully
   nosql>quit

---------------------------------
Client Configuration for DB2 z/OS
---------------------------------
  ** Ensure that "java" is in the PATH.
  ** The script assumes that the JDBC driver db2jcc.jar is already included in 
     the CLASSPATH environment variable.  If not, add db2jcc.jar to the 
     CLASSPATH or the -cp in the script.


---------------------------------
Simple Example Using NoSQLCmdLine
---------------------------------
* Start the NoSQL command line processor by running the
  db2nosql.bat / db2nosql.sh script.

* Example
   ./db2nosql.sh -user myuser -hostName myhost -port 50000 -db jsondb -password mypasswd

* Insert a JSON document into a collection "mycollection".
  Internally, if the table does not exist, one is created automatically and 
  then the row is inserted.  The user must have CREATETAB authority.
  
    nosql>db.mycollection.insert({"name":"Jerry", "age":5})
    OK
    
* Find documents in "mycollection" where "name" is "Jerry"

    nosql>db.mycollection.find({"name":"Jerry"})
    nosql>Row 1:
    nosql> {
    nosql> "_id":{"$oid":"516dc212e057244a4d323171"},
    nosql> "name":"Jerry",
    nosql> "age":5
    nosql> }

* Type "help" to see available commands and queries.

* Type "quit" to quit.

-----------------------------
Simple Example Using Java API
-----------------------------
* Compile: 
  javac -classpath nosqljson.jar Sample.java

* Run:
  ** Windows  
     java -classpath .;db2jcc.jar;nosqljson.jar Sample
  ** Unix
     java -classpath .:db2jcc.jar:nosqljson.jar Sample
  
* Output:
  Inserting: {"name":"Jerry","age":5}
  Retrieved: {"_id":{"$oid":"516de413e0579fcc9f23fac3"},"name":"Jerry","age":5}


import com.ibm.nosql.json.api.*;

public class Sample
{
  public static void main (String[] args)
  {
    String databaseHost = "localhost";
    int port = 50000;
    String databaseName = "jsondb";
    String user = "myuser";
    String password = "mypass";
    
    // e.g. jdbc:db2://localhost:50000/jsondb
    String databaseUrl = "jdbc:db2://" + databaseHost + ":" + port + "/" + databaseName;
    System.out.println ("databaseUrl: " + databaseUrl);
    
    // get a DB object
    DB db = NoSQLClient.getDB (databaseUrl, user, password);
    
    // get a DBCollection object
    DBCollection col = db.getCollection ("mycollection");
    
    // insert document
    BasicDBObject json = new BasicDBObject ();
    json.append ("name", "Jerry");
    json.append ("age", 5);
    System.out.println ("Inserting: " + json);
    col.insert (json);
    
    // query
    BasicDBObject query = new BasicDBObject ();
    query.append ("name", "Jerry");
    DBCursor cursor = col.find (query);
    try
    {
      while (cursor.hasNext ()) {
        DBObject obj = cursor.next ();
        System.out.println ("Retrieved: " + obj);
      }
    }
    finally
    {
      cursor.close ();
    }
  }

}
  
  
----------------------------
NoSQL JSON API Documentation
----------------------------
* Javadoc for the API is provided in the doc folder.
  An overview is available by clicking the link for the
  "com.ibm.nosql.json.api" package, and then scrolling down to the package
  description.


----------------
NoSQL properties
----------------
* Properties that affect the behavior of the NoSQL client can be set in the 
  nosql.properties file.  The nosql.properties file should be included in a 
  directory that is in the classpath.

* Available properties:

#When using NoSQLClient.getDB(url,user,pass), the API will create a 
#JDBC connection pool and will manage it itself for this database.
#This property provides the number of connections in the pool.
#If the program needs more connections at a given time, some of the connection
#requests will have to wait until another connection is returned back to the pool.
#Keeping this number too low may reduce concurrency. Keeping it too high, when combined
#with other clients, may exceed the database connection limits and cause errors. 
#For example if you have 20 connections per client, and you have 10 clients, then your database
#needs to be configured handle at least 200 concurrent connections.
#Default: 20
nosql.connPoolSize=20

#Number of pooled worker threads to process WriteConcern.NONE, WriteConcern.NORMAL inserts and updates
#in a fast batching mode with less frequent commits to increase throughput. If these are too many threads
#then each thread will have less data to batch. If they are too few, then they will become a bottleneck 
#in a system that could otherwise push more data. Experiment with your server to find the sweet spot.
#Notice that 'nosql.connPoolSize' practically limits the amount of threads you can use, since each thread will
#need a connection to work with. If there are not enough connections, then some threads will wait until 
#one becomes available and that will decrease performance.
#Default: 10
nosql.asyncMaxThreadCount=10

#A trace file name that your program has permissions to write to.
#NoSQL API tracing/logging can also be configured via java.util.logging
#facilities. For example Websphere provides a web based admin console 
#that shows available loggers whose levels and log file you can set. 
#If logging is turned on both in Websphere and in this properties file,
#then log is written to both places. In general, turning on logging has
#performance cost. @see nosql.traceBufferRowCount to learn about in memory 
#buffering of log records to enhance performance.
#Default: null
nosql.traceFile=/tmp/nosql.txt

#The trace level to use.
#Detail level of file log. The levels in descending order are:
#OFF - Logging turned off.
#SEVERE - Least detail, only severe problems are logged.
#WARNING
#INFO - NoSQL API will log almost all messages below this level, and nothing above this level.
#CONFIG
#FINE
#FINER
#FINEST - very detailed log.
#ALL - All levels will be logged.
#Default: OFF
nosql.traceLevel=ALL

#Specifies whether the DB2 JSON API allows different _id types in a collection.
#If enabled, a limited set of Java data types and mappings are supported for _id:
#   For java.lang.Integer, java.lang.Long, java.lang.Double _id types, values are stored and retrieved as java.lang.Double.
#   For java.sql.Timestamp, java.sql.Date, java.util.Date _id types, values are stored and retrieved as java.util.Date.
#   For java.lang.String _id types, values are stored and retrieved as java.lang.String.
#   For com.ibm.nosql.bson.types.ObjectId _id types, values are stored and retrieved as com.ibm.nosql.bson.types.ObjectId.
#   For a byte array (byte[]) _id type, the value is stored and retrieved as a byte array.
#If enabled, the ID column in DB2 is defined as VARCHAR(256) FOR BIT DATA.
#Existing collections created with older DB2 JSON API versions can coexist with new collections with mixed-typed _id support.
#Values: true (allows different _id types) or false (one _id type per collection)
#Default: false
nosql.multiTypedID=true

-------
Tracing
-------
* To enable tracing for diagnostic purposes, create the nosql.properties file,
  and set the nosql.traceFile and nosql.traceLevel properties.
  Copy the file to a directory, and add the directory in the classpath.
  
* Example nosql.properties
nosql.traceFile=/tmp/nosql.txt  
nosql.traceLevel=ALL


-----------
Limitations
-----------
* The DB2 NoSQL JSON package provides only a Java driver.
  Even though the DB2 NoSQL JSON API is similar to the MongoDB Java API,
  existing Java MongoDB applications need to be ported and recompiled to use
  DB2 NoSQL JSON due to different package names and other incompatibilities.
  To run MongoDB applications against DB2 without recompiling,
  or if the application is written in another language other than Java,
  the NoSQL Wire Listener can be used.  Refer to section "NoSQL Wire Listener".
  
* The DB2 NoSQL JSON client cannot access existing tables that are not created 
  by the NoSQL JSON client.  JSON data is stored in tables with specific table 
  definitions.

* There is no convenient method to insert or retrieve JSON documents in tables
  using SQL in DB2 CLP.  This is done today via the NoSQL JSON API or the
  NoSQL command line processor.


-------------------
NoSQL Wire Listener
-------------------
 
* The NoSQL Wire Listener acts as a mid-tier gateway server between MongoDB
  applications and DB2. It leverages the DB2 NoSQL JSON API in order to
  interface with DB2.

* A user can use a Mongo application written in the desired language
  (Java, NodeJS etc), or can use the Mongo CLI to communicate with DB2.   

* Limitations: The Wire Listener supports common MongoDB operations, but is not  
  100% compatible.  Identifying and implementing missing functionality is an
  ongoing process for future releases to address.
   
* Requirements:
  ** JRE 1.6+
  ** Wire Listener library db2NoSQLWireListener.jar
  ** DB2 NoSQL JSON library nosqljson.jar
  ** JDBC driver db2jcc.jar
 
* Configuration:
  Since the Wire Listener uses the DB2 NoSQL JSON API, ensure steps in
  "Server Configuration" and "Client Configuration" are completed.
   
* The NoSQL Wire Listener server is started using the
  wplistener.bat / wplistener.sh script.
  It can use both JDBC Type 2 and Type 4 connections.
   
* Existing MongoDB applications need to point to the Wire Listener host and
  port instead of MongoDB server.
  The database name must be an existing database in DB2.

-------------------
NoSQL Wire Listener Security
-------------------

To enable an authentication check for incoming messages, a registration file can be specified when starting the wire listener. This registration file allows to manage users and associated passwords. The user and password combination is not used for DB2 authentication, only for message authentication by the wire listener. The userid/password combination might therefore be, for example, a token to register trusted applications, a token per wirelistener, or a password for an application proxy user, etc., For details, see the Security topic in the Solution Planning for JSON in the DB2 InfoCenter.
 
wplistener.sh -register -user <appid> -password <token> -registrationFile <regFile> -action <action>

with action one of ADD or REMOVE.

For example, to add an id
wplistener.sh -register -user app1  -password mytoken -registrationFile /home/acme/register.cfg -action add

This will create a file called "register.cfg" that can  be used when starting the listener to restrict access.

-------------------
NoSQL Wire Listener Start
-------------------

 
usage: wplistener
  wlpListener -start <start options> | -shutdown < shutdown options> | -help
Options:
 -dbName <DB2 database>      DB2 database name used as the NoSQL data store
 -debug                      Enable debug for the wire listener
 -help                       Display usage help for the listener
 -host <DB2 host:port>       hostname and port when connecting to a remote DB2
                             server(host:port)
 -logPath <path>             Path for storing execution log files
 -maxTCPThreads <threads>    Maximum number of TCP Threads
 -minTCPThreads <threads>    Minimum number of TCP Threads
 -mongoPort <port>           Port that is listening for Mongo client requests
 -noSQLHost <hostname>       Name of the Host running the NoSQL Wire Listener
 -password <password>        Password for the backend server
 -registrationFile <regfile> Registration File with Credentials
 -shutdown                   Stop the NoSQL listener
 -start                      Start the NoSQL listener
 -userid <userid>            Userid for the backend server
 
Note: Not all options are required.  The valid start and shutdown options are listed below:
<start options> : -mongoPort <port> -userid <userid> -logPath <path> [-password <password>] [-debug] [-minTCPThreads <threads>] [-maxTCPThreads <threads>] [registrationFile]

<shutdown options> : -mongoPort <port> -noSQLHost <hostname> -userid <userid> [ -logPath <path> ] [-password <password>] [-debug] [-useOriginalListener] [-testCmdLine] 
 

For example, to start the DB2 listener without registrationFile specify the following arguments. This will provide access to all users without authentication:
-start -mongoPort 27017 -userid <db2admin> -password <mypasswd> -debug -dbName <dbName> -logPath c:/temp/logs

To start with a registration file:
wplistener.sh -start -mongoPort 27017 -userid db2admin -dbName acmeair -registrationFile c:/acmewire/register.cfg

In this case, messages are accepted if they use correct credentials, Wrong credentials will result in a login failure. If a message has no credentials, a 'not authorized' message will be returned for the submitted query.


To stop the listener:

-shutdown -noSQLHost localhost -mongoPort 27017 -dbName <dbName> -userid <db2admin>

Note that -noSQLHost refers to the address of the wire listener for the shutdown.




--------
NOTICES
-------

This information was developed for products and services offered in the 
U.S.A. IBM may not offer the products, services, or features discussed 
in this document in other countries. Consult your local IBM representative 
for information on the products and services currently available in your 
area. Any reference to an IBM product, program, or service is not intended 
to state or imply that only that IBM product, program, or service may be used. 
Any functionally equivalent product, program, or service that does not infringe 
any IBM intellectual property right may be used instead. However, it is the 
user's responsibility to evaluate and verify the operation of any non-IBM 
product, program, or service. 

IBM may have patents or pending patent applications covering subject 
matter described in this document. The furnishing of this document 
does not give you any license to these patents. You can send license 
inquiries, in writing, to: 

       IBM Director of Licensing
       IBM Corporation
       North Castle Drive
       Armonk, NY  10594-1785
       U.S.A.


For license inquiries regarding double-byte (DBCS) information, contact 
the IBM Intellectual Property Department in your country or send 
inquiries, in writing, to: 

       IBM World Trade Asia Corporation
       Licensing
       2-31 Roppongi 3-chome, Minato-ku
       Tokyo 106, Japan

The following paragraph does not apply to the United Kingdom or any 
ther country where such provisions are inconsistent with local law: 
INTERNATIONAL BUSINESS MACHINES CORPORATION PROVIDES THIS PUBLICATION 
"AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, 
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, 
MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE. Some states do not 
allow disclaimer of express or implied warranties in certain transactions, 
therefore, this statement may not apply to you. 

This information could include technical inaccuracies or typographical 
errors. Changes are periodically made to the information herein; these 
changes will be incorporated in new editions of the publication. IBM 
may make improvements and/or changes in the product(s) and/or the 
program(s) described in this publication at any time without notice. 

Any references in this publication to non-IBM Web sites are provided for 
convenience only and do not in any manner serve as an endorsement of 
those Web sites. The materials at those Web sites are not part of the 
materials for this IBM product and use of those Web sites is as your 
own risk. 

Licensees of this program who want to have information about it for the 
purpose of enabling: (i) the exchange of information between independently 
created programs and other programs (including this one) and (ii) the 
mutual use of the information which has been exchanged, should contact: 

       IBM Corporation
       J74/G4
       555 Bailey Avenue
       San Jose, CA 95141-1003
       U.S.A.

Such information may be available, subject to appropriate terms and 
conditions, including in some cases, payment of a fee.

The licensed program described in this information and all licensed 
material available for it are provided by IBM under terms of the IBM 
Customer Agreement, IBM International Program License Agreement, or 
any equivalent agreement between us. 

Any performance data contained herein was determined in a controlled 
environment. Therefore, the results obtained in other operating 
environments may vary significantly. Some measurements may have been 
made on development-level systems and there is no guarantee that these 
measurements will be the same on generally available systems. Furthermore, 
some measurement may have been estimated through extrapolation. Actual 
results may vary. Users of this document should verify the applicable 
data for their specific environment. 

Information concerning non-IBM products was obtained from the suppliers 
of those products, their published announcements or other publicly 
available sources. IBM has not tested those products and cannot confirm 
the accuracy of performance, compatibility or any other claims related 
to non-IBM products. Questions on the capabilities of non-IBM products 
should be addressed to the suppliers of those products. 

All statements regarding IBM's future direction or intent are subject to 
change or withdrawal without notice, and represent goals and objectives 
only. 

All IBM prices shown are IBM's suggested retail prices, are current and 
are subject to change without notice. Dealer prices may vary. 

This information is for planning purposes only. The information herein 
is subject to change before the products described become available. 

This information contains examples of data and reports used in daily 
business operations. To illustrate them as completely as possible, 
the examples include the names of individuals, companies, brands, and 
products. All of these names are fictitious and any similarity to the 
names and addresses used by an actual business enterprise is entirely 
coincidental. 

-----------------
COPYRIGHT LICENSE 
-----------------

This information contains sample application programs in source language, 
which illustrates programming techniques on various operating platforms. 
You may copy, modify, and distribute these sample programs in any form 
without payment to IBM, for the purposes of developing, using, 
marketing or distributing application programs conforming to the a
pplication programming interface for the operating platform for which 
the sample programs are written. These examples have not been thoroughly 
tested under all conditions. IBM, therefore, cannot guarantee or imply 
reliability, serviceability, or function of these programs. 


7.1	Use of Evaluation Program
If you are using an evaluation copy of the program, the following 
terms apply:

This program contains a disabling device that will prevent it from
being used upon expiration of this license. You will not tamper with 
this disabling device or program. You should take precautions to avoid
any loss of data that might result when the program can no longer be
used.

See the License Agreement terms that apply to the use of evaluation 
code.

7.2	Trademarks and service marks  

The following terms are trademarks or service marks of the IBM 
Corporation in the United States or other countries or both:

      DB2                        
      IBM
  
Java and all Java-based trademarks are trademarks of Sun Microsystems, 
Inc. in the United States, other countries, or both.

Linux is a registered trademark of Linus Torvalds in the United States, other countries, or both.

Microsoft, Windows, Windows NT, and the Windows logo are trademarks of 
Microsoft Corporation in the United States, other countries, or both.

UNIX is a registered trademark of The Open Group in the United States 
and other countries.

Other company, product, and service names may be trademarks or service 
marks of others.


(C) Copyright IBM Corporation 2001, 2013.  Copyright InfoTel 2001, 2013.
All rights reserved.


Note to U.S. Government Users Restricted Rights -- Use, duplication or 
disclosure restricted by GSA ADP Schedule Contract with IBM Corp. 
