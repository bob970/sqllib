#!/bin/sh
#############################################################################
#
# Licensed Materials - Property of IBM
#
# "Restricted Materials of IBM" 
#
# (C) COPYRIGHT IBM Corp. 1993, 2019 All Rights Reserved.
#
# US Government Users Restricted Rights - Use, duplication or
# disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
#
#############################################################################

#
# NAME: db2nrupgrade
#
# FUNCTION: db2nrupgrade - upgrade a DB2 instance for a previous version
#                      of DB2 to the current version.
#
# USAGE: db2nrupgrade [-h|-?] [-d] [-a AuthType]-b BackupPath
#          -h|-?         display the usage information.
#          -d            turn debug mode on.
#          AuthType      is the authentication type (SERVER, CLIENT or DCS)
#                        for the instance. The default is SERVER.
#          BackupPath    The backup directory of the previous version
#
#############################################################################
# Options for "set" command
setopts="${setopts:-+x}"
set ${setopts?}

# Current DB2 installation directory

DB2LOCAL=`echo $DB2LOCAL`

if [ "X${DB2LOCAL?}" != "X" -a  -f ${DB2LOCAL?}/.mount ]; then
   DB2DIR=${DB2LOCAL?}/instance
   curdir=`/bin/pwd`
else
   curdir=`/bin/pwd`
   DB2DIR=`dirname $0`
   cd ${DB2DIR?}
   DB2DIR=`/bin/pwd`
fi

cd ${DB2DIR?}
DB2DIR=`dirname ${DB2DIR?}`
if [ ! -d ${DB2DIR?}/instance ]
then
    DB2DIR=`dirname ${DB2DIR?}`
fi
cfg_tmp="${DB2DIR?}/cfg/installcopy.cfg"
cmd_db2fupdt_tmp="${DB2DIR?}/bin/db2fupdt"
DB2_KEEP_IN_ORIGINAL_DB2DIR="${DB2_KEEP_IN_ORIGINAL_DB2DIR:-FALSE}"

if [ -f "${cfg_tmp?}" -a -f "${cmd_db2fupdt_tmp?}"  -a "X${DB2_KEEP_IN_ORIGINAL_DB2DIR?}" != "XTRUE" ]; then
   db2dir_tmp=`${cmd_db2fupdt_tmp?} -f ${cfg_tmp?} -p DB2DIR`
   if [ $? -eq 0 ]; then
      if [ -d ${db2dir_tmp?} ]; then
         cd ${db2dir_tmp?} 2>/dev/null 1>/dev/null
         if [ $? -eq 0 ]; then
            DB2DIR=`echo ${db2dir_tmp?} | sed 's/\/$//'`
         fi
     fi
   fi
fi 
cd ${curdir?}
unset curdir cfg_tmp cmd_db2fupdt_tmp db2dir_tmp DB2_KEEP_IN_ORIGINAL_DB2DIR

DB2LOCAL=`echo $DB2LOCAL`

if [ "X${DB2LOCAL?}" != "X" -a  -f ${DB2LOCAL?}/.mount ]; then
   if [ -d ${DB2LOCAL?}/instance ]; then
      PROGDIR=${DB2LOCAL?}/instance
      curdir=`/bin/pwd`
   fi
else
  # Set the directory name where this file is located.
  curdir=`/bin/pwd`
  PROGDIR=`dirname $0`
  cd ${PROGDIR?}
  PROGDIR=`/bin/pwd`
  cd ${curdir?}
  chk_copy_owner ${DB2DIR?}     # Check if it's the install copy owner running the program
fi

PROGNAME=`basename $0`       # Program name
PROGARGS="$*"

# Include common definition file
if [ -f ${PROGDIR?}/db2iutil ]; then
    . ${PROGDIR?}/db2iutil
else
    echo "File ${PROGDIR?}/db2iutil not found."
    echo "Exiting..."
    exit 1
fi

get_locale         # Get the locale name of set message catalog path

# Special variables needed for instance upgrade
eval SPACEREQ=10240      # Disk space (in KB) required to upgrade an instance
DBI_INVALID_INST=10      # Invalid instance name

#-----------------------------------------------------------------------#
#               Start of function definitions
#-----------------------------------------------------------------------#

# To display the syntax menu
syntax()
{
    display_msg ${DB2CAT?} 388 \
       'DBI1388I Usage: %s [-h|-?] [-d] [-a AuthType] -b BackupPath' \
        ${PROGNAME?}
    cleanup yes 
    exit 1
}

# Check the bit width of the instance to be upgraded
chk_bitwidth()
{
    trac chk_bitwidth $@
    set ${setopts?}

    query_instuse ${INSTNAME?} "BitWidth" 0
    BitWidth_M="${INSTUSE_VALUE?}"

    trax chk_bitwidth
}

# Default values for command line options
defaultopts()
{
    set ${setopts?}
    DEBUG=1               # Debug mode is FALSE
    FORCEMIGR=1           # Force upgrade, upsupported
    FORCEMIGREQ=1         # Variable becomes true if Forced Upgrade is actually
                          # required regardless of whether or not it is requested
    AUTHTYPE="SERVER"     # Default authentication type
    NRCFGFLAGS=""          # This variable will store -flags that db2nrupgrade will want
                          # to pass on the appropriate nrcfg when it calls it.
    DASVERLIST="${DB2VER_V5?}"         # List of version that can upgrade a DAS instance
    DASTMPLIST="/tmp/.dasmigfile.lst"
    INSTTYPE=${ITYPE_UNK?}   # Initialize variable for instance type as UNKNOWN
    INSTTYPE_ORIG=${ITYPE_UNK?}
    CFGFILE=""
    CFGFILETYPE=""
    INSTNAME=`${DB2DIR?}/bin/db2usrinf -n -E`
    INSTHOME=`${DB2DIR?}/bin/db2usrinf -d ${INSTNAME?}`
    OLDSQLLIB=""
    DB2DLNK=${FALSE?}
}

# Retrieves list of user's set environment variables
init_envlist()
{
    trac get_envlist $@
    set ${setopts?}

    # su's to the user and retrieves the environment list using "env"
    rm -f ${TMPFILE4?}
    env > ${TMPFILE4?}

    trax get_envlist
}

# Sets an instance-level registry variable
set_regvar()
{
    trac set_regvar $@
    set ${setopts?}

    regvar=$1
    regvalue=$2
    dmsg "regvar = ${regvar?}"
    dmsg "regvalue = ${regvalue?}"

    db2set ${regvar?}=${regvalue?} 1> /dev/null 2> /dev/null
    rc=$?

    dmsg "rc = ${rc?}"

    trax set_regvar
}

# Gets an instance-level environment setting
get_envvar()
{
    trac get_envvar $@
    set ${setopts?}

    envvar=$1
    dmsg "envvar = ${envvar?}"

    ENVVALUE=`cat ${TMPFILE4?} | grep "^${envvar?}=" | cut -d= -f2`
    dmsg "value = ${ENVVALUE?}"

    trax get_envvar
}

# Gets an instance-level registry setting
get_regvar()
{
    trac get_regvar $@
    set ${setopts?}

    regvar=$1
    dmsg "regvar = ${regvar?}"

    rm -f ${TMPFILE3?}
    db2set ${regvar?} > ${TMPFILE3?} > /dev/null 2> /dev/null
    rc=$?

    REGVALUE=`cat ${TMPFILE3?}`
    dmsg "rc = ${rc?}"
    dmsg "value = ${REGVALUE?}"
    rm -f ${TMPFILE3?}

    if [ ${rc?} -ne 0 ]
    then
        REGVALUE=""
        dmsg "value reset to null"
    fi

    trax get_regvar
}

# Sets a DBM CFG parameter
set_dbmcfgparm()
{
    trac set_dbmcfgparm $@
    set ${setopts?}

    cfgparm=$1
    cfgval=$2
    dmsg "cfgparm = ${cfgparm?}"
    dmsg "cfgval = ${cfgval?}"

    db2 update dbm cfg using ${cfgparm?} '${cfgval?}' 1> /dev/null 2> /dev/null
    rc=$?

    dmsg "rc = ${rc?}"

    trax set_dbmcfgparm
}

# Upgrades and updates the appropriate registry variables
nrupgrade_registry()
{
    set ${setopts?}
    get_regvar DB2COMM
    if [ "X${REGVALUE?}" != "X" ]
        then
        # mark changes
        changed=${FALSE?}

        unset NEWREGVALUE
        for prot in `echo ${REGVALUE?} | sed -e 's/,/ /g' | tr a-z A-Z`
        do
            case ${prot?} in
                IPXSPX|APPC)
                    # these are removed.
                    changed=${TRUE?}
                    display_msg ${DB2CAT?} 185 \
                'DBI1185I Server protcol %s is no longer supported' \
                        ${prot?}
                    ;;
                *)
                    NEWREGVALUE="${NEWREGVALUE}${NEWREGVALUE:+,}${prot?}"
                    ;;
            esac
        done
        if [ ${changed?} -eq ${TRUE?} ]
        then
            NEWREGVALUE=`echo ${NEWREGVALUE?} | sed -e 's/ /,/g'`
            set_regvar DB2COMM ${NEWREGVALUE?}
        fi
    fi
}

# Check all cataloged local databases for conditions that would
# prevent their successful upgrade.
chk_databases()
{
    trac chk_databases $@
    set ${setopts?}

    # Error codes used in this function only
    EXIT_DB2CKMIG_WARNING=1 # Err code for authentication type mismatch
    EXIT_DB2CKMIG_ERROR=3   # Err code for view problem - override with -m flag

    rm -f ${TMPFILE1?}
    ${PROGDIR?}/db2imchk ${INSTNAME?} ${INSTHOME?} ${TMPFILE1?}
    exitcode=$?

    if [ -s ${TMPFILE1?} ]; then
        cat ${TMPFILE1?} >> ${DB2INSTLOG?}  # Append the logfile
    fi
    rm -f ${TMPFILE1?}

    case ${exitcode?} in
        ${TRUE?} )
            :    # No errors
            ;;
        ${EXIT_DB2CKMIG_WARNING?} )
            display_msg ${DB2CAT?} 266 \
                'DBI1266I  Refer to the log file "%s" for more information.' \
                 ${INSTHOME?}/upgrade.log
            ;;
        ${EXIT_DB2CKMIG_ERROR?} )
            display_msg ${DB2CAT?} 205 \
                'DBI1205E One or more local databases cannot be \
                upgraded to the version from which you are running \
                this command. Check the log file %s for the list of \
                errors.\n' ${INSTHOME?}/upgrade.log 

            if [ ${FORCEMIGR?} -eq ${FALSE?} ]; then
                stop_prog 1
            fi
            ;;
        * )
            display_msg ${DB2CAT?} 69 \
                'DBI1069E Unexpected error. Function = %s, Return code = %s.' \
                chk_databases ${exitcode?}
            stop_prog 1
            ;;
    esac

    trax chk_databases
}

backup_old_sqllib()
{
    trac backup_old_sqllib $@
    set ${setopts?}

    # Get the DB2 version for the instance
    chk_version_nr ${INSTNAME?} ${OLDSQLLIBDIR?}
    db2iver=$?    # DB2 version for instance $INSTNAME

    # Ensure that we are upgrading a valid instance
    if [ ${db2iver?} -ne ${DB2INSTVER?} -o ${db2iver?} -eq ${DB2VER?} ]; then
        display_msg ${DB2CAT?} 202 \
            'DBI1202E Instance %s cannot be upgraded. Return code %s.\n' \
            ${INSTNAME?} ${DBI_INVALID_INST?}
        stop_prog 1
    fi

    trax backup_old_sqllib
}

# clean directory upgrade trace files
# Passed is either "REMOVE" or "SAVE" 
# REMOVE deletes the trace files from the instance's home directory
# SAVE moves the trace files to sqllib/db2dump directory
clean_nrupgrade_logs()
{
    trac clean_nrupgrade_logs $@
    set ${setopts?}
    
    FLAG=$1
    trcFileList="db2imdbd.dmp db2imnod.dmp db2migsig.dmp db2imgw.dmp"

    for file in ${trcFileList?}
    do
        if [ -f ${INSTHOME?}/${file?} ]; then
            if [ "${FLAG?}" = "REMOVE" ]; then
                rm -f ${INSTHOME?}/${file?}
            elif [ "${FLAG?}" = "SAVE" ]; then
                mv ${INSTHOME?}/${file?} ${INSTHOME?}/sqllib/db2dump/
            fi
        fi
    done

    trax clean_nrupgrade_logs
}

# Copy system database directory, node directory & DCS directory
copy_db_dirs()
{
    trac copy_db_dirs $@
    set ${setopts?}

    # Copy the system database directory, node & DCS directies
    # Change ownership and permissions of all newly copied dirs
    instdirs="sqldbdir sqlnodir sqlgwdir"
    for dname in ${instdirs?}; do
        old_dname="${INSTHOME?}/${OLDSQLLIB?}/${dname?}"
        new_dname="${INSTHOME?}/sqllib/${dname?}"

        # If the old dir exist, copy it to new sqllib
        if [ -d ${old_dname?} ] ; then
            if [ -d ${new_dname?} ]; then
                rm -rf ${new_dname?}
            fi
            cp -pr ${old_dname?} ${new_dname?}
            if [ $? -ne 0 ] ; then
                display_msg ${DB2CAT?} 86 \
                    'DBI1086E An attempt to copy the file or directory %s to %s failed.\n' \
                    ${old_dname?} ${new_dname?}
                stop_prog 1
            fi
        fi

        # Set perms, ownership on new files
        if [ -d ${new_dname?} ]; then
            mkpermission "ug=rwx,o=rx" ${new_dname?}
            for file_or_dir in `find ${new_dname?}/*`; do
              if [ -d ${file_or_dir?} ]
              then
                mkpermission "ug=rwx,o=rx" ${file_or_dir?}
              else
                mkpermission "ug=rw,o=r" ${file_or_dir?}
              fi
            done
        fi
    done

    trax copy_db_dirs
}

# This function is used upgrade all the files/dirs
# containing useful info about the instance:
#     database, node, and/or dcs directories
#     database config file
upgrade_instinfo()
{
    trac upgrade_instinfo $@
    set ${setopts?}

    # if there are old profiles for the CC, copy them over
    if [ -f ${INSTHOME?}/${OLDSQLLIB?}/db2misc.prf ]
    then
        copyfile ${INSTHOME?}/${OLDSQLLIB?} ${INSTHOME?}/sqllib db2misc.prf
    fi

    # Temp file for local database directory upgrade errors
    tmperror="${INSTCTRLDIR?}/.imldbpath"
    rm -f ${tmperror?}

    # Error codes used in this function only
    EXIT_NODE_FAIL=2       # Err code for node directory upgrade failure
    EXIT_SYSDBDIR_FAIL=3   # Err code for system database directory upgrade failure
    EXIT_LDBDIR_FAIL=4     # Err code for local database directory upgrade failure

    # Upgrade the system and local database directories
    if [ -d ${INSTHOME?}/sqllib/sqldbdir ] ; then
        
        ${INSTHOME?}/sqllib/bin/db2imdbd ${INSTHOME?}/sqllib ${INSTNAME?}
        exitcode=$?

        case ${exitcode?} in
            ${TRUE?})
                :  # No errors
                ;;
            ${EXIT_SYSDBDIR_FAIL?})
                display_msg ${DB2CAT?} 222 \
                    'DBI1222W The system database directory is corrupted and cannot be upgraded.'
                stop_prog 1
                ;;
            ${EXIT_LDBDIR_FAIL?})
                # If temp file for local database directory upgrade errors
                # exists
                if [ -s ${tmperror?} ]; then
                    cat ${tmperror?} | while read ldbdirpath; do
                        display_msg ${DB2CAT?} 212 \
                            'DBI1212W Upgrade of the local database directory on %s failed.' \
                            ${ldbdirpath?}
                    done
                else
                    display_msg ${DB2CAT?} 69 \
                        'DBI1069E Unexpected error. Function = %s, Return code = %s.' \
                        upgrade_instinfo 21
                fi
                stop_prog 1
                ;;
            *)
                display_msg ${DB2CAT?} 69 \
                    'DBI1069E Unexpected error. Function = %s, Return code = %s.' \
                    upgrade_instinfo_dbdir ${exitcode?}
                stop_prog 1
                ;;
        esac
    fi

    # Upgrade the system node directory
    if [ -d ${INSTHOME?}/sqllib/sqlnodir ] ; then
        ${INSTHOME?}/sqllib/bin/db2imnod ${INSTHOME?}/sqllib
        exitcode=$?

        case ${exitcode?} in
            ${TRUE?})
                :    # No errors
                ;;
            ${EXIT_NODE_FAIL?})
                display_msg ${DB2CAT?} 220 \
                    'DBI1220E Upgrade of the node directory on %s failed.' \
            ${INSTHOME?}/sqllib
                stop_prog 1 ;;
            *)
                display_msg ${DB2CAT?} 69 \
                    'DBI1069E Unexpected error. Function = %s, Return code = %s.' \
                    upgrade_instinfo_node ${exitcode?}
                stop_prog 1 ;;
        esac
    fi

    # New db2systm
    newdb2systm="${INSTHOME?}/sqllib/db2systm"
    # Old db2systm
    olddb2systm="${INSTHOME?}/${OLDSQLLIB?}/db2systm"

    if [ -d ${INSTHOME?}/${OLDSQLLIB?}/lib64 ]
    then
       OLD_INSTBITWIDTH=64
    else
       OLD_INSTBITWIDTH=32
    fi

    ${INSTHOME?}/sqllib/bin/db2imdbm ${newdb2systm?} ${olddb2systm?} ${HIGHEST_BIT?} ${OLD_INSTBITWIDTH?} ${INSTNAME?}
    if [ $? -ne 0 ]; then
        # Restore the default dbm cfg file
        rm -f ${newdb2systm?}
        nr_copy_dbmcfg
        orig_value=${INSTTYPE?}
        prepare_itype
        new_insttype=${INSTTYPE?}
        INSTTYPE=${orig_value?}
        display_msg ${DB2CAT?} 282 \
           'DBI1282W The database manager configuration files could not be merged. The previous database manager configuration file is %s. The previous instance type is %s, and the current instance type is %s.\n' \
            ${olddb2systm?} `display_inst_type ${INSTTYPE_ORIG?}` `display_inst_type ${new_insttype?}`
    fi

    trax upgrade_instinfo
}

copy_UDFs()
{
    trac copy_UDFs $@
    set ${setopts?}
    if [ ! -d "${INSTHOME?}/${OLDSQLLIB?}/function" ]; then
        return ${TRUE?}
    fi
    # Copy all non-installed physical files back to function
    # from the OLDSQLLIB/function directory to the sqllib/function directory.
    cd ${INSTHOME?}/${OLDSQLLIB?}
    find function -type f | \
        while read filename; do
              if [ ! -f ${INSTHOME?}/sqllib/${filename?} ]
              then
                 find ${INSTHOME?}/${OLDSQLLIB?}/.metadata -name files -exec \
                     grep "${filename?}" {} \; | grep "${filename?}" 2>/dev/null  1>/dev/null
                 if [ $? -ne 0 ]; then
                    cp -pf ${INSTHOME?}/${OLDSQLLIB?}/${filename?} ${INSTHOME?}/sqllib/${filename?}
                 fi
              fi
        done
    trax copy_UDFs
}

# Install/setup user exit program
inst_userexit()
{
    trac inst_userexit
    set ${setopts?}

    # If a previous version of the user exit program exists, upgrade it.
    if [ -f ${INSTHOME?}/${OLDSQLLIB?}/adm/db2uexit ]
    then
        # Copy old db2uexit program
        cp -p ${INSTHOME?}/${OLDSQLLIB?}/adm/db2uexit \
                      ${INSTHOME?}/sqllib/adm/db2uexit
        if [ $? -ne 0 ]
        then
            display_msg ${DB2CAT?} 86 \
                'DBI1086E An attempt to copy the file or directory %s to %s failed.\n' \
                ${INSTHOME?}/${OLDSQLLIB?}/adm/db2uexit \
                ${INSTHOME?}/sqllib/adm/db2uexit
            stop_prog 1
        fi
        mkpermission "ug=rx,o=" ${INSTHOME?}/sqllib/adm/db2uexit
    fi

    # Find the uext2 wrapper program to copy
    if [ -f ${INSTHOME?}/${OLDSQLLIB?}/adm/db2uext2 ]
    then
        cp -p ${INSTHOME?}/${OLDSQLLIB?}/adm/db2uext2 \
                      ${INSTHOME?}/sqllib/adm/db2uext2
        if [ $? -ne 0 ]
        then
            display_msg ${DB2CAT?} 86 \
                'DBI1086E An attempt to copy the file or directory %s to %s failed.\n' \
                ${INSTHOME?}/${OLDSQLLIB?}/adm/db2uext2 ${INSTHOME?}/sqllib/adm/db2uext2
            stop_prog 1
        fi
        mkpermission "ug=rx,o=" ${INSTHOME?}/sqllib/adm/db2uext2
    fi

    trax inst_userexit
}

# Restore file for instance specific setting

restore_isetting()
{
    trac restore_isetting  $@
    set ${setopts?}
    BACKUP_DIR=$1
    NEW_DIR=$2
    FILE=$3
    old_file="${BACKUP_DIR?}/${FILE?}"
    new_file="${NEW_DIR?}/${FILE?}"
    if [ -f ${old_file?} ]; then
        cp -p ${old_file?} ${new_file?}
        if [ $? -ne 0 ]; then
            display_msg ${DB2CAT?} 86 \
                'DBI1086E An attempt to copy the file or directory %s to %s failed.\n' \
                ${old_file?} ${new_file?}
        fi
    fi
    trax restore_isetting
}

# Update selected variable in the instance registry
update_registry()
{
    trac update_registry $@
    set ${setopts?}
    restore_list="profile.env userprofile usercshrc"
    for restore_f in ${restore_list?}; do
       restore_isetting ${INSTHOME?}/${OLDSQLLIB?} ${INSTHOME?}/sqllib ${restore_f?}
    done

    trax update_registry
}

chk_nrupgradepath()
{
    trac chk_nrupgradepath $@
    set ${setopts?}

    db2instver=$1          # DB2 version used by the instance

    # If the instance is already using the current version
    # of DB2, do not upgrade it.
    if [ "X${db2instver?}" = "X${DB2VER?}" ]; then
        display_msg ${DB2CAT?} 124 \
            'DBI1124E Instance %s cannot be upgraded.\n' ${INSTNAME?}
        stop_prog 1
    fi

    # Check if the upgrade-path is supported

    case ${db2instver?} in
        ${DB2VER_V111?})  :    # do nothing - supported upgrade path
            ;;
        ${DB2VER_V106?})  :    # do nothing - supported upgrade path
            ;;
        ${DB2VER_V105?})  :    # do nothing - supported upgrade path
            ;;
        ${DB2VER_V101?})  :    # do nothing - supported upgrade path
            ;;
        ${DB2VER_V97?})  :    # do nothing - supported upgrade path
            ;;
        ${DB2VER_V98?})  :    # do nothing - supported upgrade path
            ;;
        *)
            display_msg ${DB2CAT?} 69 \
                'DBI1069E Unexpected error. Function = %s, Return code = %s.' \
                chk_nrupgradepath 20
            stop_prog 1
            ;;
    esac

    trax chk_nrupgradepath
    return ${TRUE?}
}

# Initialize the instance upgrade process
# Ensure that everthing is OK to upgrade the instance
# Sets env variables DB2INSTVER, DB2IPRDDIR, and OLDSQLLIB.
init_nrupgrade()
{
    trac init_nrupgrade $@
    set ${setopts?}

    # Set the value of vars INSTHOME and INSTPGRP
    # and check their validity
    get_instance
    OLD_INSTBITWIDTH=${INSTBITWIDTH?}

    # Clean up any db dir upgrade logs from previous attempt
    clean_nrupgrade_logs "REMOVE"

    # Define special variables
    def_specials

    # Check the DB2 version for the current instance
    chk_version_nr ${INSTNAME?} ${OLDSQLLIBDIR?}
    DB2INSTVER=$?                 # DB2 version for the instance INSTNAME
    DB2IPRDDIR="${db2proddir?}"   # DB2 product dir for instance INSTNAME

    # Set the env variable OLDSQLLIB
    case ${DB2INSTVER?} in
        ${DB2VER_V101?})  OLDSQLLIB="sqllib_v101"  ;;
        ${DB2VER_V97?})   OLDSQLLIB="sqllib_v97"  ;;
        ${DB2VER_V98?})   OLDSQLLIB="sqllib_v98"  ;;
        *)                OLDSQLLIB="sqllib_v${DB2INSTVER?}" ;;
    esac

    # DB2DIR is different from DB2IPRDDIR. Variable DB2DIR refers to the
    # to DB2 product dir for the LATEST version of DB2, whereas, DB2IPRDDIR
    # refers the DB2 product currently used by instance INSTNAME. Similarly,
    # there is a difference between DB2VER and DB2INSTVER. DB2VER refers
    # to the latest version of DB2, whereas, DB2INSTVER refers to the DB2
    # version of the instance INSTNAME.

    # Check that this upgrade from DB2INSTVER to DB2VER is supported
    chk_nrupgradepath ${DB2INSTVER?}

    # Get the instance type
    get_insttype_nr ${INSTNAME?} ${DB2INSTVER?} ${OLDSQLLIBDIR?}

    INSTTYPE_ORIG=${INSTTYPE?}

    INSTTYPE_KEEP_STR=""
    if [ ${KEEPITYPE?} -eq ${TRUE?} ]; then
        case ${INSTTYPE_ORIG?} in
            ${ITYPE_EEE?}) INSTTYPE_KEEP_STR="ese";;
            ${ITYPE_SRV?}) INSTTYPE_KEEP_STR="wse";;
            ${ITYPE_PER?}) INSTTYPE_KEEP_STR="standalone";;
            ${ITYPE_CLN?}) INSTTYPE_KEEP_STR="client";;
        esac
    fi

    # Ensure that the AUTHTYPE value is valid
    chk_authtype

    # Check if the backup sqllib directory exists
    if [ ! -d ${INSTHOME?}/${OLDSQLLIB?} ] ; then
        display_msg ${DB2CAT?} 81 \
            'DBI1081E The file or directory %s is missing.\n' \
            ${INSTHOME?}/${OLDSQLLIB?}
        stop_prog 1
    fi

    # Check the bitwidth of the instance to be upgraded
    chk_bitwidth

#   # Verify that all cataloged local databases can be upgraded
#   chk_databases

    # Set the env variable BAKDB2SYSTM for backup dbmcfg file
    BAKDB2SYSTM="${SQLLIBBACKUP?}/${OLDDB2SYSTM?}"

    # Retrieve environment variables
    init_envlist

    # Backup useful info about the instance
    nr_backup_instinfo ${SQLLIBBACKUP?}

    trax init_nrupgrade
}

nrupgrade_instance()
{
    trac nrupgrade_instance $@
    set ${setopts?}

    # If we pass this point, undo all changes if any error occurs
    UNDOCHG=${TRUE?}

    # See if instance is a Server instance and if a Server product is installed
    server_instance ${OLDSQLLIBDIR?}
    serverInstance=$?
    server_installed
    serverInstalled=$?

    rm -f ${TMPFILE2?}

    # Check if Spatial Extender is installed if needed
    if [ -f ${INSTHOME?}/sqllib/cfg/db2gse.lvl ]; then
        if [ ! -f ${DB2DIR?}/cfg/db2gse.lvl ]; then
            FORCEMIGREQ=${TRUE?}
            display_msg ${DB2CAT?} 183 'DBI1183W Spatial Extender not installed.\n'
        fi
    fi

    # Check if Life Sciences is installed if needed
    if [ -f ${INSTHOME?}/sqllib/lib/liblsfile.a ]; then
        if [ ! -f ${DB2DIR?}/cfg/db2lsdc.lvl ]; then
            FORCEMIGREQ=${TRUE?}
            display_msg ${DB2CAT?} 184 'DBI1184W Life Sciences Data Connect not installed.\n'
        fi
    fi

    # Check if Query Patroller is installed if needed
    if [ -f ${INSTHOME?}/sqllib/cfg/dqplevel ]; then
        if [ ! -f ${DB2DIR?}/cfg/db2qp.lvl ]; then
            FORCEMIGREQ=${TRUE?}
            display_msg ${DB2CAT?} 138 'DBI1138W Query Patroller not installed. \n'
        fi
    fi

    # Check if a server version of DB2 v8 is installed if needed
    if [ ${serverInstance?} -eq ${TRUE?} ]; then
        if [ ${serverInstalled?} -ne ${TRUE?} ]; then
            FORCEMIGREQ=${TRUE?}
            display_msg ${DB2CAT?} 137 'DBI1137W Server Product not installed.\n'
        fi
    fi

    # Look into forcing upgrade only if Forcing upgrade is required
    if [ ${FORCEMIGREQ?} -eq ${TRUE?} ]; then
        if [ ${FORCEMIGR?} -eq ${FALSE?} ]; then
            display_msg ${DB2CAT?} 124 \
                'DBI1124E Instance %s cannot be upgraded.\n' ${INSTNAME?}
            stop_prog 1
        else
            CFGFILE=""
            CFGFILETYPE=""
            prepare_itype
            display_msg ${DB2CAT?} 140 'DBI1140W Command is being Forced.\n'
        fi
    fi
    
    # If forcing upgrade is required, push the -F into db2nrcfg
    if [ ${FORCEMIGR?} -eq ${TRUE?} ]; then
        NRCFGFLAGS="-F ${NRCFGFLAGS?}"
    fi

    if [ "X${INSTTYPE_KEEP_STR?}" != "X" ]; then
        NRCFGFLAGS="-s ${INSTTYPE_KEEP_STR?} ${NRCFGFLAGS?}"
    fi

    # If upgrade debug output is required, push the -d into create scripts
    if [ ${DEBUG?} -eq ${TRUE?} ]; then
        NRCFGFLAGS="-d ${NRCFGFLAGS?}"
    fi

    # Stop DB2 ACS services
    db2_acs_stop_disable  ${INSTNAME?} "stop" "${DB2IPRDDIR?}" "${DB2DIR?}/instance/db2nrupgrade"

    # call db2rmicons to remove existing DB2 desktop entries for the instance
    if [ -f ${INSTHOME?}/sqllib/bin/db2rmicons ]; then
       ${INSTHOME?}/sqllib/bin/db2rmicons 1>/dev/null 2>/dev/null
    fi
    
    # If we get this far, backup old sqllib directory and continue upgrade
    backup_old_sqllib

    # db2nrcfg may need to know what we're upgrading from.
    DB2NRCFG_MIGRATIONINSTTYPE=${INSTTYPE?}
    export DB2NRCFG_MIGRATIONINSTTYPE

    # Create an instances for the latest version of DB2
    dmsg "${PROGDIR?}/db2nrcfg ${NRCFGFLAGS?}"
    # Remove instance from instlist, this is necessary or db2nrcfg will error
    ${PROGDIR?}/db2iset -d ${INSTNAME?} 2>&1 1>/dev/null
    ${PROGDIR?}/db2nrcfg ${NRCFGFLAGS?} 2>&1 > ${TMPFILE2?}
    rc=$?
    prog=db2nrcfg

    if [ ${DEBUG?} -eq ${TRUE?} ]; then
        dmsg "    **** Start of output from ${prog?} command **** "
        if [ -s ${TMPFILE2?} ]; then
            cat ${TMPFILE2?} 2>&1 | tee -a ${DB2INSTLOG?}
        fi
        dmsg "    **** End of output from ${prog?} command **** "
    fi

    dmsg " Return code from ${prog?} = ${rc?} "
    if [ ${rc?} -ne 0 ] ; then
        display_msg ${DB2CAT?} 124 \
            'DBI1124E Instance %s cannot be upgraded.\n' ${INSTNAME?}
        stop_prog 1
    fi
    rm -f ${TMPFILE2?}

    # Copy of the database, node, and/or dcs directories
    copy_db_dirs

    # Upgrade all required files/dirs for the instance
    upgrade_instinfo

    # Copy UDFs
    copy_UDFs

    # Install/setup user exit program
    inst_userexit

    if [ -f ${INSTHOME?}/${OLDSQLLIB?}/cfg/db2cli.ini ]; then
        cp -p ${INSTHOME?}/${OLDSQLLIB?}/cfg/db2cli.ini \
                      ${INSTHOME?}/sqllib/cfg/db2cli.ini
        if [ $? -ne 0 ] ; then
            display_msg ${DB2CAT?} 86 \
                'DBI1086E An attempt to copy the file or directory %s to %s failed.\n' \
                ${INSTHOME?}/${OLDSQLLIB?}/cfg/db2cli.ini \
                ${INSTHOME?}/sqllib/cfg/db2cli.ini
        fi
    fi 

    if [ -f ${INSTHOME?}/${OLDSQLLIB?}/db2nodes.cfg ]; then
        cp -p ${INSTHOME?}/${OLDSQLLIB?}/db2nodes.cfg \
                      ${INSTHOME?}/sqllib/db2nodes.cfg
        if [ $? -ne 0 ] ; then
            display_msg ${DB2CAT?} 86 \
                'DBI1086E An attempt to copy the file or directory %s to %s failed.\n' \
                ${INSTHOME?}/${OLDSQLLIB?}/db2nodes.cfg \
                ${INSTHOME?}/sqllib/db2nodes.cfg
        fi
    fi 

    #check to see if instance has a token
    HAS_TOKEN=${FALSE?}
    if [ ${DEBUG?} -eq ${TRUE?} ]; then
       ${DB2DIR?}/instance/db2ickts -d -k ${INSTNAME?} 2>&1 1> /dev/null
    else 
       ${DB2DIR?}/instance/db2ickts -k ${INSTNAME?} 2>&1 1> /dev/null
    fi
    if [ $? -eq ${TRUE?} ]; then
        HAS_TOKEN=${TRUE?}
    fi

    #check to see if text search is started and stop it if so
    TEXT_SEARCH_STARTED=${FALSE?}
    ${DB2DIR?}/instance/db2ickts -t ${INSTNAME?} 2>&1 1> /dev/null
    if [ $? -eq ${TRUE?} ]; then
        TEXT_SEARCH_STARTED=${TRUE?}
        ##stop text search or upgrade will fail
       if [ ${DEBUG?} -eq ${TRUE?} ]; then
          ${DB2DIR?}/instance/db2ickts -d -s ${INSTNAME?} 2>&1 1> /dev/null
       else
          ${DB2DIR?}/instance/db2ickts -s ${INSTNAME?} 2>&1 1> /dev/null
       fi   
    fi

    #Copy over old tss files if there are any
    if [ ${HAS_TOKEN?} -eq ${FALSE?} ]; then
        ${DB2DIR?}/install/db2ls -q | grep TEXT_SEARCH 2> /dev/null 1> /dev/null
        if [ $? -eq ${TRUE?} ]; then
            if [ -d ${INSTHOME?}/${OLDSQLLIB?}/db2tss/config ]; then
                tssHome="${INSTHOME?}/sqllib/db2tss"
                cp -pr ${INSTHOME?}/${OLDSQLLIB?}/db2tss/config ${tssHome?}
                ${tssHome?}/bin/configTool configureParams -configPath ${tssHome?}/config -installPath ${DB2DIR?}/db2tss 2>&1 1>/dev/null
            fi
        fi
    fi

    #upgrade db2audit.cfg
    if [ -f ${INSTHOME?}/${OLDSQLLIB?}/security/db2audit.cfg ]; then
        rm -f ${INSTHOME?}/sqllib/security/db2audit.cfg 2>&1 1>/dev/null
        cp -p ${INSTHOME?}/${OLDSQLLIB?}/security/db2audit.cfg ${INSTHOME?}/sqllib/security
        ${INSTHOME?}/sqllib/bin/db2imaudcfg
        if [ $? -ne 0 ] ; then
            rm -f ${INSTHOME?}/sqllib/security/db2audit.cfg 2>&1 1>/dev/null
            if [ -f ${DB2DIR?}/security64/db2audit.cfg ]; then
                copyfile ${DB2DIR?}/security64 ${INSTHOME?}/sqllib/security db2audit.cfg
            elif [ -f ${DB2DIR?}/security32/db2audit.cfg ]; then
                copyfile ${DB2DIR?}/security32 ${INSTHOME?}/sqllib/security db2audit.cfg
            fi
            if [ -f ${INSTHOME?}/sqllib/security/db2audit.cfg ]; then
                mkpermission "u=rw,go=r" ${INSTHOME?}/sqllib/security/db2audit.cfg
            fi
        fi
    fi

    #copy back auditdata
    if [ -d ${INSTHOME?}/${OLDSQLLIB?}/security/auditdata -a -d ${INSTHOME?}/sqllib/security/auditdata ]; then
        cp -prf ${INSTHOME?}/${OLDSQLLIB?}/security/auditdata ${INSTHOME?}/sqllib/security 2>&1 1>/dev/null
    fi

    # Update instance registry
    update_registry

    ${DB2DIR?}/instance/db2iset -i ${INSTNAME?} DB2MEMMAXFREE=

    # Upgrade any registry settings, as required.
    nrupgrade_registry

    # If Relational Connect was present in the previous version, copy settings file
    if [ -f ${INSTHOME?}/${OLDSQLLIB?}/cfg/db2dj.ini ]; then
        cp ${INSTHOME?}/${OLDSQLLIB?}/cfg/db2dj.ini \
            ${INSTHOME?}/sqllib/cfg/db2dj.ini
        # If Relational Connect is not present in v8, issue a warning
        if [ ! -f ${DB2DIR?}/cfg/db2rcon.lvl ]; then
            display_msg ${DB2CAT?} 143 'DBI1143W Relational Connect not installed.\n'
        fi
    fi

    # If Classic Connect was present in the previous version, copy settings file
    if [ -f ${INSTHOME?}/${OLDSQLLIB?}/cfg/djxclassic2.cfg ]; then
        cp ${INSTHOME?}/${OLDSQLLIB?}/cfg/djxclassic2.cfg \
            ${INSTHOME?}/sqllib/cfg/djxclassic2.cfg
    fi

    # If SSLconfig.ini was in sqllib/cfg, copy it
    if [ -f ${INSTHOME?}/${OLDSQLLIB?}/cfg/SSLconfig.ini ]; then
        copyfile ${INSTHOME?}/${OLDSQLLIB?}/cfg ${INSTHOME?}/sqllib/cfg SSLconfig.ini
    fi

    # Move any db dir log files to sqllib/db2dump directory
    clean_nrupgrade_logs "SAVE"

    # if old mmm config file, copy it.
    if [ -f ${INSTHOME?}/${OLDSQLLIB?}/db2md_config.xml ]
    then
        cp ${INSTHOME?}/${OLDSQLLIB?}/db2md_config.xml ${INSTHOME?}/sqllib
    fi

    # If the original instance is 32-bit, we need to link sqllib/lib to lib32
    if [ "X${BitWidth_M?}" = "X32" ]; then
       cd ${INSTHOME?}/sqllib
       rm -f ${INSTHOME?}/sqllib/lib 2>&1 1>/dev/null
       ln -s lib32 ${INSTHOME?}/sqllib/lib
    fi

    # Upgrade DB2 ACS
    for license_file in ${INSTHOME?}/${OLDSQLLIB?}/acs/${ACS_LIC?} ; do break; done
    if [ -f ${license_file?} \
         -a -d ${INSTHOME?}/${OLDSQLLIB?}/acs \
         -a -d ${INSTHOME?}/sqllib/acs ]; then
       rm -rf ${INSTHOME?}/sqllib/acs 
       cp -prf ${INSTHOME?}/${OLDSQLLIB?}/acs ${INSTHOME?}/sqllib
    fi

    if [ ! -f ${license_file?} \
           -a -d ${INSTHOME?}/${OLDSQLLIB?}/acs \
           -a -d ${INSTHOME?}/sqllib/acs ]; then
       cd ${INSTHOME?}/${OLDSQLLIB?}/acs
       find . -type d | \
           while read dirname; do
                 if [ ! -d ${INSTHOME?}/sqllib/acs/${dirname?} ]
                 then
                    cp -prf ${INSTHOME?}/${OLDSQLLIB?}/acs/${dirname?} ${INSTHOME?}/sqllib/acs 
                 fi
           done
       find . -type f | \
           while read filename; do
                 if [ ! -f ${INSTHOME?}/sqllib/acs/${filename?} ]
                 then
                    cp -pf ${INSTHOME?}/${OLDSQLLIB?}/acs/${filename?} ${INSTHOME?}/sqllib/acs/${filename?}
                 fi
           done
    fi

    # copy the old instance.log
    if [ -f ${INSTHOME?}/${OLDSQLLIB?}/log/instance.log ]; then
        cp -pf ${INSTHOME?}/${OLDSQLLIB?}/log/instance.log ${INSTHOME?}/sqllib/log
    else
        rm -f ${INSTHOME?}/sqllib/log/instance.log
    fi
		
 	db2set -cleanup
		
    trax nrupgrade_instance
}

#Check if the upgrade is done, a flag is set in .instuse if it is done
chk_upgraded()
{
    trac chk_upgraded $@
    set ${setopts?}
    chk_upgraded_rc=${FALSE?}
    query_instuse ${INSTNAME?} "Upgraded" ""
    if [ ${INSTUSE_VALUE?} ]; then
        chk_upgraded_rc=${TRUE?}
    fi
    trax chk_upgraded
    return ${chk_upgraded_rc?}
}

#set a flag in .instuse after upgrade is done
mark_upgraded()
{
    trac mark_upgraded $@
    set ${setopts?}
    update_instuse ${INSTNAME?} "Upgraded" "0"

    ##restart text search if we stopped it earlier
    if [ ${TEXT_SEARCH_STARTED?} -eq ${TRUE?} ]; then
       # Start db2ts after upgrade is complete
       rc=`db2ts start for text` 
       if [ $? -ne 0 ]; then
         if [ ${DEBUG?} -eq ${TRUE?} ]; then
           dmsg "${rc?}"
         fi
       fi 
    fi

    trax chk_upgraded
    return ${TRUE?}
}

#-----------------------------------------------------------------------#
#                End of function definitions
#-----------------------------------------------------------------------#

#-----------------------------------------------------------------------#
#                             Main program
#-----------------------------------------------------------------------#

defaultopts        # Define default values for command-line options

# Process command-line options
case $# in
    0)
        syntax ;;
    *)
        while getopts :a:hdFkb:j: optchar; do
            case ${optchar?} in
                a)  # Authentication type
                    AUTHTYPE="${OPTARG?}" ;;
                d)  # Turn debug mode on
                    DEBUG=0
                    setopts="-x" ;;
                F)  # Force instance upgrade, unsupported
                    FORCEMIGR=0 ;;
                k)  # Keep the instance type during update
                    KEEPITYPE=${TRUE?} ;;
                #j) text_search,servicename,portnumber
                b) OLDSQLLIBDIR="${OPTARG?}" ;;
                ?)  # Display syntax
                    cleanup
                    syntax ;;
            esac
        done
esac

set ${setopts?}

if [ -z "${OLDSQLLIBDIR?}" ]; then
    syntax
fi

# LD_LIBRARY_PATH can cause problems during upgrade - remove it.
unset LD_LIBRARY_PATH

trap "stop_prog 2" 1 2 3 15
cleanup                      # Clean up

#check if upgrade is done
chk_upgraded
if [ $? -eq ${TRUE?} ]; then
    stop_prog 1
fi

# Initialize the instance upgrade process
init_nrupgrade

# Upgrade DB2 instance
nrupgrade_instance

#mark upgrade is done
mark_upgraded

# Successful exit
stop_prog 0

