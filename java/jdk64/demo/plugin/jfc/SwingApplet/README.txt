# ========================================================================
#  Licensed Materials - Property of IBM
#  "Restricted Materials of IBM"
#
#  IBM SDK, Java(tm) Technology Edition, v8
#  (C) Copyright IBM Corp. 2000, 2014. All Rights Reserved
#
#  US Government Users Restricted Rights - Use, duplication or disclosure
#  restricted by GSA ADP Schedule Contract with IBM Corp.
# ========================================================================
applet, as long as the Swing classes are in the browser's class
path.  In the Java 2 platform, Standard Edition, the Swing classes
are core, and thus are always in the class path.  For example,
you can run SwingApplet with this command:

   appletviewer SwingApplet.html

These instructions assume that this installation's version of appletviewer
command is in your path.  If it isn't, then you should either
specify the complete path to the appletviewer command or update
your PATH environment variable as described in the installation
instructions for the Java(TM) SE Development Kit.

