function noWebAccess() {
	// check for local IC
	var tempIC_url = getCurrentVersionUrl();

	if (tempIC_url != "") {	// if local found
		document.getElementById('LocalICMessage').style.display = "block";
		document.getElementById('RemoteICMessage').style.display = "none";
		document.getElementById('NoAccessMessage').style.display = "none";
		// set link URLs to local IC
		g_sICURL4Links = tempIC_url;
		document.getElementById('LocalICURL').innerHTML = g_sICURL4Links;
	} else {	// try remote IC
		var tempIC_url = getUnknownVersionUrl();
		// make sure it isn't pointing to publib
		var pattern_publib = /publib/i;
		if (tempIC_url.match(pattern_publib)) {
			tempIC_url = "";
		}
		if (tempIC_url != "") {	// if remote found (and not publib)
			document.getElementById('LocalICMessage').style.display = "none";
			document.getElementById('RemoteICMessage').style.display = "block";
			document.getElementById('NoAccessMessage').style.display = "none";
			// set link URLs to remote IC
			g_sICURL4Links = tempIC_url;
			document.getElementById('RemoteICURL').innerHTML = g_sICURL4Links;
		} else {	// else no connection to an IC
			document.getElementById('LocalICMessage').style.display = "none";
			document.getElementById('RemoteICMessage').style.display = "none";
			document.getElementById('NoAccessMessage').style.display = "block";
			// set link URLs to online IC
			// defaults to online URL anyway
		}
	}
}


function getFSCheckImageURL()
{
	//return g_sICURL4Links.replace(/index\.jsp/, "topic") + "/com.ibm.db2.luw.fs.doc/doc/fscheck.gif";
	var knwCenterLink = "http://www-01.ibm.com/support/knowledgecenter/js/dojo/resources/blank.gif";
    return knwCenterLink;
}

function linkToGettingStarted()
{
	var pattern_publib = /publib/i;
	var topic_path = "/com.ibm.db2.luw.fs.doc/doc/fsGettingStarted.html"; // default non-web topic
	if (g_sICURL4Links.match(pattern_publib)) {
		// use web accessible topic instead
		topic_path = "/com.ibm.db2.luw.fs.doc/doc/fsGettingStarted.html";
	}
	openWindow(g_sICURL4Links + '?topic=' + topic_path);
}

function linkToDownloadTools()
{
	var pattern_publib = /publib/i;
	var topic_path = "/com.ibm.db2.luw.fs.doc/doc/fsGetTools.html"; // default non-web topic
	if (g_sICURL4Links.match(pattern_publib)) {
		// use web accessible topic instead
		topic_path = "/com.ibm.db2.luw.fs.doc/doc/fsGetTools.html";
	}
	openWindow(g_sICURL4Links + '?topic=' + topic_path);
}

function linkToLearnTools()
{
	var pattern_publib = /publib/i;
	var topic_path = "/com.ibm.db2.luw.fs.doc/doc/fsAboutTools.html"; // default non-web topic
	if (g_sICURL4Links.match(pattern_publib)) {
		// use web accessible topic instead
		topic_path = "/com.ibm.db2.luw.fs.doc/doc/fsAboutTools.html";
	}
	openWindow(g_sICURL4Links + '?topic=' + topic_path);
}

function linkToLicense()
{
	var pattern_publib = /publib/i;
	var topic_path = "/com.ibm.db2.luw.qb.server.doc/doc/c0061199.html"; // default non-web topic
	if (g_sICURL4Links.match(pattern_publib)) {
		// use web accessible topic instead
		topic_path = "/com.ibm.db2.luw.qb.server.doc/doc/c0061199.html";
	}
	openWindow(g_sICURL4Links + '?topic=' + topic_path);
}


function showMainContent()
{
	// hide connection error paragraphs
	document.getElementById('LocalICMessage').style.display = "none";
	document.getElementById('RemoteICMessage').style.display = "none";
	document.getElementById('NoAccessMessage').style.display = "none";

	// set links to use publib IC initially
	g_sICURL4Links = getPublibUrl();

	// check if we can show the product update button
	if (isUpdateServiceInstalled()) {
		document.getElementById('productUpdatesLink').style.display = "block";
	}
	else {
		document.getElementById('productUpdatesLink').style.display = "none";
	}

	// check if we can show the create SAMPLE database button
	if (isDB2SAMPLInstalled()) {
		document.getElementById('New_CreateSAMPLEDatabase').style.display = "block";
	}
	else {
		document.getElementById('New_CreateSAMPLEDatabase').style.display = "none";
	}
}

function initialize()
{
  showMainContent();
}
