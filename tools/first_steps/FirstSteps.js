var g_bConpeInstalled = false;
var g_sPublibICUrl = "";
var g_sCurrentVersionICUrl = "";
var g_sUnknownVersionICUrl = "";
var g_sICURL4Links = "";	// IC URL variable used for links
var g_bWinIA64 = false;
var g_bInitialized = false;

function exit()
{
  window.close();
}

function isWindows()
{
  return (navigator.platform.indexOf("Win") >= 0);
}

function isLinux()
{
  return (navigator.platform.indexOf("Linux") >= 0);
}

function isLinux32()
{
  return (navigator.platform.indexOf("Linux i686") >= 0);
}

function isLinuxAMD64()
{
  return (navigator.platform.indexOf("Linux x86_64") >= 0);
}

function isWindowsIA64()
{
  if (isWindows()) {
    if (!g_bInitialized)
    {
      initializeFromTempFile();
    }
    return g_bWinIA64;
  }
  else {
    return false;
  }
}

function fileExists(filename)
{
  if (isIE()) {
    try {
      activex = new ActiveXObject("Scripting.FileSystemObject");
      return activex.FileExists(filename);
    } catch(e) {
      alert("exception " + e.name + ': ' + e.message);
    }
  }
  else {
    // for Mozilla, Firefox, and Netscape
    try {
      netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
      file = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsILocalFile);
      file.initWithPath(filename);
      return file.exists();
    } catch(e) {
      alert("exception " + e.name + ': ' + e.message);
    }
  }
}

function getTempDir()
{
  var path;

  try {
    if (isIE()) {
      var fso = new ActiveXObject("Scripting.FileSystemObject");
      path = fso.GetSpecialFolder(2);
    }
    else {
      netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
      DIR_SERVICE = new Components.Constructor("@mozilla.org/file/directory_service;1","nsIProperties");
      path = (new DIR_SERVICE()).get("TmpD", Components.interfaces.nsIFile).path;
    }
  }
  catch(e) {
    alert("exception " + e.name + ': ' + e.message);
  }

  return path;
}

function getUserId()
{
  var user;

  if (!isIE()) {
    try {
      netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
      var userInfo = Components.classes["@mozilla.org/userinfo;1"].createInstance(Components.interfaces.nsIUserInfo);
      user = userInfo.username;
    }
    catch(e) {
      alert("exception " + e.name + ': ' + e.message);
    }
  }

  return user;
}

function initializeFromTempFile()
{
  var tempStr = "";
  var path = "";

  if (isWindows()) {
    path = getTempDir() + "\\db2fs.js";
  }
  else
  {
    path = getTempDir() + "/db2fs_" + getUserId() + ".js"
  }

  if (fileExists(path)) {
    if (isIE()) {
      var fso = new ActiveXObject("Scripting.FileSystemObject");
      var f = fso.OpenTextFile(path, 1);
      tempStr = f.ReadAll();
    }
    else {
      try {
        netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
        var f = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsILocalFile);
      	f.initWithPath(path);
        var inputStream = Components.classes["@mozilla.org/network/file-input-stream;1"].createInstance(Components.interfaces.nsIFileInputStream);
        inputStream.init(f, 0x01, 00004, null);
  			var sInputStream = Components.classes["@mozilla.org/scriptableinputstream;1"].createInstance(Components.interfaces.nsIScriptableInputStream);
  			sInputStream.init(inputStream);

  			tempStr = sInputStream.read(sInputStream.available());
  		}
  		catch(e) {
        alert("exception " + e.name + ': ' + e.message);
        return;
      }
    }

    var tempArray = tempStr.split(";");
    for ( i=0; i<tempArray.length; i++) {
      var sKey = "";
      var sValue = "";
      var tempArray2 = tempArray[i].split(",");

      sKey = tempArray2[0];
      if (tempArray2.length > 1) {
        sValue = tempArray2[1];
      }

      if (sKey == "conpe" && sValue == "Y") {
        g_bConpeInstalled = true;
      }
      else if (sKey == "publibICUrl"){
        g_sPublibICUrl = sValue;
      }
      else if (sKey == "currentVersionICUrl") {
        g_sCurrentVersionICUrl = sValue;
      }
      else if (sKey == "unknownVersionICUrl") {
        g_sUnknownVersionICUrl = sValue;
      }
      else if (sKey == "winia64" && sValue == "Y") {
        g_bWinIA64 = true;
      }
    }
  }

  g_bInitialized = true;
}

function isESEInstalled()
{
  if (isWindows()) {
    return fileExists(getDB2Path() + "\\cfg\\db2ese.lvl");
  }
  else {
    return fileExists(getDB2Path() + "/cfg/db2ese.lvl");
  }
}

function isCONPEInstalled()
{
  if (!g_bInitialized)
  {
    initializeFromTempFile();
  }

  if (isWindows()) {
    return fileExists(getDB2Path() + "\\cfg\\db2client.lvl") && g_bConpeInstalled;
  }
  else {  // Unix
    return fileExists(getDB2Path() + "/cfg/db2client.lvl") && g_bConpeInstalled;
  }

}

function isConnectInstalled()
{
  if (isWindows()) {
    return (fileExists(getDB2Path() + "\\cfg\\db2consv.lvl") || isCONPEInstalled());
  }
  else {
    return (fileExists(getDB2Path() + "/cfg/db2consv.lvl") || isCONPEInstalled());
  }
}

function isServerInstalled()
{
  if (isWindows()) {
    return (fileExists(getDB2Path() + "\\cfg\\db2ese.lvl") ||
            fileExists(getDB2Path() + "\\cfg\\db2wse2.lvl") ||
            fileExists(getDB2Path() + "\\cfg\\db2wse.lvl") ||
            fileExists(getDB2Path() + "\\cfg\\db2exp.lvl") ||
            fileExists(getDB2Path() + "\\cfg\\db2pe.lvl"));
  }
  else {
    return (fileExists(getDB2Path() + "/cfg/db2ese.lvl") ||
            fileExists(getDB2Path() + "/cfg/db2wse2.lvl") ||
            fileExists(getDB2Path() + "/cfg/db2wse.lvl") ||
            fileExists(getDB2Path() + "/cfg/db2exp.lvl") ||
            fileExists(getDB2Path() + "/cfg/db2pe.lvl"));
  }
}

function isConnectInstalledOnly()
{
  return (isConnectInstalled() && !isServerInstalled());
}

function isADCLInstalled()
{
  if (isWindows()) {
    return fileExists(getDB2Path() + "\\cfg\\db2client.lvl");
  }
  else {
    return fileExists(getDB2Path() + "/cfg/db2client.lvl");
  }
}

function isADCLInstalledOnly()
{
  return (isADCLInstalled() && !isServerInstalled() && !isConnectInstalled() && !isADMCLInstalled());
}

function isADMCLInstalled()
{
  if (isWindows()) {
    return fileExists(getDB2Path() + "\\cfg\\db2admcl.lvl");
  }
  else {
    return fileExists(getDB2Path() + "/cfg/db2admcl.lvl");
  }
}

function isDB2SAMPLInstalled()
{
  if (isWindows()) {
    return fileExists(getDB2Path() + "\\bin\\db2sampl.exe");
  }
  else {
    return fileExists(getDB2Path() + "/bin/db2sampl");
  }
}

function isUpdateServiceInstalled()
{
  if (isWindows()) {
    return fileExists(getDB2Path() + "\\cfg\\install\\ibmupdateutil.exe");
  }
  else {
    
    return fileExists(getDB2Path() + "/bin/db2updserv");
  }
}

function isNonRootInstance()
{
  if (!isWindows()) {
    return fileExists(getDB2Path() + "/global.reg");
  }
  else {
    // non-root is Unix only
    return false;
  }
}

function getPublibUrl()
{
  if (!g_bInitialized)
  {
    initializeFromTempFile();
  }

  return g_sPublibICUrl;
}

function getCurrentVersionUrl()
{
  if (!g_bInitialized)
  {
    initializeFromTempFile();
  }

  return g_sCurrentVersionICUrl;
}

function getUnknownVersionUrl()
{
  if (!g_bInitialized)
  {
    initializeFromTempFile();
  }

  return g_sUnknownVersionICUrl;
}

function getDB2Path()
{
  var topdir = top.document.location.href;
  var i = topdir.lastIndexOf("/");

  if (i<7) {
    // this means topdir is only "file://"
    document.write("file not found");
  }

  if (topdir.substring(0,8) == "file:///") {
    if (isWindows()) {
      topdir = topdir.substring(8); // get rid of "file:///"
    }
    else {
      topdir = topdir.substring(7); // get rid of "file://"
    }
  }

  var i = topdir.lastIndexOf("/");
  topdir = topdir.substring(0, i); //get rid of the filename
  i = topdir.lastIndexOf("/");
  topdir = topdir.substring(0, i); //go up 1 directory
  i = topdir.lastIndexOf("/");
  topdir = topdir.substring(0, i); //go up another 1 directory
  i = topdir.lastIndexOf("/");
  topdir = topdir.substring(0, i); //go up another 1 directory
  i = topdir.lastIndexOf("/");
  topdir = topdir.substring(0, i); //go up another 1 directory

  // replace '/' with '\' on Windows
  if (isWindows()) {
    var i = topdir.indexOf("/");
    while (i >= 0) {
      topdir = topdir.substring(0,i) + "\\" + topdir.substring(i+1);
      i = topdir.indexOf("/");
    }

    // replace %20 with space. Path on Windows allows to have space in it.
    i = topdir.indexOf("%20");
    while (i >= 0) {
      topdir = topdir.substring(0,i) + " " + topdir.substring(i+3);
      i = topdir.indexOf("%20");
    }
  }

  return topdir;
}

function isIE()
{
  return (navigator.appName.indexOf("Microsoft Internet Explorer") >= 0);
}

function execute(command, commandParam, numOfParam)
{
  if (isIE()) {
    try {
      activexShell = new ActiveXObject("Shell.Application");
      activexShell.ShellExecute(command, commandParam, "", "open", "1");
    } catch (e) {
      alert("exception " + e.name + ': ' + e.message);
    }
  }
  else {
    try {
      netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
      var FileFactory = new Components.classes("@mozilla.org/file/local;1","nsILocalFile","initWithPath");
      var program = new FileFactory(command);
      var process = Components.classes["@mozilla.org/process/util;1"].createInstance(Components.interfaces. nsIProcess);
      process.init(program);
      process.run(false, commandParam, numOfParam, {});
    } catch (e) {
      alert("exception " + e.name + ': ' + e.message);
    }
  }
}


// Action functions

function searchICAction()
{
  alert("search: " + document.searchICForm.textfield.value);
}

function createSampleDBAction()
{
  if (isWindows()) {
    if (isIE()) {
      execute(getDB2JAVITPath(), "-j:FirstSteps.FSCreateSampleDBDialog -c:.;..\\java\\Common.jar;..\\tools\\first_steps\\db2fs.jar");
    }
    else {
      execute(getDB2JAVITPath(), ["-j:FirstSteps.FSCreateSampleDBDialog", "-c:.;..\\java\\Common.jar;..\\tools\\first_steps\\db2fs.jar"], 2);
    }
  }
  else {
    execute(getDB2JAVITPath(), ["-j:FirstSteps.FSCreateSampleDBDialog", "-c:.:../java/Common.jar:../tools/first_steps/db2fs.jar"], 2);
  }
}

function productUpdateAction()
{
  if (isWindows()) {
    if (isIE()) {
      execute(getDB2JAVITPath(), "-j:FirstSteps.FSProgramLauncher -c:.;..\\java\\Common.jar;..\\tools\\first_steps\\db2fs.jar -a:\"-u\"");
    }
    else {
      execute(getDB2JAVITPath(), ["-j:FirstSteps.FSProgramLauncher", "-c:.;..\\java\\Common.jar;..\\tools\\first_steps\\db2fs.jar", "-a:\"-u\""], 3);
    }
  }
  else {
      execute(getDB2JAVITPath(), ["-j:FirstSteps.FSProgramLauncher", "-c:.:../java/Common.jar:../tools/first_steps/db2fs.jar", "-a:\"-u\""], 3);
  }
}

function getDB2JAVITPath()
{
  var db2javitPath = getDB2Path();
  if (isWindows()) {
    db2javitPath += "\\bin\\db2javit.exe";
  }
  else {
    db2javitPath += "/bin/db2javit";
  }

  return db2javitPath;
}

function openWindow(webLink)
{
  window.open(webLink);
}

// delete the temp First Steps profile if the browser is Mozilla, Firefox, Netscape
if (!isIE()) {
  try {
  netscape.security.PrivilegeManager.enablePrivilege("UniversalXPConnect");
  var profileManager = Components.classes["@mozilla.org/profile/manager;1"];
  var profile = profileManager.getService(Components.interfaces.nsIProfile);
  profile.deleteProfile("DB2_FIRSTSTEPS",false);
  } catch(e) {
    // alert("exception " + e.name + ': ' + e.message);
  }
}
