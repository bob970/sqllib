function displayVersion()
{
  document.getElementById('versionString').innerHTML = "11.5";
}

function displayLPVersion()
{
	var version = "11.5.0.0";
	
	//Replace anything labeled as a versionArea class
	var tags = document.getElementsByTagName("span");	
	for(var i = 0; i < tags.length; i++) 
	{
		if(tags[i].className.match("versionArea")) 
		{
			tags[i].innerHTML = version;	
		}
		
	}
}

function displayLPCopyRightYear()
{
	var year = "2019";
	
	//Replace anything labeled as a versionArea class
	var tags = document.getElementsByTagName("span");	
	for(var i = 0; i < tags.length; i++) 
	{
		if(tags[i].className.match("yearArea")) 
		{
			tags[i].innerHTML = year;	
		}
	}
}

function displayPrimaryProduct(){
	
	var primaryProd=findPrimaryProduct().toUpperCase();	
	var tags = document.getElementsByTagName("span");	
	
	for(var i = 0; i < tags.length; i++) 
	{
		if(tags[i].className.match("primaryProduct")) 
		{
                        tags[i].innerHTML = property(primaryProd); 
		}		
	}

}

function findPrimaryProduct(){

  var numvars = 0; 
  var i=0;
  var isInst, isAvail;
  var prod;
  var PrimaryProd;
  var j=0;
  if ((top.instArray != undefined) || (top.instArray != null)) {
    numvars = top.instArray.length;
  }
  
  for (i=0; i<numvars; i++) {
    if (top.instArray[i][0] != undefined) {
      //Get name of product, remove unusual characters
      prod = top.instArray[i][0].valueOf();
      if (prod.charCodeAt(0) == 13) {  prod = prod.substr(1);    }
      if (prod.charCodeAt(0) == 10) {  prod = prod.substr(1);    }
  
      isInst = top.instArray[i][1];
      isAvail = top.availArray[i][1];
  		if (j==0 && isAvail==1){
  	  	    PrimaryProd=prod;    	  	      	    
  	  	    j=1;
  	  	}
    }
  }	  	
  return PrimaryProd;
}

