# NLS_MESSAGEFORMAT_NONE 
INFORMIX_SVRKIND=Informix
INFORMIX_WRAP_DESC=Provides support for the federated database to access Informix databases.

#Wrapper 
INFORMIX_WOPTS_DB2_FENCED_DESC=Specifies whether the wrapper runs in fenced mode or trusted mode. The default is 'N'; the wrapper runs in trusted mode.

INFORMIX_WOPTS_DB2_UM_PLUGIN_DESC=Specifies a case-sensitive string for the class name that corresponds to the user mapping repository class. For example: "UserMappingRepositoryLDAP".

#Wrapper and Server Options
INFORMIX_DB2_UM_PLUGIN_LANG_DESC=Specifies the language for the user-mapping plug-in. Valid values are 'Java' or 'C'. The default is 'Java'. 

# Server
INFORMIX_SVROPTS_NODE_DESC=Specifies the name by which the data source is defined as an instance to its relational database management system.

INFORMIX_SVROPTS_PASSWORD_DESC=Specifies whether passwords are sent to a data source. 'Y' specifies that passwords are always sent to the data source and validated.

INFORMIX_SVROPTS_DBNAME_DESC=Specifies the alias for the database that you want to access.

INFORMIX_SVROPTS_PUSHDOWN_DESC=Specifies whether the federated server allows the data source to evaluate operations. The default is 'Y'. 'N' indicates that the federated server retrieves columns from the remote data source and does not allow the data source to evaluate operations.

INFORMIX_SVROPTS_DB2_MAXIMAL_PUSHDOWN_DESC=Specifies whether the query optimizer chooses an access plan that pushes down more query operations to the data source. The default value is 'N'; the query optimizer does not push down more query operations but instead chooses the plan that has the lowest estimated cost.

INFORMIX_SVROPTS_FOLD_ID_DESC=Specifies the case for the user ID that is sent to the data source. If no value is specified, the federated server sends the user ID in uppercase; then if the uppercase user ID fails, the server sends the user ID in lowercase.

INFORMIX_SVROPTS_FOLD_PW_DESC=Specifies the case for the password that is sent to the data source. If no value is specified, the federated server sends the password in uppercase; then if the uppercase password fails, the server sends the password in lowercase.

INFORMIX_SVROPTS_COLLATING_SEQUENCE_DESC=Specifies whether the data source and the federated database use the same default collating sequence. To guarantee correct results from the federated server, specify 'I', which indicates that the data source is insensitive to case.

INFORMIX_SVROPTS_COMM_RATE_DESC=Specifies the communication rate, in megabytes per second, between the federated server and the data source server. Valid values are greater than 0 and less than 2147483648. You must express the value as a whole number, for example, 12.

INFORMIX_SVROPTS_CPU_RATIO_DESC=Specifies how much faster or slower the CPU of the data source runs when compared to the speed of the federated server's CPU. Valid values are greater than 0 and less than 1x10 23. Values can be expressed in any valid double notation, for example, 123E10, 123, or 1.21E4.

INFORMIX_SVROPTS_IO_RATIO_DESC=Specifies how much faster or slower the CPU of the data source runs when compared to the speed of the federated server's CPU. Valid values are greater than 0 and less than 1x10 23. Values can be expressed in any valid double notation, for example, 123E10, 123, or 1.21E4.

INFORMIX_SVROPTS_IUD_APP_SVPT_ENFORCE_DESC=Specifies whether the federated server enforces the use of application savepoints. 'Y' specifies that the federated server enforces savepoints and prevents INSERT, UPDATE and DELETE operations on nicknames when the data source does not support application savepoints.

INFORMIX_SVROPTS_INFORMIX_LOCK_MODE_DESC=Specifies the lock mode for a data source. Valid values are 'W', 'N', or a number. The default is 'W'; the wrapper waits an unlimited amount of time for the lock to be released. 'N' specifies do not wait; an error is returned immediately. Use a number to specify the maximum amount of time, in seconds, to wait. 

INFORMIX_SVROPTS_DB2_UM_PLUGIN_DESC=Specifies a case-sensitive string for the class name that corresponds to the user mapping repository class. For example, "UserMappingRepositoryLDAP".

INFORMIX_SVROPTS_DB2_TWO_PHASE_COMMIT_DESC=Specifies whether the federated server is enabled for two-phase commit transactions with the data source server.

INFORMIX_SVROPTS_DB2_MAX_ASYNC_REQUESTS_PER_QUERY_DESC=Specifies the maximum number of concurrent asynchronous requests from a query. Valid values are from -1 to 64000. -1 specifies that the federated query optimizer determines the number of requests. 0 specifies that the data source cannot accommodate additional asynchronous requests.

INFORMIX_SVROPTS_CLIENT_LOCALE_DESC=Specifies the CLIENT_LOCALE to use for the connection between the federated server and the data source server. Any valid Informix locale is valid. If this option is not specified, the CLIENT_LOCALE environment variable is set to the value specified in the db2dj.ini file or to the Informix locale that most closely matches the code page and territory of the federated database.

INFORMIX_SVROPTS_DB_LOCALE_DESC=Specifies the DB_LOCALE to use for the connection between the federated server and the data source server. Any valid Informix locale is valid. If this option is not specified, the DB_LOCALE environment variable is set to the value specified in the db2dj.ini file. If the db2dj.ini file does not specify a value, the DB_LOCALE environment variable is not set.

INFORMIX_SVROPTS_OLD_NAME_GEN_DESC=Specifies how to convert the column names and index names that are in the data source into nickname column names and local index names for the federated database. 'N' specifies that the generated names closely match the names in the data source. 'Y' specifies that the generated names are the same as the names that were created in version 9 and earlier. Thus, the names might not closely match the data source names. The default is 'N'.

#User 
INFORMIX_USROPTS_REMOTE_AUTHID_DESC=Specifies the remote user ID to which you are mapping the local user ID. Valid settings include any string that has a length of 255 or fewer characters. If this option is not specified, the ID used to connect to the federated database is used.

INFORMIX_USROPTS_REMOTE_PASSWORD_DESC=Specifies the remote password for remote user ID. Valid settings include any string that has a length of 32 characters or fewer. If this option is not specified, no password is sent to the data source.

#Column Options
INFORMIX_COLOPTS_NUMERIC_STRING_DESC=Specifies whether the column contains numeric strings that include blanks. 'Y' specifies that the column does not contain any blanks, which interfere with the sorting of the column data. 

#Discover 

#Common options
INFORMIX_COMMONOPTS_INFORMIXDIR_DESC=Specifies the directory path where the Informix Client SDK software is installed. This option is required.

INFORMIX_COMMONOPTS_INFORMIXSERVER_DESC=Specifies the name of the default Informix server. This option is required. On UNIX, the value must be a valid entry in the sqlhosts file. On Microsoft Windows, the value must be an SQLHOSTS registry key.

INFORMIX_COMMONOPTS_INFORMIXSQLHOSTS_DESC=Specifies the full path to the Informix sqlhosts file. You must specify this option if you do not use the default path for the Informix sqlhosts file. On UNIX, the default path is $INFORMIXDIR/etc. On Windows, if the SQLHOSTS registry key does not reside on the local computer, then the INFORMIXSQLHOSTS value is the name of the Windows system that stores the registry.

INFORMIX_COMMONOPTS_CLIENT_LOCALE_DESC=Specifies the code page to use for accessing Informix data. This option is optional. If the CLIENT_LOCALE variable is not set, the wrapper determines the code page and territory of the federated database. The wrapper sets the Informix environment variable CLIENT_LOCALE to the closest matching Informix locale. If there is no closely matching locale, the wrapper uses a default setting. On UNIX, the variable is set to en_us.8859-1. On Windows, the variable is set to en_us.CP1252. To obtain the list of valid Informix locales, issue the glfiles command on the Informix server.

INFORMIX_COMMONOPTS_DB_LOCALE_DESC=Specifies the name of the Informix database locale. Specify this option when the Informix database and the client local use different code pages and you want Informix to perform the conversion between the two code pages. This option is optional.

INFORMIX_COMMONOPTS_DBNLS_DESC=Specifies that Informix verify that the DB_LOCALE setting matches the actual locale of the Informix database. 1 specifies that verification occurs.
