#!/bin/ksh

################################################################
#
# Global variables
#
export LANG=C
export CT_LOCAL_SCOPE=1
export CT_PEER_SCOPE=2
export CT_DEFAULT_SCOPE=$CT_PEER_SCOPE
export CT_MANAGEMENT_SCOPE=$CT_DEFAULT_SCOPE

typeset HOSTNAME=$(/bin/hostname -s)
typeset FORCED_OFFLINE_FILE="/var/db2/db2_gpfs_forced_offline_${HOSTNAME}"
typeset IGNORE_ANY_MOUNT_FILE="/var/mmfs/etc/ignoreAnyMount"

typeset OS="$(uname)"
typeset IP_ADDRESS=""
typeset SUBNET_IP_ADDRESS=""
typeset SUBNET=""
typeset IS_UNMOUNT_ON_DISK_FAIL_SET=""
typeset IS_TB_HOST=""

typeset -i exit_rc=0
typeset -i exists=0
typeset -i cleanup=0
typeset -i runAddResources=0
typeset -i runDeleteResources=0
typeset -i runVerifyResources=0

export CR_BASE=condresp
export CR_CURRENT_VER=V115
export CR_BASE_VER=${CR_BASE}${CR_CURRENT_VER}

export RESP_LOG=/var/db2/${CR_BASE_VER}_resp.log
export RESP_SCR=${CR_BASE_VER}_resp.ksh
export RESP_NAME=${CR_BASE_VER}_${HOSTNAME}_response
export COND_BASE=${CR_BASE_VER}_${HOSTNAME}_condition
export outFile=/var/db2/${CR_BASE_VER}_setup.out

# Determine RSCT installation location
typeset RSCT="/opt/rsct"
if [ ! -d $RSCT/bin ]; then
    RSCT="/usr/sbin/rsct"
fi

export POLICY_DIR=$RSCT/sapolicies/db2

DB2CLUSTER="db2cluster -cfs -network_resiliency"

################################################################
#
# log a mesage to the system log. 
#
typeset logname="$(basename $0)[$$]"
log()
{
    priority=$1
    shift
    logger -p $priority -t $logname "$@"
    if [[ "err" == "$priority" ]]; then
        logger -p $priority -t $logname  "A failure to configure monitoring of CFS network resources occurred."
        logger -p $priority -t $logname  "This may cause a cluster failure if CFS network connectivity is lost."
        logger -p $priority -t $logname  "See /var/db2/condrespV115_setup.out for details"
        logger -p $priority -t $logname  "The following command can be invoked as root to attempt to resolve the problem:"
        logger -p $priority -t $logname  "db2cluster -cfs -network_resiliency -repair -all"

        echo "$@"
        echo "A failure to configure monitoring of CFS network resources occurred."
        echo "This may cause a cluster failure if CFS network connectivity is lost."
        echo "The following command can be invoked as root to attempt to resolve the problem:"
        echo "db2cluster -cfs -network_resiliency -repair -all"
        echo
        echo "=========================================================================="
        echo "= setupCondResp $@ on ${HOSTNAME} complete with errors at $(date +%D-%T) "
        echo "=========================================================================="
        exit 1
    fi

    if [[ "warn" == "$priority" ]]; then
        logger -p $priority -t $logname  "A warning was detected while configuring monitoring of CFS network resources."
        logger -p $priority -t $logname  "See /var/db2/condrespV115_setup.out for details"
        logger -p $priority -t $logname  "The following command can be invoked as root to attempt to resolve the problem:"
        logger -p $priority -t $logname  "db2cluster -cfs -network_resiliency -repair -all"

        echo
        echo "$@"
        echo  "A warning was detected while configuring monitoring of CFS network resources."
        echo  "The following command can be invoked as root to attempt to resolve the problem:"
        echo  "db2cluster -cfs -network_resiliency -repair -all"
        echo
    fi
}

################################################################
#
#
setupCondRespHelp()
{
    IFUP="ifconfig <ifc> up"
    IFDOWN="ifconfig <ifc> down"

    echo "
NAME
    ${CR_BASE_VER}_setup.ksh - Setup condition/response objects

SYNOPSIS
    ${CR_BASE_VER}_setup.ksh [-a] [-d] [-g] [-h]

DESCRIPTION
    Setup, delete, or list the various CFS network resiliency condition/response objects
    The script will log it's output to /var/db2/${CR_BASE_VER}_setup.out

    NOTE: This script should not be run manually. It is intended to be invoked
    by the db2cluster command. Each option lists the corresponding db2cluster command.

    -a
        db2cluster -cfs -network_resiliency [-gpfsadpter] -add [-all]
        Create condition/response objects for the CFS adapter.
        The CFS adapter is identified via the mmsdrquery command.
        The root user must run this command.
        The event script used is ${POLICY_DIR}/${CR_BASE_VER}_resp.ksh
        Reaction to $IFUP and $IFDOWN will be logged to the system log using the logger command.
        Refer to the OS man pages for instructions to setup the system log.

    -d
        db2cluster -cfs -network_resiliency [-gpfsadpter] -delete [-all]
        Delete all CFS network resiliency condition/response objects.
        Objects are deleted before processing -a or -g options.
        Only objects on the local host are deleted.
        The response script and log files will not be removed.

    -l
        db2cluster -cfs -network_resiliency [-gpfsadpter] -list [-all]
        List all the CFS network resiliency condition response object names in the peer domain.

    -L
        db2cluster -cfs -network_resiliency [-gpfsadpter] -list -resources [-all]
        List all the CFS network resiliency  condition response object resources in the peer domain.

    -v
        Verify that the network_resiliency resources exist and are correct.

    -h | -?
        Print this help text and exit without processing any other options.

    <no input options>
        List all existing DB2 condition/response objects and then
        print the help text.


"
    exit 0
}

################################################################
#
# Use $CR_BASE so that all versions are seen
#
dumpCFSResouceNames()
{
    export  CT_MANAGEMENT_SCOPE=${CT_PEER_SCOPE}
    echo
    echo "====> Conditions <===="
    echo
    lscondition | grep ${CR_BASE} | cut -d '"' -f 2 | sort
    echo
    echo "===> Responses <===="
    echo
    lsresponse  | grep ${CR_BASE} | cut -d '"' -f 2 | sort
    echo
    echo "====> Associations <===="
    echo
    lscondresp  | grep ${CR_BASE} | cut -d '"' -f 2,4 | sort | tr '"' ' '
    echo
}

################################################################
#
# Use $CR_BASE so that all versions are seen
#
dumpCFSResouces()
{
    export  CT_MANAGEMENT_SCOPE=${CT_PEER_SCOPE}

    echo
    echo "====> Conditions <===="
        lscondition ${CR_BASE}
    echo
    echo "===> Responses <===="
        lsresponse ${CR_BASE}
    echo
    echo "====> Associations <===="
        lscondresp ${CR_BASE}
    echo
}

################################################################
#
#
#
rmForcedOffline()
{
    if [[ -f $FORCED_OFFLINE_FILE ]]; then
        log notice "$LINENO: rm -f $FORCED_OFFLINE_FILE"
        rm -f $FORCED_OFFLINE_FILE
    fi
}

################################################################
#
#
#
setupPath()
{
    if (( runAddResources )); then
        if [[ ! -d /usr/lpp/mmfs/bin ]]; then
            log err "$LINENO: Error: /usr/lpp/mmfs/bin missing"
        fi
    fi

    if [[ ! -x /usr/bin/lsrsrc ]]; then
        log err "$LINENO: Error lsrsrc command is not executable"
    fi

    if [[ ! -d ${POLICY_DIR} ]]; then
        log err "$LINENO: Error: ${POLICY_DIR} does not exist"
    fi

    if [[ ! -d /var/db2 ]]; then
        log err "$LINENO: Error: /var/db2 log directory does not exist"
    fi

    export PATH=$PATH:/bin:/usr/bin:/usr/lpp/mmfs/bin/:
}

################################################################
#
#
isTbHost()
{
    # Similar to sqlhaGetHostsFromRSCT(), we will check that:
    # /usr/lpp/mmfs/bin/mmlsconfig unmountOnDiskFail returns yes for localhost and
    # /var/mmfs/etc/ignoreAnyMount file exists to confirm that localhost is tiebreaker host.
    #
    # Sample Output:
    # [root@coralm200]# /usr/lpp/mmfs/bin/mmlsconfig -Y unmountOnDiskFail
    # mmlsconfig::HEADER:version:reserved:reserved:configParameter:value:nodeList:
    # mmlsconfig::0:1:::unmountOnDiskFail:no::
    # mmlsconfig::0:1:::unmountOnDiskFail:yes:coralm200:
    IS_UNMOUNT_ON_DISK_FAIL_SET=$(/usr/lpp/mmfs/bin/mmlsconfig -Y unmountOnDiskFail | grep yes | grep ${HOSTNAME})

    if [[ -e ${IGNORE_ANY_MOUNT_FILE} && -n ${IS_UNMOUNT_ON_DISK_FAIL_SET} ]] ; then
        log debug "$LINENO: $HOSTNAME is a tiebreaker-host. Debug Info: $IS_UNMOUNT_ON_DISK_FAIL_SET."
        IS_TB_HOST=true
    else
        log debug "$LINENO: $HOSTNAME is not a tiebreaker-host. Debug Info: $IS_UNMOUNT_ON_DISK_FAIL_SET."
        IS_TB_HOST=false
    fi
}

################################################################
#
#
findCFSAdapter()
{
    typeset PS4="findCFSAdapter -> "
    typeset -i rc
    typeset LONG_CFS_HOSTNAME=""
    typeset SHORT_CFS_HOSTNAME=""
    typeset option="$1"
    typeset -i numAdapters=0
    typeset TEMP_COND_NAME=""
    typeset TEMP_IP_ADDRESS=""

    echo
    echo "================================================================"
    echo
    if [[ -z ${SUBNET_COND_NAME} ]] ; then
        # If the subnets parameter is set in GPFS then monitor the adapter on
        # that subnet
        SUBNET=$(/usr/lpp/mmfs/bin/mmlsconfig subnets | cut -d ' ' -f 2)

        if [[ (-n $SUBNET) && ($SUBNET != "(undefined)") ]] ; then
            echo "==== ${HOSTNAME} uses Subnet=${SUBNET} for CFS ===="
            
            #
            # Display the resource
            #
            echo "The RMC resource is"
            set -x
            lsrsrc -s "Subnet == '${SUBNET}' && NodeNameList == '${HOSTNAME}'" IBM.NetworkInterface
            rc=$?
            set +x
            if (( rc )); then
                log err "$LINENO: Error $rc returned by lsrsrc"
            fi
            echo

            ADAPTER=$(lsrsrc -s "Subnet == '${SUBNET}' && NodeNameList == '${HOSTNAME}'" IBM.NetworkInterface Name | grep Name | cut -d '"' -f2)

            if [[ -z ${ADAPTER} ]] ; then
               log info "$LINENO: CFS on $HOSTNAME has no adapters on subnet $SUBNET. This may be due to the fact that the host was intentionally not part of this subnet (for example for a tiebreaker host in a Geographically Dispersed pureScale Cluster (GDPC) or else that the subnets GPFS configuration parameter was incorrectly set. Will attempt to find another adapter on this host."
            else
                # For now, if there are multiple adapters on the subnet then 
                # just choose the first one. However, in future if we support
                # multiple adapters on a single subnet then we will need to 
                # create a condition for each adapter. We will also need to 
                # store the configuration someplace - maybe 
                # /var/db2/condrespV115.cfg (which would be a single point of 
                # failure). Then the response script would have to be updated 
                # to handle the configuration - which ones to ignore, when to 
                # disable GPFS in the case of multiple adapters, etc.

                NUM_ADAPTERS=$(echo ${ADAPTER} |wc -w)
                if [[ $NUM_ADAPTERS > 1 ]]
                then
                   FIRST_ADAPTER=$(echo ${ADAPTER} | cut -d ' ' -f1)
                   log warn "$LINENO: Found more than one adapter on subnet $SUBNET on host $HOSTNAME. These adapters are \""${ADAPTER}"\". We will use the first adapter found, \"$FIRST_ADAPTER\"."
                   ADAPTER=$FIRST_ADAPTER
                fi

                SUBNET_IP_ADDRESS="$(lsrsrc -s "Name == '${ADAPTER}' && NodeNameList == '${HOSTNAME}' && Subnet == '${SUBNET}'" IBM.NetworkInterface IPAddress | grep IPAddress | cut -d '"' -f2)"
                if [[ -z $SUBNET_IP_ADDRESS ]]
                then
                   log warn "$LINENO: Could not find an IP address for adapter $ADAPTER on host $HOSTNAME on subnet $SUBNET. Will attempt to find another adapter on this host."
                else
                   (( numAdapters += 1))

                   log debug "$LINENO: CFS on $HOSTNAME uses adapter $ADAPTER IP Address $IP_ADDRESS"
                   SUBNET_COND_NAME="${COND_BASE}_${ADAPTER}"
                fi
            fi
        fi
    fi

    if [[ -z ${COND_NAME} ]] ; then
        for line in `/usr/lpp/mmfs/bin/mmsdrquery sdrq_node_info sdrq_admin_interface:sdrq_daemon_ip_address  | cut -d ':' -f 7,8`; do

            LONG_CFS_HOSTNAME=$( echo $line | awk -F':' '{print $1}' )
            IP_ADDRESS=$( echo $line | awk -F':' '{print $2}' )

            if [[ -z $LONG_CFS_HOSTNAME ]]; then
                log err "$LINENO: CFS admin hostname not set - unable to create condresp resources"
            fi
            SHORT_CFS_HOSTNAME=$(echo ${LONG_CFS_HOSTNAME} | cut -d '.' -f 1)

            if [[ "$HOSTNAME" == "${SHORT_CFS_HOSTNAME}" ]]; then
                # This ip address belongs to this host
                echo "==== ${SHORT_CFS_HOSTNAME} uses IP=${IP_ADDRESS} for CFS ===="

                (( numAdapters += 1))

                #
                # Display the resource
                #
                echo "The RMC resource is"
                set -x
                lsrsrc -s "IPAddress == '${IP_ADDRESS}'" IBM.NetworkInterface
                rc=$?
                set +x
                if (( rc )); then
                    log err "$LINENO: Error $rc returned by lsrsrc"
                fi
                echo

                ADAPTER="$(lsrsrc -s "IPAddress == '${IP_ADDRESS}'" IBM.NetworkInterface Name | grep Name | cut -d '"' -f2)" # '

                log debug "$LINENO: CFS on $HOSTNAME uses adapter $ADAPTER IP Address $IP_ADDRESS"
                COND_NAME="${COND_BASE}_${ADAPTER}"
                break
            fi
        done
    fi

    # Check if localhost is tiebreaker host
    isTbHost

    # If $SUBNET is defined, then $SUBNET_COND_NAME should exist except for tiebreaker host.
    if [[ ( ( -n $SUBNET ) && ( $SUBNET != "(undefined)" ) ) && -z ${SUBNET_COND_NAME} && ( "${IS_TB_HOST}" == "false" ) ]] ; then
        log err "$LINENO: findCFSAdapter did not set SUBNET_COND_NAME"
    fi

    if (( numAdapters == 0 )); then
        # log a warning, but do not return one to db2cluster (no warnings += 1)
        # so that db2cluster does not issue a warning.
        log warn "$LINENO: This host is not part of a CFS domain"

        # Exit now since we can not continue creating resources.
        # Note that this will leave the response resource in place.
        exit 0
    fi

    # When subnet is NOT defined, numAdapters should be exactly one.
    # When subnet is defined (like GDPC), numAdapters will be exactly two.
    # Only allow these conditions to pass ahead.
    if ! [[ ( ( -n ${SUBNET_COND_NAME} && ( $numAdapters == 2 ) ) || ( -z ${SUBNET_COND_NAME} && ( $numAdapters == 1 ) ) ) ]]; then
        log err "$LINENO: Unexpected number of adapters encountered for CFS. Found $numAdapters adapters. Debug Info: $SUBNET_COND_NAME."
    fi

    if [[ -z ${COND_NAME} ]]; then
        log err "$LINENO: findCFSAdapter did not set COND_NAME"
    fi

    if [[ "-add" == "$option" ]]; then
        # Since the resources are being created, remove the forced offline file if it exits
        rmForcedOffline
        createCondition

        # We already added the condition response for public ethernet.
        # Now handle the private network defined by subnet if any.
        if [[ -n ${SUBNET_COND_NAME} ]]; then
           # Save the public ethernet values
           TEMP_COND_NAME=$COND_NAME
           TEMP_IP_ADDRESS=$IP_ADDRESS

           # Rather than changing the entire code under createCondition, we will pass in
           # subnet values using COND_NAME and IP_ADDRESS. We will revert them back once done.
           COND_NAME=$SUBNET_COND_NAME
           IP_ADDRESS=$SUBNET_IP_ADDRESS
           createCondition

           # Revert back the values before return
           COND_NAME=$TEMP_COND_NAME
           IP_ADDRESS=$TEMP_IP_ADDRESS
        fi
    fi
}

################################################################
#
#
showObjects()
{
    echo "================================================================"
    echo
    echo "DB2 Condition response objects"
    echo
    lscondition | grep ${CR_BASE} | sort
    echo
    lsresponse  | grep ${CR_BASE} | sort
    echo
    lscondresp  | grep ${CR_BASE} | sort
    echo
}

################################################################
#
#
deleteResources()
{
    typeset OBJ_LIST=""

    echo
    echo "================================================================"
    echo
    echo "==== Delete all $TYPE condition response objects ===="
    echo
    typeset RESP_LIST="$(lsresponse  -D " " | grep "${CR_BASE}.*${HOSTNAME}" | awk '{print $1}' | tr '"' ' ')" # '
    typeset COND_LIST="$(lscondition -D " " | grep "${CR_BASE}.*${HOSTNAME}" | awk '{print $1}' | tr '"' ' ')" # '

    echo RESP_LIST=$RESP_LIST
    echo COND_LIST=$COND_LIST

    for resp in $RESP_LIST; do
        for cond in $COND_LIST; do
            deleteCondResp $cond $resp
        done
    done

    for resp in $RESP_LIST; do
        deleteResponse $resp
    done
    for cond in $COND_LIST; do
        deleteCondition $cond
    done

    # Since the resources are being removed, remove the forced offline file if it exists
    rmForcedOffline
}

################################################################
#
#
typeset -i errors=0
typeset -i warnings=0

verifyResources()
{
    findCFSAdapter -verify
    verifyAllResources
}

verifyAllResources()
{
    export CT_MANAGEMENT_SCOPE=$CT_LOCAL_SCOPE
    typeset OBJ_BASE="${CR_BASE}"
    echo "================================================================"
    echo "                     Resource verification"

    verifyResource "condition"
    verifyResource "response"
    verifyResource "condresp"

    if [[ -f $FORCED_OFFLINE_FILE ]]; then
        echo "===> $FORCED_OFFLINE_FILE exists"
        (( warnings += 1 ))
    fi
    echo 
    if (( errors == 1 )); then
        log err "$LINENO: $errors error was detected"
    elif (( errors )); then
        log err "$LINENO: $errors errors were detected"
    elif (( warnings == 1)); then
        log warn "$LINENO: $warnings warning was detected"
    elif (( warnings )); then
        log warn "$LINENO: $warnings warnings were detected"
    else
        log notice "$LINENO: No errors or warnings were detected"
    fi

    if (( warnings )); then
        exit_rc=2
    fi
}


verifyResource()
{
    OBJ_TYPE=$1
    echo
    echo "Verify the network_resiliency $OBJ_TYPE resource"
    typeset numObjects="$(ls$OBJ_TYPE | grep ${OBJ_BASE} | wc -l)"

    # For $OBJ_TYPE "condition" and "condresp", if subnet is defined,
    # number of objects will be two. For all else (i.e. $OBJ_TYPE "response"), it should be one.
    if [[ ( $numObjects > 2 ) || ( ( $numObjects > 1 ) && ( -z ${SUBNET_COND_NAME} || $OBJ_TYPE == "response" ) ) ]]; then
        echo "===> Multiple $OBJ_TYPE resources exist on ${HOSTNAME}"
        ls$OBJ_TYPE | grep ${OBJ_BASE} | sort
        (( errors += 1 ))
    elif [[ $numObjects == 0 ]]; then
        echo "===> The $OBJ_TYPE resource does not exist on ${HOSTNAME}"
        (( errors += 1 ))
    else
        echo "-> There is exactly $numObjects $OBJ_TYPE resousrce"
    fi

    if [[ $OBJ_TYPE == "condition" ]]; then
        typeset OBJ_DATA="$(lscondition ${COND_NAME})"
        echo "$OBJ_DATA"

        # MonitorStatus contains valid and acceptable(warning) values
        verifyState MonitorStatus   "Monitored" "Error, monitored"
        verifyState ResourceClass   "IBM.NetworkInterface"
        verifyState Toggle          "Yes"
        verifyState EventExpression "OpState != 1"
        verifyState RearmExpression "OpState = 1"
        verifyState IPAddress       "${IP_ADDRESS}"
        verifyState Node            ${HOSTNAME}

        if [[ -n ${SUBNET_COND_NAME} ]]; then
            OBJ_DATA="$(lscondition ${SUBNET_COND_NAME})"
            echo "$OBJ_DATA"

            # MonitorStatus contains valid and acceptable(warning) values
            verifyState MonitorStatus   "Monitored" "Error, monitored"
            verifyState ResourceClass   "IBM.NetworkInterface"
            verifyState Toggle          "Yes"
            verifyState EventExpression "OpState != 1"
            verifyState RearmExpression "OpState = 1"
            verifyState IPAddress       "${SUBNET_IP_ADDRESS}"
            verifyState Node            ${HOSTNAME}
        fi
    elif [[ $OBJ_TYPE == "response" ]]; then
        typeset OBJ_DATA="$(lsresponse ${RESP_NAME})"
        echo "$OBJ_DATA"

        verifyState DaysOfWeek      "1-7"
        verifyState TimeOfDay       "0000-2400"
        verifyState ActionScript    "${POLICY_DIR}/${RESP_SCR}"
        verifyState ReturnCode      "0"
        verifyState CheckReturnCode "y"
        verifyState UndefRes        "y"
        verifyState Node            ${HOSTNAME}
        if [[ !  -x ${POLICY_DIR}/${RESP_SCR} ]]; then
            echo "===> ${POLICY_DIR}/${RESP_SCR} is not executable"
            (( errors += 1 ))
        else
            echo "-> ${POLICY_DIR}/${RESP_SCR} is executable"
        fi
    elif [[ $OBJ_TYPE == "condresp" ]]; then
        typeset OBJ_DATA="$(lscondresp ${COND_NAME} ${RESP_NAME})"
        echo "$OBJ_DATA"

        verifyState Condition "${COND_NAME}"
        verifyState Response  "${RESP_NAME}"
        verifyState State     "Active"
        verifyState Node      ${HOSTNAME}

        if [[ -n ${SUBNET_COND_NAME} ]]; then
            OBJ_DATA="$(lscondresp ${SUBNET_COND_NAME} ${RESP_NAME})"
            echo "$OBJ_DATA"

            verifyState Condition "${SUBNET_COND_NAME}"
            verifyState Response  "${RESP_NAME}"
            verifyState State     "Active"
            verifyState Node      ${HOSTNAME}
        fi
    else
        log debug "$LINENO: Unexpected object type $OBJ_TYPE specified."
    fi
}

verifyState()
{
    typeset fieldName="$1"
    typeset value="$2"
    typeset warnValue="$3"

    if [[ -z $value ]]; then
        echo "===> The value for ${fieldName} is unset"
        (( errors += 1 ))
    else
        typeset -i state=$(echo "$OBJ_DATA" | grep -w "${fieldName}" | grep -w "${value}" | wc -l)
        if (( state != 1 )); then
            if [[ -n $warnValue ]]; then
                typeset -i state=$(echo "$OBJ_DATA" | grep -w "${fieldName}" | grep -w "${warnValue}" | wc -l)
                if (( state == 1 )); then
                    echo "===> The ${fieldName} contains a warning value"
                    (( warnings += 1 ))
                else
                    echo "===> The ${fieldName} contains an error value"
                    (( errors += 1 ))
                fi
            else
                echo "===> The ${fieldName} contains an error value"
                (( errors += 1 ))
            fi
        else
            echo "-> The ${fieldName} is correct"
        fi
    fi
}

################################################################
# Response functions
################################################################
#
#
createResponse()
{
    typeset PS4="createResponse -> "
    typeset -i rc=0
    typeset -i createResponse=0

    echo
    echo "==== createResponse ${RESP_NAME} ===="
    echo
    #
    # Check to see if the response exists
    #
    set -x
    CT_MANAGEMENT_SCOPE=$CT_LOCAL_SCOPE lsresponse ${RESP_NAME}
    rc=$?
    set +x
    case $rc in
        (0)
        log notice "$LINENO: Response ${RESP_NAME} already exists"
        createResponse=0
        ;;

        (5)
        : Response does not exist
        createResponse=1
        ;;

        (*)
        log err "$LINENO: Error $rc returned by lsresponse"
        ;;
    esac

    #
    # Create the response
    #
    if (( createResponse )); then
        #
        # Create the response
        #
        log debug "$LINENO: Create ${RESP_NAME}"
        set -x
        mkresponse -n "${RESP_NAME} event handler" -s "${POLICY_DIR}/${RESP_SCR}" -u -e A -r 0 -p ${HOSTNAME} ${RESP_NAME}
        rc=$?
        set +x
        if (( rc )); then
            log err "$LINENO: Error $rc occurred creating response"
        fi
        #
        # Lock the response so it can not be accidentally removed
        #
        log debug "$LINENO: Lock ${RESP_NAME}"
        set -x
        chresponse -L ${RESP_NAME}
        rc=$?
        set +x
        if (( rc )); then
            log err "$LINENO: chresponse -L returned $rc"
        fi
    fi

    #
    # Display the response
    #
    set -x
    CT_MANAGEMENT_SCOPE=$CT_LOCAL_SCOPE lsresponse ${RESP_NAME}
    set +x
}

################################################################
#
#
deleteResponse()
{
    typeset PS4="deleteResponse -> "
    typeset OBJ_NAME="$1"
    typeset -i rc=0

    if [[ -z ${OBJ_NAME} ]]; then
        log err "$LINENO: Usage: deleteResponse responseName"
    fi

    lsresponse ${OBJ_NAME}

    echo "Removing ${OBJ_NAME}"
    # Ignore rc from chresponse
    log debug "$LINENO: Unlock ${OBJ_NAME}"
    set -x
    chresponse -U ${OBJ_NAME}
    rc=$?
    set -x
    if (( rc )); then
        log err "$LINENO: chresponse -U returned $rc"
    fi

    log debug "$LINENO: Remove response ${OBJ_NAME}"
    rmresponse -f ${OBJ_NAME}
    rc=$?
    set +x
    if (( rc )); then
        log err "$LINENO: Error $rc returned by rmresponse"
    fi
    echo
}    

################################################################
# Condition functions
################################################################
#
#
createCondition()
{
    typeset PS4="createCondition -> "
    typeset -i rc
    typeset -i createCondition=0

    echo "==== createCondition ${COND_NAME} using ${IP_ADDRESS} ===="
    echo
    #
    # Check to see if the condition exists
    #
    CT_MANAGEMENT_SCOPE=$CT_LOCAL_SCOPE lscondition ${COND_NAME}
    rc=$?
    case $rc in
        (0) 
        # log the fact that the condition exists
        # But continue making progress
        log notice "$LINENO: Condition ${COND_NAME} already exists"
        createCondition=0
        ;;

        (5)
        createCondition=1
        ;;

        (*)
        log err "$LINENO: Error $rc returned by lscondition"
        ;;
    esac

    #
    # Create the condition if it does not already exist
    #
    if (( createCondition )); then
        log debug "$LINENO: Create ${COND_NAME}"
        set -x
        mkcondition -r IBM.NetworkInterface -d 'Adapter is not online' -e 'OpState != 1' -D 'Adapter is online' -E 'OpState = 1' -m l -S c -s "IPAddress == '${IP_ADDRESS}'" ${COND_NAME}
        rc=$?
        set +x
        if (( rc )); then
            log err "$LINENO: Error $rc occurred creating condition"
        fi
        #
        # Lock the condition so it can not be accidentally removed
        #
        log debug "$LINENO: Lock ${COND_NAME}"
        set -x
        chcondition -L ${COND_NAME}
        rc=$?
        set +x

        if (( rc )); then
            log err "$LINENO: chcondition -L return $rc"
        fi
    fi

    #
    # Display the condition
    #
    echo
    set -x
    CT_MANAGEMENT_SCOPE=$CT_LOCAL_SCOPE lscondition ${COND_NAME}
    rc=$?
    set +x
    if (( rc )); then
        log err "$LINENO: Error $rc returned by lscondition"
    fi

    #
    # Link the response to the condition and activate it
    #
    createCondResp ${COND_NAME} ${RESP_NAME}
}

################################################################
#
#
deleteCondition()
{
    typeset PS4="deleteCondition -> "
    typeset -i rc=0

    typeset OBJ_NAME="$1"

    if [[ -z ${OBJ_NAME} ]]; then
        log err "$LINENO: Usage: deleteCondition conditionName"
    fi
    lscondition ${OBJ_NAME}

    # Ignore rc from chcondition 
    log debug "$LINENO: Unlock ${OBJ_NAME}"
    set -x
    chcondition -U ${OBJ_NAME}
    rc=$?
    set +x
    if (( rc )); then
        log err "$LINENO: chcondition -U returned $rc"
    fi

    log debug "$LINENO: Remove condition ${OBJ_NAME}"
    set -x
    rmcondition -f ${OBJ_NAME}
    rc=$?
    set +x
    if (( rc )); then
        log err "$LINENO: Error $rc returned by rmcondition"
    fi
    echo
}

################################################################
# Condition response functions
################################################################
#
#
createCondResp()
{
    typeset PS4="createCondResp -> "
    typeset -i rc=0
    typeset -i createCondResp=0

    typeset OBJ1_NAME=$1
    typeset OBJ2_NAME=$2

    if [[ -z ${OBJ1_NAME} || -z ${OBJ2_NAME} ]]; then
        log err "$LINENO: Usage: createCondResp conditionName responseName"
    fi

    set -x
    CT_MANAGEMENT_SCOPE=$CT_LOCAL_SCOPE lscondresp ${OBJ1_NAME} ${OBJ2_NAME}
    rc=$?
    set +x
    case $rc in
        (0)
        log notice "$LINENO: condresp ${OBJ1_NAME} ${OBJ2_NAME} already exists"
        createCondResp=0
        ;;

        (5)
        : condresp does not exist
        createCondResp=1
        ;;

        (*)
        log err "$LINENO: Error $rc returned by lscondresp"
        ;;
    esac

    if (( createCondResp )); then
        log debug "$LINENO: Create ${OBJ1_NAME} ${OBJ2_NAME} link"
        set -x
        mkcondresp ${OBJ1_NAME} ${OBJ2_NAME}
        rc=$?
        set +x
        if (( rc )); then
            log err "$LINENO: Error $rc returned by stopcondresp"
        fi

        activateCondResp ${COND_NAME} ${RESP_NAME}
    fi
}

################################################################
#
# There is no chcondresp. This is here to allow lock/unlock
#
chCondResp()
{
    typeset PS4="chCondResp -> "

    if [[ -z ${OBJ1_NAME} || -z ${OBJ2_NAME} ]]; then
        log err "$LINENO: OBJ1_NAME (condition) and OBJ2_NAME (response) must both be set"
    fi

    typeset op=""
    OPTIND=1
    while getopts LU opt
    do
        case $opt in
            (L) op="-L" ; log debug "$LINENO: Lock   ${OBJ1_NAME} ${OBJ2_NAME} link" ;;
            (U) op="-U" ; log debug "$LINENO: Unlock ${OBJ1_NAME} ${OBJ2_NAME} link" ;;
        esac
    done
    shift $(( OPTIND - 1 ))

    if [[ -z $op ]]; then
        log err "$LINENO: Usage chCondResp [-L | -U]"
    fi

    rmcondresp $op ${OBJ1_NAME} ${OBJ2_NAME}
    rc=$?
    set +x
    if (( rc && rc != 5 )); then
        log err "$LINENO: Error $rc returned by rmcondresp $op"
    fi
}

################################################################
#
#
deactivateCondResp()
{
    typeset PS4="deactivateCondResp -> "
    typeset -i rc=0

    typeset OBJ1_NAME=$1
    typeset OBJ2_NAME=$2

    if [[ -z ${OBJ1_NAME} || -z ${OBJ2_NAME} ]]; then
        log err "$LINENO: Usage: deactivateCondResp conditionName responseName"
    fi

    chCondResp -U

    log debug "$LINENO: Deactivate ${OBJ1_NAME} ${OBJ2_NAME} link"
    set -x
    stopcondresp ${OBJ1_NAME} ${OBJ2_NAME}
    rc=$?
    set +x
    if (( rc && rc != 5 )); then
        log err "$LINENO: Error $rc returned by stopcondresp"
    fi
    chCondResp -L

}

################################################################
#
#
activateCondResp()
{
    typeset PS4="activateCondResp -> "
    typeset -i rc=0

    typeset OBJ1_NAME=$1
    typeset OBJ2_NAME=$2

    if [[ -z ${OBJ1_NAME} || -z ${OBJ2_NAME} ]]; then
        log err "$LINENO: Usage: activateCondResp conditionName responseName"
    fi

    chCondResp -U

    log debug "$LINENO: Activate ${OBJ1_NAME} ${OBJ2_NAME} link"
    set -x
    startcondresp ${OBJ1_NAME} ${OBJ2_NAME}
    rc=$?
    set +x
    if (( rc && rc != 5 )); then
        log err "$LINENO: Error $rc returned by startcondresp"
    fi

    chCondResp -L
}

################################################################
#
#
deleteCondResp()
{
    typeset PS4="deleteCondResp -> "
    typeset -i active=0
    typeset -i rc=0

    typeset OBJ1_NAME=$1
    typeset OBJ2_NAME=$2
    if [[ -z ${OBJ1_NAME} || -z ${OBJ2_NAME} ]]; then
        log err "$LINENO: Usage: deleteCondResp conditionName responseName"
    fi

    set -x
    CT_MANAGEMENT_SCOPE=$CT_LOCAL_SCOPE lscondresp ${OBJ1_NAME} ${OBJ2_NAME}
    rc=$?
    set +x
    if (( rc )); then
        return
    fi

    deactivateCondResp ${OBJ1_NAME} ${OBJ2_NAME}

    chCondResp -U

    log debug "$LINENO: Remove condresp ${OBJ1_NAME} ${OBJ2_NAME} link"
    set -x
    rmcondresp ${OBJ1_NAME} ${OBJ2_NAME}
    rc=$?
    set +x
    if (( rc && rc != 5 )); then
        log err "$LINENO: Error $rc returned by rmcondresp"
    fi

}

################################################################
#
#
updateGpfsCallback()
{
    typeset PS4="updateGpfsCallback -> "
    typeset SHUTDOWN_CB_NAME="db2GpfsShutdown"
    typeset SHUTDOWN_CB_EVENT="shutdown"
    typeset SHUTDOWN_CB_PARMS="gpfs_shutdown"
    typeset SHUTDOWN_CB_CMD="$POLICY_DIR/condrespV115_resp.ksh"
    typeset CMD=UNKNOWN
    typeset NAME=UNKNOWN
    typeset EVENT=UNKNOWN
    typeset PARMS=UNKNOWN

    typeset -i callbackFound=0
    typeset -i callbackFoundForOtherVersions=0
    numCondresp=$(lscondition | grep -c condrespV115)

    set -x
    cmdOut=$(mmlscallback ${SHUTDOWN_CB_NAME} | grep -v "No callback identifiers")
    rc=$?
    set +x

    if (( 0 == rc )); then
        echo "$cmdOut
        "
        NAME=$( echo "$cmdOut" | head -1)
        CMD=$(  echo "$cmdOut" | grep command | awk '{print $3}')
        EVENT=$(echo "$cmdOut" | grep event   | awk '{print $3}')
        PARMS=$(echo "$cmdOut" | grep parms   | awk '{print $3}')

        if [[ "$NAME"  == "${SHUTDOWN_CB_NAME}"  &&
              "$EVENT" == "${SHUTDOWN_CB_EVENT}" &&
              "$CMD"   == "${SHUTDOWN_CB_CMD}"   &&
              "$PARMS" == "${SHUTDOWN_CB_PARMS}" ]]
        then
            # Found the DB2 shutdown callback
            callbackFound=1
        elif [[ "$NAME"  == "${SHUTDOWN_CB_NAME}"  &&
                "$EVENT" == "${SHUTDOWN_CB_EVENT}" &&
                "$PARMS" == "${SHUTDOWN_CB_PARMS}" ]]
        then
            # Found the DB2 shutdown callback for other versions
            callbackFoundForOtherVersions=1
        fi
    fi

    log debug "$LINENO: '$NAME' '$EVENT' '$CMD' '$PARMS'"
    log debug "$LINENO: '$runAddResources' '$runDeleteResources' '$numCondresp' '$callbackFoundForOtherVersions' '$callbackFound'"

    # If we found a GPFS callback of other levels and TSA condresp is non-zero or
    # If TSA condresp is 0 but GPFS callback is found
    # Remove it
    if [[ ( $runAddResources -eq 1 && $numCondresp -gt 0 && $callbackFoundForOtherVersions -eq 1 ) \
         || ( $runDeleteResources -eq 1 && $numCondresp -eq 0 && $callbackFound -eq 1 ) ]]
    then
        echo "Removing existing GPFS callback"
        set -x
        mmdelcallback $NAME
        rc=$?
        set +x
        if (( rc )); then
            log err "$LINENO: Error $rc removing gpfs shutdown callback $NAME"
        fi
    fi

    if (( $runAddResources && $numCondresp && !$callbackFound )); then
        echo "Adding GPFS callback"
        set -x
        mmaddcallback ${SHUTDOWN_CB_NAME} --sync --command ${SHUTDOWN_CB_CMD} \
            --priority 0 --event ${SHUTDOWN_CB_EVENT} --parms "${SHUTDOWN_CB_PARMS}"
        rc=$?
        set +x
        if (( rc )); then
            log err "$LINENO: Error $rc adding gpfs shutdown callback $NAME"
        fi
    fi
}

################################################################
################################################################
#
# main()
{
    typeset PS4="${CR_BASE_VER}_setup -> "

    if (( ! $# )); then
        showObjects
        setupCondRespHelp
    fi

    if (( 1 == $# )) && [[ "$1" == "-h" ]]; then
        setupCondRespHelp
    fi

    if [[ -z $setupCondRespLogging ]]; then
        # Restart so that output is logged
        export SRCDIR=$(dirname $0)
        export prog=$(basename $0)

        # This round of getopts will process help/list requests
        # without logging to $outFile. Commands that should log are
        # ignored here and will be processed in the next round.
        while getopts advlLh? opt
          do
          case $opt in
              (a)  ;; # Ignore
              (d)  ;; # Ignore
              (v)  ;; # Ignore
              (l) dumpCFSResouceNames ; exit 0 ;;
              (L) dumpCFSResouces ;     exit 0 ;;
              (h) setupCondRespHelp ;;
              (?) setupCondRespHelp ;;
          esac
        done

        if [[ "root" != "$(whoami)" ]]; then
            echo "Only root can invoke $prog - you are $(whoami)"
            exit -1
        fi

        touch $outFile
        chmod a+wr $outFile

        # Re-invoke the script such that output goes to $outFle
        export setupCondRespLogging=1
        log debug "$LINENO: $SRCDIR/$prog $*"
        $SRCDIR/$prog $* >> $outFile 2>&1
        typeset -i exit_rc=$?
        unset setupCondRespLogging
        if (( exit_rc == 1 )); then
            echo "Errors occurred - check system log and $outFile on $(hostname -s)"
            echo
        elif (( exit_rc == 2 )); then
            echo "Warnings occurred - check system log and $outFile on $(hostname -s)"
            echo
        fi
        exit $exit_rc
    fi

    # From this point forward, all output to stdout/stderr 
    # will be written to $outfile
    echo "================================================================"
    echo "= setupCondResp $@ on ${HOSTNAME} start at $(date +%D-%T) "
    echo "================================================================"

    OPTIND=1

    while getopts adv opt
    do
        case $opt in
            (a) runAddResources=1 ;;
            (d) runDeleteResources=1 ;;
            (v) runVerifyResources=1 ;;
            (*) echo "Invalid paramater"; exit -1 ;;
        esac
    done

    # Don't shift so the output has the args.
    # shift $(( OPTIND - 1 ))

    setupPath

    showObjects

    if (( ! runAddResources && ! runDeleteResources && ! runVerifyResources )); then
        echo "================================================================"
        setupCondRespHelp
    fi
        
    if (( runAddResources )); then
        createResponse
        findCFSAdapter -add
        verifyAllResources
    fi

    if (( runDeleteResources )); then
        deleteResources
    fi

    if (( runVerifyResources )); then
        verifyResources
    fi

    updateGpfsCallback

    showObjects

    if (( warnings )); then
        echo "=========================================================================="
        echo "= setupCondResp $@ on ${HOSTNAME} complete with warnings at $(date +%D-%T) "
        echo "=========================================================================="
        exit 2
    else
        echo "=========================================================================="
        echo "= setupCondResp $@ on ${HOSTNAME} complete at $(date +%D-%T) "
        echo "=========================================================================="
        exit 0
    fi
}
