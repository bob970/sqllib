#!/bin/ksh -p
#-----------------------------------------------------------------------
# (C) COPYRIGHT International Business Machines Corp. 2001-2009
# All Rights Reserved
#
# US Government Users Restricted Rights - Use, duplication or
# disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
#
# NAME: mountV115_stop.ksh /mount_point [force] [verbose]
#
# FUNCTION: Stop method for filesystem in TSA cluster
#
# Return 0 if filesystem is unmounted cleanly
# Do not return until filesystem is no longer mounted at this node
# Force option will skip the soft unmount
#
#-----------------------------------------------------------------------
#
# Write messages to the system log and mmfs.log.latest
#
log()
{
    priority=$1
    shift
    logger -p $priority -t $myNameAndPid "$@"

    # mmfs.log.latest must be a link. If it is not, then
    # do not write to it - the link is in process of being recreated.
    if [[ -L /var/adm/ras/mmfs.log.latest ]]; then
        echo "$(date): $myNameAndPid: $@" >> /var/adm/ras/mmfs.log.latest
    fi
}

#-----------------------------------------------------------------------

export PATH=/bin:/usr/bin:/sbin:$PATH
export LANG=C
export CT_MANAGEMENT_SCOPE=2

myname=$(basename $0)
dirname=$(dirname $0)
myNameAndPid="$(basename $0)[$$]"

MP=${1}

UNAME=$(uname)

# environment variable enabled by ISAS 
isVgManaged=$vgManaged

# Set the name of the forced offline file which may
# be created by the RSCT response script when the 
# GPFS adapter goes offline.
FORCED_OFFLINE_FILE="/var/db2/db2_gpfs_forced_offline_`hostname -s`"

log notice "$LINENO: Entered $MP"

if [[ $MP == "." || $MP == ".." || $MP == "/" ]] ; then
   log err "$LINENO: A bad mount point was provided $MP"
   return 3
fi

let force=0
if [[ "$2" == "force" ]]; then
   force=1
fi

if [[ "$2" == "verbose" || "$3" == "verbose" ]]; then
   typeset -ft $(typeset +f)
   set -x
else
   # Close stdout and stderr
   exec 2> /dev/null
   exec 1> /dev/null
   set +x
fi

if [[ -z $MP ]]; then
   log err "$LINENO: Error: Must specify mount point"
   return 2
fi

function isMounted
{
   let mounted=0

   # If we're already trying to force GPFS offline due to a failed GPFS
   # network adapter, just return not mounted so we don't attempt IO
   if [[ ! -e ${FORCED_OFFLINE_FILE} ]]; then
      if [[ "${UNAME}" == "AIX" ]]; then
         mounted=$(mount | awk '{print $2}' | grep -c "^${MP}\$")
      else
         mounted=$(mount | awk '{print $3}' | grep -c "^${MP}\$")
      fi
      if [[ -z $mounted ]]; then
         mounted=0
      fi
   fi
   return $mounted
}

function checkForInitProcess
{
   let initProcFound=0

   for pid in $1
   do
      if [ $pid == 1 ]; then
         initProcFound=1
         break
      fi
   done
}

if [[ $UNAME == "AIX" ]]; then
   #AIX
   until [[ -r /etc/filesystems ]]; do
      log err "$LINENO: Error: /etc/filesystems not readable"
      sleep 1
   done
   blockDevice=$(cat /etc/filesystems | grep -p ^${MP?}: | awk '{print $1 " " $2 " " $3}' | egrep "^dev = " | awk '{print $3}')
   fstype=$(cat /etc/filesystems | grep -p ^${MP?}: | awk '{print $1 " " $2 " " $3}' | egrep "^vfs = " | awk '{print $3}')
   if [[ "$fstype" == "mmfs" ]]; then
      fstype="gpfs"
   fi

   if [[ -z $blockDevice ]]; then
      # This is not a valid mount point, it may be an RLV
      blockDevice=${MP?}
      unset MP
      rlv=true
   fi


   lvname=${blockDevice#/dev/}
   vgname=$(lslv $lvname 2> /dev/null | grep "VOLUME GROUP" | awk '{print $6}')

   # For GPFS we will allow no VG/LV
   if [[ "$fstype" == "gpfs" ]]; then
      blockDevice="mmfs"
      lvname="mmfs"
      vgname="mmfs"
   else
      if [[ ! -z "$vgname" ]] ; then
         majNum=$(ls -l /dev/$vgname | tr "," " " | awk '{print $5}')
         diskID=$(lspv | grep " $vgname " | awk '{print $2}')
         hdiskID=$(lspv | grep " $vgname " | awk '{print $1}')
         lvname=${blockDevice#/dev/}
      else
         log err "$LINENO: Mount point $MP not defined at this host"
         isMounted
         exit $mounted
      fi
   fi
else
   blockDevice=$(cat /etc/fstab | awk '{print $1 " " $2}' | grep " ${MP}$" | awk '{print $1}')
   if [[ -z $blockDevice ]]; then
      if [[ -r /etc/fstab ]]; then
         log err "$LINENO: Mount point $MP not defined at this host"
         isMounted
         exit $mounted
      fi
   fi
fi

###################################################################
# Start main...
rc=0

if [[ "${rlv}" == "true" && "$fstype" != "gpfs" && -z $isVgManaged ]]; then
   # RLV
   varyoffvg ${vgname?}
   rc=$?
   return $rc
fi

isMounted
if [[ $mounted == 0 ]]; then
   log info "$LINENO: Filesystem ${MP} isn't mounted on this node" 
else
   # If rg exists for this MP, touch file and exit
   DATA_SVC_RG_NAME=$(echo "db2mnt"${MP?} | tr "/" "_")
   rc1=$(lsrsrc -s "Name == '${DATA_SVC_RG_NAME?}'" IBM.Test Name | grep -c ${DATA_SVC_RG_NAME?})
   if [[ $rc1 -eq 1 ]]; then
      touch ${MP}/.mp_pseudo
   else
      # Clean up dir and umount it
      rm -f ${MP}/.mp_monitor
      umount $MP
      doKillAlso=$?
      isMounted

      ((softUnmounts=0))
      # Only attempt soft unmount if 'force' option is not used
      while [ $force != 1 -a $mounted != 0 -a $softUnmounts != 10 ];
      do
         if [[ "${UNAME}" == "AIX" ]]; then
               exportfs -u ${MP}
               list_of_pids_to_be_killed=$(fuser -c $MP)
         else
               exportfs -u :${MP}
               list_of_pids_to_be_killed=$(fuser -m $MP)
         fi

         log notice "$LINENO: Following list of pids preventing $MP unmounting: $list_of_pids_to_be_killed" 
         sleep 10

         umount $MP
         doKillAlso=$?
         isMounted
         ((softUnmounts+=1))
      done

      # At this point, nice unmounting has not worked ... be more drastic
      skipSubsequentKill=false;

      while [ $mounted != 0 ];
      do
         if [[ "${UNAME}" == "AIX" ]]; then
            exportfs -u ${MP}
            list_of_pids_to_be_killed=$(fuser -c $MP)
         else
            exportfs -u :${MP}
            list_of_pids_to_be_killed=$(fuser -m $MP)
         fi

         checkForInitProcess "$list_of_pids_to_be_killed"

         if [[ $doKillAlso != 0 ]]; then 
            if [[ ! -z $list_of_pids_to_be_killed ]]; then
               if [ $initProcFound == 0 ]; then
                  log notice "$LINENO: Killing following list of pids for MP :$MP: $list_of_pids_to_be_killed"
                  if [[ "${UNAME}" == "AIX" ]]; then
                     fuser -ck $MP 
                  else
                     if [[ "${skipSubsequentKill}" == "true" ]] ; then
                        log err "$LINENO: GPFS filesystem detected, skipping fuser -k"
                     else
                        fuser -mk $MP
                     fi

                     fstype=$(cat /etc/fstab | grep " ${MP} " | awk '{ print $3 }')
                     if [[ "$fstype" == "gpfs" ]] ; then
                        log err "$LINENO: GPFS filesystem detected, skipping subsequent fuser -k"
                        skipSubsequentKill=true;   
                     fi
                  fi
               else
                  log notice "$LINENO: Init process found (pid = 1), skipping fuser -k"
               fi
            else
               # There are no PIDs holding this mount, yet umount fails 
               # Be even more drastic
               umount -f $MP
               temprc=$?
               isMounted
               if [ $mounted != 0 ]; then
                  log err "$LINENO: There are no PIDs preventing unmount, yet unmount $MP fails! Mount rc=$temprc" 
               fi
            fi
         fi

         umount $MP
         doKillAlso=$?
         sleep 1
         isMounted
         if [[ $mounted != 0 && $initProcFound == 0 ]]; then
            if [[ "${UNAME}" == "AIX" ]]; then
               fuser -ck $MP 
            else
               if [[ "${skipSubsequentKill}" == "true" ]] ; then
                  log err "$LINENO: GPFS filesystem detected, skipping fuser -k"
               else
                  fuser -mk $MP
               fi
            fi
         else
            log notice "$LINENO: Init process found (pid = 1), skipping fuser -k"
         fi
      done
   fi
fi

# Filesystem is unmounted or was not mounted
if [[ "${UNAME}" == "AIX" && "$fstype" != "gpfs" && -z $isVgManaged ]]; then
   varyoffvg ${vgname?}
   lspvLine=$(lspv -L | grep " $vgname ")
   vgactive=$(echo $lspvLine | awk '{print $4}')
   while [ "$vgactive" == "active" ];
   do
      sleep 1
      umount -f $MP
      openLV=$(lsvg -L ${vgname?} | grep "OPEN LVs" | awk '{print $3}')
      if [[ $openLV == 0 ]]; then
         varyoffvg ${vgname?}
      fi
      rc=$?
      lspvLine=$(lspv -L | grep " $vgname ")
      vgactive=$(echo $lspvLine | awk '{print $4}')
      log notice "$LINENO: Volume group $vgname is still active : $vgactive"
   done
fi

log notice "$LINENO: Returning $rc for $MP"
return $rc
