#!/bin/ksh -p
#-----------------------------------------------------------------------
# (C) COPYRIGHT International Business Machines Corp. 2001-2009
# All Rights Reserved
#
# US Government Users Restricted Rights - Use, duplication or
# disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
#
# INPUT:  hadrV115_monitor.ksh db2instp db2insts hadrdb [verbose]
#
# OUTPUT: 1 if online, 2 if offline, 0 if not known
#
# NOTES: Can only be used in the context of a TSA environment configured 
#        with the db2 HA Integrated Configuration Utility (db2haicu)
#
#-----------------------------------------------------------------------

# Determine RSCT installation location
typeset RSCT="/opt/rsct"
if [ ! -d $RSCT/bin ]; then
   RSCT="/usr/sbin/rsct"
fi

#
# Write messages to the system log
#
log()
{
   priority=$1
   shift

   logger -p $priority -t $PROGNAME "$@"
}

PATH=/bin:/usr/bin:/sbin:/usr/sbin:$RSCT/bin
export PATH
LANG=C
export LANG

PROGNAME="$(basename $0)[$$]"
log notice "$LINENO: Entering : $*"

PROGPATH=$(dirname $0)
DB2HADRINSTANCE1=${1?}
DB2HADRINSTANCE2=${2?}
dbname=${3?}
DB2HADRDBNAME=$(echo ${dbname?} | tr "[a-z]" "[A-Z]")
localhost=$(hostname)
VERBOSE=${4:-noverbose}

#log debug "$LINENO: PROGNAME: $PROGNAME,PROGPATH: $PROGPATH,DB2HADRINSTANCE1: $DB2HADRINSTANCE1, DB2HADRINSTANCE2: $DB2HADRINSTANCE2, dbname: $dbname, DB2HADRDBNAME: $DB2HADRDBNAME, VERBOSE: $VERBOSE"

if [[ "$VERBOSE" == "verbose" ]]; then
   typeset -ft $(typeset +f)
   set -x
else
   # Close stdout and stderr
   exec 2> /dev/null
   exec 1> /dev/null
   set +x
fi

CT_MANAGEMENT_SCOPE=2
export CT_MANAGEMENT_SCOPE

CLUSTER_APP_LABEL=db2_${DB2HADRINSTANCE1?}_${DB2HADRINSTANCE2?}_${DB2HADRDBNAME?}


###########################################################
# set_candidate_P_instance()
###########################################################
set_candidate_P_instance()
{
   candidate_P_instance=${DB2HADRINSTANCE1?}
   candidate_S_instance=${DB2HADRINSTANCE2?}
   localShorthost=$(hostname | tr "." " " | awk '{print $1}')

#log debug "$LINENO: candidate_P_instance: $candidate_P_instance, candidate_S_instance: $candidate_S_instance, localShorthost: $localShorthost"

   if [[ ${DB2HADRINSTANCE1?} != ${DB2HADRINSTANCE2?} ]]; then
      rm  -f ${PROGPATH?}/.${CLUSTER_APP_LABEL?}
      if [[ -f ${PROGPATH?}/.${CLUSTER_APP_LABEL?} ]]; then
         candidate_P_instance=$(cat ${PROGPATH?}/.${CLUSTER_APP_LABEL?})
         if [[ ${candidate_P_instance?} == ${DB2HADRINSTANCE1?} ]]; then
            candidate_S_instance=${DB2HADRINSTANCE2?}
         else
            candidate_S_instance=${DB2HADRINSTANCE1?}
         fi
      else
         nn1=$(lsequ -s "Name like '%${DB2HADRINSTANCE1?}#_${localShorthost?}%'" | grep "Resource:Node" | awk '{print $3}' | tr ":" " " | tr "{" " " | tr "}" " " | awk '{print $1}')
         if [[ -z "$nn1" ]] ; then
            nn1=$(lsequ -s "Name like '%${DB2HADRINSTANCE1?}#_0%'" | grep "Resource:Node" | awk '{print $3}' | tr ":" " " | tr "{" " " | tr "}" " " | awk '{print $1}')
         fi

         nn1s=$(echo $nn1 | tr "." " " | awk '{print $1}')
   
         if [[ ${nn1s?} == ${localShorthost?} ]]; then
            candidate_P_instance=${DB2HADRINSTANCE1?}
            candidate_S_instance=${DB2HADRINSTANCE2?}
         else
            candidate_P_instance=${DB2HADRINSTANCE2?}
            candidate_S_instance=${DB2HADRINSTANCE1?}
         fi
         echo ${candidate_P_instance?} > ${PROGPATH?}/.${CLUSTER_APP_LABEL?}
      fi
   fi

#log debug "$LINENO: candidate_P_instance: $candidate_P_instance, candidate_S_instance: $candidate_S_instance, localShorthost: $localShorthost, nn1: $nn1, nn1s: $nn1s"

   return 0
}


###########################################################
# monitorhadr()
###########################################################
monitorhadr()
{
   # Get path to home dir
   DB2INSTANCE=${candidate_P_instance?}
   export DB2INSTANCE
   if [[ -z ${INSTHOME} ]] ; then
      userhome=~${DB2INSTANCE?}
      eval userhome=$userhome
      INSTHOME=${userhome}
   fi

   firstChar=$(echo $INSTHOME | cut -c1-1)
   if [[ -z ${INSTHOME} || "$firstChar" != "/" ]] ; then
      INSTHOME=$(cat /etc/passwd | grep "^${DB2INSTANCE?}:" | cut -f 6 -d \:)
   fi

   if [[ -z ${INSTHOME} ]] ; then
      log error "$LINENO: There is no home directory defined for ${DB2INSTANCE?}"
      rc=2
      return $rc
   fi
   
   manual=-1
   grc=3
   while [ $grc -ne 0 -a $grc -ne 1 ];
   do
      # If IBM.Test resource exists OR reintegration flag file exists, run db2gcf
      usepd=$(lsrsrc -s "Name like 'db2#_${DB2HADRDBNAME?}#_%${candidate_P_instance?}%'" IBM.Test | grep -v "test_rg" |  grep -c "^resource ")
      flagFileNamePre=db2_${DB2HADRDBNAME?}
      flagFileNamePost1=Reintegrate_${candidate_P_instance?}_${candidate_S_instance?}
      flagFileNamePost2=Reintegrate_${candidate_S_instance?}_${candidate_P_instance?}
      flagFile=$(ls /tmp | grep $flagFileNamePre | grep -c $flagFileNamePost1)
      if [[ $flagFile -eq 0 ]]; then
         flagFile=$(ls /tmp | grep $flagFileNamePre | grep -c $flagFileNamePost2)
      fi 

      # if manual mode, or no state flags, can simply use db2pd to return HADR state ...

      # Use db2pd if in manual mode (manual = 1), OR 
      # if we're in auto mode (or if the mode is unknown), AND there are no IBM.Test resource and flag file
      if [[ $manual -eq 1 || ( ( $manual -eq 0 || $manual -eq -1 ) && ( $usepd -eq 0 && $flagFile -eq 0 ) ) ]]; then
         # for not activated; standby; standard; instance offline - return 2
         hdrRole=$(su - ${candidate_P_instance} -c "db2pd -hadr -db ${DB2HADRDBNAME?}" | egrep -i "= Primary|= Standby|not activated|not active" | head -1 | tr "[a-z]" "[A-Z]" | awk '{print $3}' | uniq)
         #log debug "$LINENO: manual mode, or no state flags, use db2pd: hdrRole:$hdrRole, manual:$manual, usepd:$usepd, flagFile:$flagFile"
         if [[ $hdrRole == "PRIMARY" ]]; then
            grc=0
         elif [[ $hdrRole == '' ]]; then
            # either instance is offline or we're hitting ASLR error
            p_pid=$(ps -u ${candidate_P_instance?} -o args | grep -v grep | grep -c db2sysc)
            if [[ $p_pid -eq 0 ]]; then
               # hadr is also offline on this host
               rc=2
               log notice "$LINENO: Returning $rc : instance is not available"
               exit $rc 
            fi
            # otherwise
            log notice "$LINENO: db2pd returns empty due to ASLR, try db2pd again"
         else # database is either Standby, Standard, or not activated 
            grc=1
            
            if [[ $hdrRole == "STANDBY" ]]; then
               isPeer=$(su - ${candidate_P_instance} -c "db2pd -hadr -db ${DB2HADRDBNAME?}" | grep -i "Peer" | grep -c -i "State" )
               if [[ $isPeer -gt 0 ]]; then
                  # STANDBY PEER state

                  # Delete reintegration flag if it exists
                  tmpReint=reint.${candidate_P_instance}.${DB2HADRDBNAME?}.out
                  hdrReint=$(ls /tmp | grep -c $tmpReint)
                  if [[ $hdrReint -ne 0 ]]; then
                     rm -f /tmp/${tmpReint?}
                  fi

                  # Check for 'Failed Offline' state and clear it if set
                  reset_failed_offline
               fi
            fi
         fi
      fi
      
      # If state change during the reading ... re-read
      usepd=$(lsrsrc -s "Name like 'db2#_${DB2HADRDBNAME?}#_%${candidate_P_instance?}%'" IBM.Test | grep -v "test_rg" |  grep -c "^resource ")
      flagFile=$(ls /tmp | grep $flagFileNamePre | grep -c $flagFileNamePost1)
      if [[ $flagFile -eq 0 ]]; then
         flagFile=$(ls /tmp | grep $flagFileNamePre | grep -c $flagFileNamePost2)
      fi 
      if [[ $usepd -ne 0 ]] ; then
         log debug "$LINENO: Must try reading with db2gcf"
      fi

      # If not in manual mode (or mode is unknown), AND
      # there are IBM.Test resource or flag file, OR db2pd was unsuccessful (grc=3),
      # use db2gcf
      if [[ ( $manual -eq 0 || $manual -eq -1 ) && ( $usepd -ne 0 || $flagFile -ne 0 || $grc -eq 3 )]]; then
         manual=$(lssamctrl | grep -c "Manual")
         if [[ $manual == 0 ]]; then
            log debug "$LINENO: auto mode, and state flags or db2pd fails, use db2gcf: manual:$manual, usepd:$usepd, flagFile:$flagFile, grc:$grc"
            # Want db2gcf to timeout prior to the monitor method timeout ... 
            # gcfTimeOut=15
            # Want db2gcf to timeout prior to the monitor method timeout ... 
            mcto=29
            mcto=$(lsrsrc -s "Name = 'db2_${candidate_P_instance?}_${candidate_S_instance?}_${DB2HADRDBNAME?}-rs' AND NodeNameList = '${localhost?}'" IBM.Application  MonitorCommandTimeout | grep MonitorCommandTimeout | awk '{print $3}')
            if [[ $mcto == "" ]]; then
               #try the other way
               mcto=$(lsrsrc -s "Name = 'db2_${candidate_S_instance?}_${candidate_P_instance?}_${DB2HADRDBNAME?}-rs' AND NodeNameList = '${localhost?}'" IBM.Application  MonitorCommandTimeout | grep MonitorCommandTimeout | awk '{print $3}')
            fi

            if [[ $mcto -lt 29 ]]; then
               mcto=29
            fi
            let gcfTimeOut=mcto/2
            su - ${candidate_P_instance?} -c "$INSTHOME/sqllib/bin/db2gcf -t ${gcfTimeOut?} -s -i ${candidate_P_instance?} -i ${candidate_S_instance?} -h ${DB2HADRDBNAME?}"
            grc=$?
            log debug "$LINENO: su - ${candidate_P_instance?} -c $INSTHOME/sqllib/bin/db2gcf -t ${gcfTimeOut?} -s -i ${candidate_P_instance?} -i ${candidate_S_instance?} -h ${DB2HADRDBNAME?} returns $grc"
         else
            log debug "$LINENO: Domain is placed in manual mode, skip db2gcf call."
         fi
      fi
   done


   if [[ $grc == 0 ]]; then
      # Only good db2gcf result maps to TSA ONLINE
      rc=1
   elif [[ $grc == 1 ]]; then
      # OFFLINE
      rc=2
   fi 
   
   return $rc
}
#######################################################
# cleanup_failed_flag()
#######################################################
cleanup_failed_flag()
{
   if [[ $DB2HADRINSTANCE1 > $DB2HADRINSTANCE2 ]]; then
      clusterInitiatedMoveRG=db2_${DB2HADRDBNAME?}_ClusterInitiatedMove_${DB2HADRINSTANCE2?}_${DB2HADRINSTANCE1?}
   else
      clusterInitiatedMoveRG=db2_${DB2HADRDBNAME?}_ClusterInitiatedMove_${DB2HADRINSTANCE1?}_${DB2HADRINSTANCE2?}
   fi
   REMOVE_CLUSTER_MOVE_FAILED_RG=$(echo "remove.RG.$clusterInitiatedMoveRG.failed")

   if [[ -e /tmp/${REMOVE_CLUSTER_MOVE_FAILED_RG} ]]; then
      lrc2=3
      ciMoveExist=$(lsrsrc -s "Name like '$clusterInitiatedMoveRG'" IBM.Test | grep -v "test_rg" |  grep -c "^resource ")
      if [ $ciMoveExist -ne 0 ]; then
         log info "$LINENO: ciMoveExist:$ciMoveExist, lrc2: $lrc2,removing clusterInitiatedMoveRG: ${clusterInitiatedMoveRG}"
         rmrsrc -s "Name like '$clusterInitiatedMoveRG'" IBM.Test
         lrc2=$?
      fi
      if [[ $ciMoveExist -eq 0 || $lrc2 -eq 0 ]]; then
         log debug "$LINENO: ciMoveExist:$ciMoveExist, lrc2: $lrc2,removing REMOVE_CLUSTER_MOVE_FAILED_RG: ${REMOVE_CLUSTER_MOVE_FAILED_RG}"
         rm -f /tmp/${REMOVE_CLUSTER_MOVE_FAILED_RG}
      fi
   fi
}

#######################################################
# reset_failed_offline()
#######################################################
reset_failed_offline()
{
   opState=$(lsrsrc -s "Name = 'db2_${candidate_P_instance?}_${candidate_S_instance?}_${DB2HADRDBNAME?}-rs' AND NodeNameList = '${localhost?}'" IBM.Application OpState | grep OpState | awk '{print $3}')

   # TSA resource state shows FAILED OFFLINE, reset it
   if [[ $opState -eq 3 ]]; then
      resetrsrc -s "Name = 'db2_${candidate_P_instance?}_${candidate_S_instance?}_${DB2HADRDBNAME?}-rs' AND NodeNameList = '${localhost?}'" IBM.Application
      log notice "$LINENO: resetrsrc for resource 'db2_${candidate_P_instance?}_${candidate_S_instance?}_${DB2HADRDBNAME?}-rs' issued on '${localhost?}'" 
   fi

   # corner case where standby and primary instance have different names
   if [[ ${candidate_S_instance?} != ${candidate_P_instance?} ]]; then
      opState=$(lsrsrc -s "Name = 'db2_${candidate_S_instance?}_${candidate_P_instance?}_${DB2HADRDBNAME?}-rs' AND NodeNameList = '${localhost?}'" IBM.Application OpState | grep OpState | awk '{print $3}')
      
      # TSA resource state shows FAILED OFFLINE, reset it
      if [[ $opState -eq 3 ]]; then
         resetrsrc -s "Name = 'db2_${candidate_S_instance?}_${candidate_P_instance?}_${DB2HADRDBNAME?}-rs' AND NodeNameList = '${localhost?}'" IBM.Application
         log notice "$LINENO: resetrsrc for resource 'db2_${candidate_S_instance?}_${candidate_P_instance?}_${DB2HADRDBNAME?}-rs' issued on '${localhost?}'" 
      fi  
   fi  
}

#######################################################
# main()
#######################################################
main()
{
   # What instance is at this node?
   set_candidate_P_instance
   cleanup_failed_flag
   
   # We need to check if the instance start is complete here. To minimize
   # the dependency on the checking for the db2 start script, we first check
   # if there is a db2sysc process. If not, no need to go further. If there is
   # a db2sysc process, check if the script is still running.
   p_out=$(ps -u ${candidate_P_instance?} -o args | egrep "^db2sysc|^\[db2sysc\]"| grep -v defunct)
   p_pid=$(echo $p_out | tr " " "\n" | grep -v "\[db2sysc\]" | grep -c "db2sysc")
   if [ $p_pid -eq 0 ]; then
      log info "$LINENO: $PROGNAME $DB2INSTANCE is offline, hadr monitor must return offline"
      rc=2
      return $rc
   else
      isInstanceStartRunning=$(ps -ef | grep db2V115_start.ksh | grep -v grep | grep ${candidate_P_instance?} | wc -l)
      if [[ $isInstanceStartRunning -ne 0 ]]; then
         log notice "$LINENO: db2sysc started but Instance start still running...return OFFLINE for HADR database ${DB2HADRDBNAME?}"
         rc=2
         return $rc
      fi
   fi

   # Should re-integration be performed?
   nn=$(lsrsrc -s "Name = 'db2_${DB2HADRDBNAME?}_${localShorthost?}_Reintegrate_${candidate_P_instance?}_${candidate_S_instance?}' OR Name = 'db2_${DB2HADRDBNAME?}_${localShorthost?}_Reintegrate_${candidate_S_instance?}_${candidate_P_instance?}'" IBM.Test Name | grep -c Name)

   # If so, all apps must be forced...
   if [[ $nn -ne 0 ]]; then
      # create reset failed offline
      tmpReint=reint.${candidate_P_instance?}.${DB2HADRDBNAME?}.out
      su - ${candidate_P_instance?} -c "/bin/ksh $RSCT/sapolicies/db2/forceAllApps $candidate_P_instance $DB2HADRDBNAME"
      touch /tmp/${tmpReint}
      log debug "$LINENO: Reintegration memory file created under /tmp for db ${DB2HADRDBNAME?}"
   fi
   monitorhadr
   return $rc
}

if [[ "$VERBOSE" == "verbose" ]]; then
   typeset -ft $(typeset +f)
fi

#######################################################
main "${@:-}"
log notice "$LINENO: Returning $rc : $*"

exit $rc
