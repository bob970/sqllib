#!/bin/ksh -p
#-----------------------------------------------------------------------
# (C) COPYRIGHT International Business Machines Corp. 2001-2009
# All Rights Reserved
#
# US Government Users Restricted Rights - Use, duplication or
# disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
#
# NAME: db2V115_stop.ksh db2instance [verbose]
#
# (%W%) %E%  %U%
#
# FUNCTION: Stop method for DB2 in TSA HA cluster
#
# Return 0 if sucessful
# Return non-zero value otherwise
#
#-----------------------------------------------------------------------
PATH=/bin:/usr/bin:/sbin:/usr/sbin

# Determine RSCT installation location
if [ -d /opt/rsct/bin ]; then
   PATH=$PATH:/opt/rsct/bin
else
   PATH=$PATH:/usr/sbin/rsct/bin
fi

export PATH
export LANG=C

export CT_MANAGEMENT_SCOPE=2

DB2INSTANCE=${1}
NN=${2:-0}
VERBOSE=${3:-noverbose}

PROGNAME="$(basename $0)[$$]"
PROGPATH=$(dirname $0)

isHadrPrimary=0

if [[ "$VERBOSE" == "verbose" ]]; then
   typeset -ft $(typeset +f)
   set -x
else
   # Close stdout and stderr
   exec 2> /dev/null
   exec 1> /dev/null
   set +x
fi

# environment variable enabled to override default db2gcf timeout value
gcf_value=`su - ${DB2INSTANCE?} -c "env | grep DB2GCF_STOP_TIMEOUT" | awk -F= '{print $2}'`
gcf_timeout=${gcf_value:-100}

###################################################################
# Functions ...
#
function log
{
    priority=$1
    shift
    logger -p $priority -t ${PROGNAME} "$@"
}

###################################################################
# Start main error checking...

log notice "$LINENO: Entered $0, $DB2INSTANCE, $NN"

if [[ -z $DB2INSTANCE ]]; then
   log err "$LINENO: Error: Must specify instance name"
   return 0
fi

if [[ -z ${INSTHOME} ]] ; then
   userhome=~${DB2INSTANCE?}
   eval userhome=$userhome
   INSTHOME=${userhome}
fi

firstChar=$(echo $INSTHOME | cut -c1-1)
if [[ -z ${INSTHOME} || "$firstChar" != "/" ]] ; then
   INSTHOME=$(cat /etc/passwd | grep "^${DB2INSTANCE?}:" | cut -f 6 -d \:)
fi

if [[ -z ${INSTHOME} ]] ; then
   # Do not exit here, we still need to make an attempt at stopping the instance
   log warn "$LINENO: There is no home directory defined for ${DB2INSTANCE?}"
else
   INSTHOME=${INSTHOME#*=}
   INSTHOME=${INSTHOME%%*( )}
fi

###################################################################
# main ...

ret=1
# Ensure home directory is accessible ...
if [[ ! -z ${INSTHOME} ]] ; then
   /bin/ksh -c "cd $INSTHOME/sqllib/bin; touch $INSTHOME/sqllib/tmp/.tmp.$NN; rm -f $INSTHOME/sqllib/tmp/.tmp.$NN" &
   ProcNum=$!
   sleep 2
   kill -0 ${ProcNum} 2> /dev/null
   ret=$?
   kill -9 ${ProcNum} 2> /dev/null
   if [[ $ret == 0 ]]; then
      log warn "$LINENO: $INSTHOME may not be accessible."
   fi
fi

if [[ -z ${INSTHOME} || $ret == 0 ]] ; then
   log warn "$LINENO: Instance home directory is either unretrievable or inaccessible."
   log warn "$LINENO: Will do a hard kill of ( $DB2INSTANCE, $NN )"
   kill -9 $(ps -o pid,comm -u $DB2INSTANCE | grep "db2[a-z]" | awk '{print $1}') 2> /dev/null > /dev/null
   rc=$?
   log info "$LINENO: Returning 0 from $0 ( $DB2INSTANCE, $NN ). rc=$rc"
   return 0
fi

# Do not need to stop if no process model
p_pid=$(ps -u ${DB2INSTANCE?} -o args | grep -c "^db2sysc ${NN?}[ ]*$")
if [[ $p_pid == 0 && $NN -eq 0 ]]; then
   p_pid=$(ps -u ${DB2INSTANCE?} -o args | grep -v "^db2sysc [0-9]" | grep -c "^db2sysc")
fi

if [[ $p_pid == 0 ]]; then
   log notice "$LINENO: Returning 0 from $0 ( $DB2INSTANCE, $NN ). Partition already stopped."
   return 0
fi

# If single partition, check for HADR db's
DB2NODES_CFG=${INSTHOME?}/sqllib/db2nodes.cfg
if test -r $DB2NODES_CFG ; then
   nln=$(grep -v "^[ ]*$" $DB2NODES_CFG | wc -l | awk '{print $1}')
else
   nln=1
fi

if [ $nln -eq 1 ]; then
   dbl1=/tmp/dblist1.${DB2INSTANCE?}.${NN?}.out
   rm -f ${dbl1?} 2> /dev/null
   touch ${dbl1?} 2> /dev/null
   su - ${DB2INSTANCE?} -c "/bin/ksh LANG=en_US db2 list database directory" | grep "Database name" \
   |  awk '{print $4}' > ${dbl1?}

   while read db; do
      rc=$(su - ${DB2INSTANCE} -c "db2pd -hadr -db ${db?}" | grep -i -c '= Primary')
      if [[ $rc -gt 0 ]]; then
         isHadrPrimary=1
      fi
   done < ${dbl1?}
   rm -f ${dbl1?}
   if [ $isHadrPrimary -eq 1 ]; then  
      # Use db2gcf -k and for good measure, set rc such that kill is invoked later in this script
      su - ${DB2INSTANCE} -c "$INSTHOME/sqllib/bin/db2gcf -t 100 -k -p ${NN?} -i ${DB2INSTANCE?} -L"
      rc=1
   else
      su - ${DB2INSTANCE} -c "$INSTHOME/sqllib/bin/db2gcf -t $gcf_timeout -d -p ${NN?} -i ${DB2INSTANCE?} -L"
      rc=$?
   fi
else
   su - ${DB2INSTANCE} -c "$INSTHOME/sqllib/bin/db2gcf -t $gcf_timeout -d -p ${NN?} -i ${DB2INSTANCE?} -L"
   rc=$?
fi

if [[ $rc != 0 ]]; then
   log notice "$LINENO: Killing db2 processes"
   kill -9 $(ps -o pid,comm -u $DB2INSTANCE | grep "db2[a-z]" | grep -v db2haicu | awk '{print $1}')
   log notice "$LINENO: Removing IPCs"
   su - ${DB2INSTANCE} -c "$INSTHOME/sqllib/bin/db2gcf -t 10 -k -p ${NN?} -i ${DB2INSTANCE?};ipclean -a"
   log notice "$LINENO: Completed hard kill ( $DB2INSTANCE, $NN )"
   rc=0
fi

log notice "$LINENO: Returning $rc from $0 ( $DB2INSTANCE, $NN )"
return $rc
