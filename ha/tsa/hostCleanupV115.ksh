#!/bin/ksh
#-----------------------------------------------------------------------
# (C) COPYRIGHT International Business Machines Corp. 2001-2010
# All Rights Reserved
#
# US Government Users Restricted Rights - Use, duplication or
# disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
#
# NAME: hostCleanupV115.ksh [version] [identifier] [zero] [command] [verbose]
#
# FUNCTION: Cleanup all db2 server processes on this host before the 
#           host re-joins the RSCT peer domain 
# Return 0 the host has been successfully cleaned up
# Return non-zero otherwise
#
#-----------------------------------------------------------------------
PATH=/bin:/usr/bin:/sbin:$PATH
export PATH
export LANG=C
export CT_MANAGEMENT_SCOPE=2

myname="$(basename $0)[$$]"
dirname=$(dirname $0)
MP=${0}
version=${1}
identifier=${2}
zero=${3}
command=${4}

VERBOSE=${5:-noverbose}

UNAME=$(uname)

#
# Write messages to the system log
#
function log 
{
   logger -p $1 -t $myname "$2"
}

function errorExit
{
   log err "$1: Failed to cleanup all the DB2 pureScale resources with exit code $2. This host will not be allowed to rejoin the DB2 pureScale cluster until the error occurred has been resolved."  
   exit $2
}

function find_db2ls
{
    for name in $(strings /var/db2/global.reg); do
        if [[ -d $name ]]; then
            if test -x ${name}/install/db2ls ;then
                DB2LS="${name}/install/db2ls"
                break
            fi
        fi
    done
}

# unsetting library search path that could
# mess up our shared lib loading
unset LD_LIBRARY_PATH
unset LD_PRELOAD
unset LIBPATH

# Verify input...
if [[ $# < 4 ]]; then
   log err "$LINENO: Error: Not enough input parameters. '$*'"
   rc=2
   errorExit $LINENO $rc
fi

# Make sure verbose is setup for debugging purposes
if [[ "$VERBOSE" == "verbose" ]]; then
   typeset -ft $(typeset +f)
   set -x
else
   # Close stdout and stderr
   exec 2> /dev/null
   exec 1> /dev/null
   set +x
fi

if [[ $version -ne 1 ]]; then
   log err "$LINENO: Error: input version is incorrect: $version expected 1"
   rc=3
   errorExit $LINENO $rc
fi 

log notice "$LINENO: Entered ($MP)"

###################################################################
# Start main...
rc=0

#figure out which install images exist
if test ! -x /usr/local/bin/db2ls ; then
   find_db2ls
   if test ! -x ${DB2LS}; then
       log err "$LINENO: Exiting ($MP): install images can not be detected properly, '/usr/local/bin/db2ls' is not executable or is missing."
       rc=4;
       errorExit $LINENO $rc
   fi
else
    DB2LS=/usr/local/bin/db2ls
fi

log debug "$LINENO: Using ${DB2LS?} to list instances"

installedCopies=`${DB2LS} -c | grep -v "^#" | awk -F : '{ print \$1 }'`
found=0 ;

#run db2ilist for each install image
for path in $installedCopies
do
   log debug "$LINENO: Checking path $path"
   for instanceName in `$path/bin/db2ilist -s dsf`
   do
      exeName="~$instanceName/sqllib/adm/db2rocme"
      eval exeName=$exeName
      if [ -e "$exeName" ] ; then
         log notice "$LINENO: DB2 pureScale instance $instanceName will be cleaned up" 
         found=$found+1 ;
         db2rocmOutput=`$exeName $version $identifier $instanceName $zero $command 2>&1`
         rc=$?
         if [[ $rc -ne 0 ]] ; then
            log err "$LINENO: $instanceName cleanup failed with return code $rc and error message $db2rocmOutput" 
            errorExit $LINENO $rc
         fi
      else
         log err "$LINENO: DB2 pureScale instance $instanceName is missing the required db2rocme executable ($exeName)." 
      fi
   done
done

#if all instances for each image are clean, start up gpfs and exit
if [[ $found -eq 0 ]] ; then
   log notice "$LINENO: No DB2 pureScale instances found"
fi

clusterName=`/usr/lpp/mmfs/bin/mmlsconfig clusterName | awk '{ print \$2 }'`
if [[ $clusterName != "" ]] ; then
   log notice "$LINENO: starting up this host in the GPFS cluster $clusterName"
   retryCount=0
   while true
   do
      commandOut=`/usr/lpp/mmfs/bin/mmstartup`
      log notice "$LINENO: Return code: $? $commandOut"
      sleep 5
      state=$(/usr/lpp/mmfs/bin/mmgetstate | grep $(hostname -s) | awk '{print $3}')
      if [[ "$state" == "active" || "$state" == "arbitrating" || $retryCount -eq 20 ]] ; then
         break
      fi
      log notice "$LINENO: gpfs daemon is $state - delay 5 seconds"
      retryCount=$((${retryCount}+1))
   done
fi

log notice "$LINENO: Returning $rc"
exit $rc
