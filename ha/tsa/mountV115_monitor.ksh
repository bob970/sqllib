#!/bin/ksh -p
#-----------------------------------------------------------------------
# (C) COPYRIGHT International Business Machines Corp. 2001-2009
# All Rights Reserved
#
# US Government Users Restricted Rights - Use, duplication or
# disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
#
# NAME: mountV115_monitor.ksh /mount_point [ verbose ]
#
# FUNCTION: Monitor method for filesystem in TSA cluster
#
# Return 1 if mount point is mounted and accessible (ie. online)
# Return 2 if mount point is not mounted and underlying VG varyedoff
# Return 3 if mount point is mounted but IO errors
# Return 4 if no /etc/fstab,/etc/filesystems entry
#
# Return 0 (Unknown) VG varied on but fs not mounted
#-----------------------------------------------------------------------
#
# Write messages to the system log and mmfs.log.latest 
#
log()
{
    priority=$1
    shift
    logger -p $priority -t $myNameAndPid "$@"

    # mmfs.log.latest must be a link. If it is not, then
    # do not write to it - the link is in process of being recreated.
    if [[ -L /var/adm/ras/mmfs.log.latest ]]; then
        echo "$(date): $myNameAndPid: $@" >> /var/adm/ras/mmfs.log.latest
    fi
}

#-----------------------------------------------------------------------

export PATH=/bin:/usr/bin:/sbin:$PATH
export LANG=C
export CT_MANAGEMENT_SCOPE=2

myname=$(basename $0)
dirname=$(dirname $0)
myNameAndPid="$(basename $0)[$$]"
fstype="Unknown"
typeset -r UNKNOWN=0
typeset -r ONLINE=1
typeset -r OFFLINE=2
typeset -r FAILED_OFFLINE=3
typeset -r STUCK_ONLINE=4

typeset -i rc=$UNKNOWN
typeset -r MAX_RETRIES=20

MP=${1}
VERBOSE=${2:-noverbose}

UNAME=$(uname)

# environment variable enabled by ISAS 
isVgManaged=$vgManaged

#log debug "$LINENO: Entering monitor for $MP"

# If GPFS was forced offline by RSCT response script, return failed offline
GPFS_FAILED_FILE=$(echo "$myname.$MP.gpfs_failed"|sed 's/\//./g')
FORCED_OFFLINE_FILE="/var/db2/db2_gpfs_forced_offline_`hostname -s`"
MOUNT_MAINT_FILE="/var/db2/.db2_maintenance"$(echo "$MP"|sed 's/\//_/g')

typeset isMountRetries=1

if [[ -e ${FORCED_OFFLINE_FILE} ]]; then
    rc=$FAILED_OFFLINE;
    if [[ ! -e /var/db2/${GPFS_FAILED_FILE} ]]; then
        log err "$LINENO: ${FORCED_OFFLINE_FILE} exists at this node. Returning 3"
        touch /var/db2/${GPFS_FAILED_FILE}
    #else
        # This puts too many messages in the system log
        #log debug "$LINENO: ${FORCED_OFFLINE_FILE} exists at this node. Returning 3"
    fi
    echo $rc
    return $rc
fi

if [[ -e /var/db2/${GPFS_FAILED_FILE} ]]; then
    # GPFS adapter back online - remove the failed file
    rm /var/db2/${GPFS_FAILED_FILE}
fi

if [[ "$VERBOSE" == "verbose" ]]; then
#  exec   2> /dev/null
   set -x
else
   #   Close stdout   and   stderr
   exec   2> /dev/null
   exec   1> /dev/null
   set +x
fi

if [[ -z $MP ]]; then
   rc=$FAILED_OFFLINE
   log err "$LINENO: Error: Must specify mount point"
   log err "$LINENO: Returning $rc for mount $MP"
   echo $rc
   return $rc
fi

function isMounted
{
   let mounted=0

   # If we're already trying to force GPFS offline due to a failed GPFS
   # network adapter, just return not mounted so we don't attempt IO
   if [[ ! -e ${FORCED_OFFLINE_FILE} ]]; then
      if [[ "${UNAME}" == "AIX" ]]; then
         mounted=$(mount | awk '{print $2}' | grep -c "^${MP}\$")
      else
         mounted=$(mount | awk '{print $3}' | grep -c "^${MP}\$")
      fi
      if [[ -z $mounted ]]; then
         mounted=0
      fi
   fi
   #log debug "$LINENO: isMounted returns $mounted"
   return $mounted
}

function mainMonitorLogic
{
   isMounted
   if [ ${mounted} -gt 0 ]; then
      if [ ! -d ${MP_MONITOR_PATH} ]; then
         log debug "$LINENO: Creating directory ${MP_MONITOR_PATH}"
         mkdir ${MP_MONITOR_PATH}
      fi
      pathOnMP=$(df $MP_MONITOR_PATH | grep -c $MP)
      if [[ ${pathOnMP} -eq 0 ]] ; then
         log err "$LINENO: The file monitor path ${MP_MONITOR_PATH} does not exist on ${MP}"
         rc=$OFFLINE
      else 
         touch ${MP_MONITOR_FILE}
         if [ -f ${MP_MONITOR_FILE} ]; then
            #log debug "$LINENO: No error performing I/O to file ${MP_MONITOR_FILE}"
            rc=$ONLINE
            rm -f ${MP_MONITOR_FILE}
         else
            log err "$LINENO: Error performing I/O to file ${MP_MONITOR_FILE}"
        
            # log a message if we detect GPFS to check free inodes
            if [[ "$fstype" == "gpfs" ]]; then
               log debug "$LINENO: GPFS filesystem was detected, ensure there are enough free inodes with mmdf."
            fi

            rm -f ${MP_MONITOR_FILE}
            rc=$OFFLINE
         fi
      fi
   fi
}

function waitForGpfsActiveState
{
   typeset -i waitForActive=12
   typeset -i hadToWait=0

   state=$(/usr/lpp/mmfs/bin/mmgetstate | grep $(hostname -s) | awk '{print $3}')
   until [[ "$state" == "active" ]]; do
      hadToWait=1
      log debug "$LINENO: gpfs daemon is $state - delay 5 seconds"
      sleep 5

      (( waitForActive = waitForActive - 1 ))

      if (( ! waitForActive )); then
          # If the user has run db2cluster -cfs -stop without first
          # running db2cluster -cm -stop, the monitor will get stuck
          # If a timeout occurs, then Log a warning so that the admin
          # has a clue as to what is going on.
          log warn "$LINENO: db2cluster -cm -stop must be run prior to db2cluster -cfs -stop"
          break
      fi

      if [[ -e ${FORCED_OFFLINE_FILE} ]]; then
          # The forced offline file was created due to an adapter down event
          # while waiting for the daemon to be started.
          # This means the adapter failed just after the monitor was invoked
          # Return failed offline now so that failover will proceed
          log debug "$LINENO: ${FORCED_OFFLINE_FILE} exists - return failed offline"
          touch /var/db2/${GPFS_FAILED_FILE}
          exit $FAILED_OFFLINE
      fi

      state=$(/usr/lpp/mmfs/bin/mmgetstate | grep $(hostname -s) | awk '{print $3}')
   done

   if (( hadToWait && waitForActive )); then
      log debug "$LINENO: Had to wait for GPFS daemon to become active - delay 30 for fs mount to complete"
      sleep 30
   fi
}

###################################################################
# Start main...
#
# First do some automount checks and 
# determine the device for the mount point
#
if [[ "${UNAME}" == "AIX" ]]; then
   #AIX

   # Retrieve the mount point's file system type.
   # Retry up to (5 * MAX_RETRIES) seconds.
   #
   retryCount=0
   fstype=""
   while [[ $retryCount -lt $MAX_RETRIES && -z $fstype ]] ;
   do
      fstype=$(cat /etc/filesystems | grep -p ^${MP?}: | awk '{print $1 " " $2 " " $3}' | egrep "^vfs = " | awk '{print $3}')

      if [[ -z $fstype ]]; then
         log war "$LINENO: Unable to determine the file system type for $MP... Retry attempt: $retryCount/$MAX_RETRIES"
         sleep 5
         retryCount=$((${retryCount}+1))
      fi
   done

   if [[ -z $fstype ]]; then
      log err "$LINENO: Unable to determine the file system type for $MP. Debug info: $retryCount/$MAX_RETRIES/$fstype. Exiting with rc=$FAILED_OFFLINE (Failed Offline)"
      exit $FAILED_OFFLINE
   fi

   if [[ "$fstype" == "mmfs" ]]; then
      fstype="gpfs"
   fi

   if [[ "$fstype" != "gpfs" ]] ; then
      isAutoMountSet=$(cat /etc/filesystems | grep -p ^${MP?}: | awk '{print $1 " " $2 " " $3}' | egrep "^mount = " | awk '{print $3}' |  egrep -i -c "(automatic|true)")
      if [[ $isAutoMountSet -eq 1 ]]; then
         log err "$LINENO: Automounting is enable for $MP. Please change this."
         isMounted
         if [[ $mounted -gt 0 ]]; then
            log err "$LINENO: Exiting with rc=$STUCK_ONLINE (Stuck Online)"
            exit $STUCK_ONLINE
         else
            log err "$LINENO: Exiting with rc=$FAILED_OFFLINE (Failed Offline)"
            exit $FAILED_OFFLINE
         fi
      fi
   fi

   mRetry=3
   while [ 1 ]; do
      blockDevice=$(cat /etc/filesystems | grep -p ^${MP?}: | awk '{print $1 " " $2 " " $3}' | egrep "^dev = " | awk '{print $3}')

      #log debug "$LINENO: blockDevice equals $blockDevice"

      if [[ -z $blockDevice ]]; then
         # This is not a valid mount point, it may be an RLV
         blockDevice=${MP?}
         unset MP
         rlv=true
      fi

      lvname=${blockDevice#/dev/}
      lslvout=$(lslv -L ${lvname} 2>&1)
      lslvrc=$?
      vgname=$(echo $lslvout | grep "VOLUME GROUP" | awk '{print $6}')
      if [[ ! -z "$vgname" ]] ; then
         majNum=$(ls -l /dev/$vgname | tr "," " " | awk '{print $5}')
         lspvLine=$(lspv -L | grep " $vgname ")
         diskID=$(echo $lspvLine | awk '{print $2}')
         hdiskID=$(echo $lspvLine | awk '{print $1}')
         lvname=${blockDevice#/dev/}
         #log debug "$LINENO: majNum,lspvLine,diskID,hdiskID,lvname equals $majNum,$lspvLine,$diskID,$hdiskID,$lvname"
      else
         # For GPFS we will allow no VG/LV
         if [[ "$fstype" == "gpfs" ]]; then
            blockDevice="mmfs"
            lvname="mmfs"
            vgname="mmfs"
         else
            log debug "$LINENO: blockDevice equals $blockDevice"
            log debug "$LINENO: lslv output: $lslvout"
            log debug "$LINENO: lslv rc=$lslvrc"
            log debug "$LINENO: vgname = $vgname" 

            log err "$LINENO: Mount point $MP not defined at this host. Check /etc/filesystems"
            log err "$LINENO: majNum,lspvLine,diskID,hdiskID,lvname equals $majNum,$lspvLine,$diskID,$hdiskID,$lvname"

            if [[ $mRetry -gt 1 ]]; then
               log debug "$LINENO: RETRY lv check"
               sleep 2
               ((mRetry=mRetry-1))
               continue # back to top of loop
            fi

            isMounted
            if [[ $mounted -gt 0 ]]; then
               # This is bad ... not in /etc/filesystem yet mounted
               log err "$LINENO: Exiting with rc=$STUCK_ONLINE (Stuck Online)"
               exit $STUCK_ONLINE
            else
               log err "$LINENO: Exiting with rc=$FAILED_OFFLINE (Failed Offline)"
               exit $FAILED_OFFLINE
            fi
         fi
      fi

      break # break out of loop
   done

elif [[ "${UNAME}" == "SunOS" ]]; then
   # SunOS

   isAutoMountSet=$(cat /etc/vfstab | awk '{print $3 " " $6}' | grep "^${MP} " | egrep -c "yes")
   if [[ $isAutoMountSet -eq 1 ]]; then
      log err "$LINENO: Mount at boot is set to yes for $MP. Please change this."
      isMounted   
      if [[ $mounted -gt 0 ]]; then
         log err "$LINENO: Exiting with rc=$STUCK_ONLINE (Stuck Online)"
         exit $STUCK_ONLINE
      else
         log err "$LINENO: Exiting with rc=$FAILED_OFFLINE (Failed Offline)"
         exit $FAILED_OFFLINE
      fi
   fi

   blockDevice=$(cat /etc/vfstab | awk '{print $1 " " $3}' | grep " ${MP}$" | awk '{print $1}')
   #log debug "$LINENO: blockDevice is $blockDevice"
   isMounted
   if [[ -z "$blockDevice" ]]; then
      log err "$LINENO: Mount point $MP not defined at this host.  Check /etc/vfstab."
      if [[ $mounted -gt 0 ]]; then
         log err "$LINENO: Exiting with rc=$STUCK_ONLINE (Stuck Online)"
         exit $STUCK_ONLINE
      else
         log err "$LINENO: Exiting with rc=$FAILED_OFFLINE (Failed Offline)"
         exit $FAILED_OFFLINE
      fi
   fi

else
   # Linux

   fstype=$(cat /etc/fstab | grep " ${MP} " | awk '{ print $3 }')
   if [[ "$fstype" != "gpfs" ]] ; then
      isAutoMountSet=$(cat /etc/fstab | awk '{print $2 " " $4}' | grep "^${MP} " | egrep -c "noauto")
      if [[ $isAutoMountSet -eq 0 ]]; then
         log err "$LINENO: $MP is not set to noauto. Please change this."
         isMounted
         if [[ $mounted -gt 0 ]]; then
            log err "$LINENO: Exiting with rc=$STUCK_ONLINE (Stuck Online)"
            exit $STUCK_ONLINE
         else
            log err "$LINENO: Exiting with rc=$FAILED_OFFLINE (Failed Offline)"
            exit $FAILED_OFFLINE
         fi
      fi
   fi

   blockDevice=$(cat /etc/fstab | awk '{print $1 " " $2}' | grep " ${MP}$" | awk '{print $1}')
   #log debug "$LINENO: blockDevice is $blockDevice"
   isMounted
   if [[ -z "$blockDevice" ]]; then
      log err "$LINENO: Mount point $MP not defined at this host.  Check /etc/fstab."
      if [[ $mounted -gt 0 ]]; then
         log err "$LINENO: Exiting with rc=$STUCK_ONLINE (Stuck Online)"
         exit $STUCK_ONLINE
      else
         log err "$LINENO: Exiting with rc=$FAILED_OFFLINE (Failed Offline)"
         exit $FAILED_OFFLINE
      fi
   fi
fi

#
# Check to see if the the mount point stub exists.
# 
if [[ ! -z "$blockDevice" ]]; then
   cd $MP
   rc=$?
   cd /
   if [[ $rc -ne 0 ]]; then
      # -d sometimes fails with stale mounts, so only check for existence
      # of the mountpoint. On some platforms, -e doesn't work either.
      # Let's check `df <mount_point>` and see if the mount point is stale.
      #
      # The output of `df` for stale mount point is different between linux/aix.
      # AIX
      # root@pscalep74001b:/> df /db2fs/svtfs0
      # Filesystem    512-blocks      Free %Used    Iused %Iused Mounted on
      # /dev/svtfs0            -         -    -         -     -  /db2fs/svtfs0
      #
      # LINUX (linuxamd64/ppcle)
      # [root@svtppcle17 ~]# df /db2sd_20160205214148
      # df: /db2sd_20160205214148: Stale file handle
      stale=`df $MP 2>&1 | grep $MP | tr -d " " | grep -vxiE 'stale|\-{5}'`
      log debug "$LINENO: `df $MP 2>&1 | grep $MP`"
      if [[ -e $MP ]] || [[ -n $stale ]]; then
         log err "$LINENO: Mount point stub $MP exists but is not currently accessible (filesystem may be offline).  Continuing..."
      else
         log err "$LINENO: Mount point stub $MP not defined at this host.  You may need to do a mkdir $MP at all nodes."
         exit $FAILED_OFFLINE
      fi
   fi
fi

#
# If the mount point is a Logical Volume Group then check the status.
# If the LV is active, return online. If not, return offline
# rlv can be true only on AIX
#
let mounted=0
let rc=$OFFLINE
if [[ $rlv == "true" ]]; then
   # This will be the case when MP passed in is actually a RLV
   # This was invoked from an integrted HA code path and this is 
   # not associtated mount, therefore, we simply return the rc after
   # performing the check.
   vgactive=$(echo $lspvLine | awk '{print $4}')
   if [[ "$vgactive" == "active" ]];then
      rc=$ONLINE
      echo $rc
      return $rc
   else
      rc=$OFFLINE
      echo $rc
      return $rc
   fi
fi

#
# What is status of this mount point?
#
MP_MONITOR_PATH=${MP}/.`hostname -s`
MP_MONITOR_FILE=${MP_MONITOR_PATH}/.mp_monitor.$$
mainMonitorLogic

# If AIX, what is status of the underlying VG if fs not mounted
if [[ "${UNAME}" == "AIX" ]]; then
   if [[ $rc -eq $OFFLINE ]]; then
      vgactive=$(echo $lspvLine | awk '{print $4}')
      #log debug "$LINENO: vgactive is $vgactive"
      if [[ "$vgactive" == "active" ]];then
         lsvgout=$(lsvg -L $vgname 2>&1)
         lsvgrc=$?
         concurrent=$(lsvg $vgname | grep "^Concurrent:" | awk '{print $2}')
         if [[ $lsvgrc -ne 0 ]]; then
            log err "$LINENO: lsvgout, lsvgrc=$lsvgrc, concurrent=$concurrent"
            exit $UNKNOWN
         fi
      fi
   fi
fi

# PureScale specific monitoring logic
#
if [[ "$fstype" == "gpfs" ]]; then
   TIMEOUT_FILE=$(echo "$myname.$MP.timeout"|sed 's/\//./g')

   # If file system online and if maintenance file is present, return
   # failed off-line
   if [[ $rc -eq $ONLINE && -e /var/db2/.db2_maintenance_`hostname -s` ]]; then
      log info "$LINENO: File system is online and maintenance file is present. Returning 3"
      rc=$FAILED_OFFLINE;
   fi

   # If mount is in maintenance mode, return failed offline
   if [[ -e $MOUNT_MAINT_FILE ]]; then
      log info "$LINENO: Mount point is in maintenance mode. Returning 3"
      rc=$FAILED_OFFLINE ;
   fi

   # If this is the first offline, then do a stop-force, and return a
   # failed-offline to TSA.  The next time through, we'll return offline,
   # and TSA will try to restart.
   if [[ $rc -eq $OFFLINE ]]; then
      if [[ ! -e "/var/db2/$TIMEOUT_FILE" ]]; then
         # If GPFS was forced offline due to adapter down, there is 
         # the possibility of a race here. If that is the case, then
         # don't invoke the stop script as it will get stuck trying to umount.
         if [[ ! -e ${FORCED_OFFLINE_FILE} ]]; then
            # Check to see if the GPFS daemon is active.
            # If it is not, then wait for it to become active.
            # Additionally, if we waited, wait an additional 30 seconds to allow time
            # for GPFS to mount the file system
            waitForGpfsActiveState
            # verify if the file system is mounted now
            mainMonitorLogic
            if [[ $rc -eq $ONLINE ]]; then
               log debug "$LINENO: mount $MP detected as online in second monitoring phase"
            fi

            manual=$(lssamctrl | grep -c "Manual")

            # filesystem is still not mounted and we are not in manual mode,
            # do a stop force
            if [[ $manual -eq 0 && $rc -eq $OFFLINE ]]; then
               log debug "$LINENO: backgrounding the stop force for $MP"
               $dirname/mountV115_stop.ksh $MP force &
            fi
            touch "/var/db2/$TIMEOUT_FILE"
         fi

         if [[ $rc -eq $OFFLINE ]]; then
            rc=$FAILED_OFFLINE
         fi
      else
         stoppid=$(ps -ef | grep "mountV115_stop.ksh $MP force" | grep -v grep | awk '{print $2}')
         if [[ $stoppid -gt 0 ]] ; then
            log debug "$LINENO: Killing the background stop process $stoppid after offline"
            kill -9 $stoppid
         fi

         # If the GPFS file system is reporting offline due to the disk not being available, 
         # then background a start request for it.
         startRunning=$(ps -ef | grep -v grep | grep -c "mountV115_start.ksh $MP")

         # Don't bother checking disk availability if a start is already running
         if [[ $startRunning -eq 0 ]]; then
            deviceName=${blockDevice#/dev/}
            diskNotFound=$(/usr/lpp/mmfs/bin/mmlsnsd -f $deviceName -m | grep $(hostname -s) | grep -c "not found")
            if [[ $diskNotFound -gt 0 ]]; then
               log notice "$LINENO: GPFS disk is unavailable, backgrounding the start for $MP"
               $dirname/mountV115_start.ksh $MP &
            fi
         fi
      fi
   fi

   # Remove any temporary monitor-timeout files
   if [[ $rc -eq $ONLINE && -e "/var/db2/$TIMEOUT_FILE" ]]; then
      rm -f "/var/db2/$TIMEOUT_FILE"

      stoppid=$(ps -ef | grep "mountV115_stop.ksh $MP force" | grep -v grep | awk '{print $2}')
      if [[  $stoppid -gt 0 ]] ; then
         log debug "$LINENO: Killing the background stop process $stoppid after back online"
         kill -9 $stoppid
      fi
   fi
fi

# Let's not inundate the system log with returning online messages every second
if [[ $rc -ne $ONLINE ]]; then
    log debug "$LINENO: Returning $rc for mount $MP"
fi

echo $rc
return $rc
