#!/bin/ksh -p
#-----------------------------------------------------------------------
# (C) COPYRIGHT International Business Machines Corp. 2001-2009
# All Rights Reserved
#
# US Government Users Restricted Rights - Use, duplication or
# disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
#
# INPUT:  hadrV115_start.ksh db2instp db2insts db2hadrdb [verbose]
#
# OUTPUT: 0 if started succesfully, non-zero otherwise
#
#-----------------------------------------------------------------------
PATH=/bin:/usr/bin:/sbin:/usr/sbin

# Determine RSCT installation location
if [ -d /opt/rsct/bin ]; then
   PATH=$PATH:/opt/rsct/bin
else
   PATH=$PATH:/usr/sbin/rsct/bin
fi

export PATH

#
# Write messages to the system log
#
log()
{
   priority=$1
   shift

   logger -p $priority -t $PROGNAME "$@"
}

LANG=C
export LANG

PROGNAME="$(basename $0)[$$]"
log notice "$LINENO: Entering : $*"

PROGPATH=$(dirname $0)
DB2HADRINSTANCE1=${1?}
DB2HADRINSTANCE2=${2?}
dbname=${3?}
DB2HADRDBNAME=$(echo ${dbname?} | tr "[a-z]" "[A-Z]")
VERBOSE=${4:-noverbose}

log debug "$LINENO: PROGNAME: $PROGNAME,PROGPATH: $PROGPATH,DB2HADRINSTANCE1: $DB2HADRINSTANCE1, DB2HADRINSTANCE2: $DB2HADRINSTANCE2, dbname: $dbname, DB2HADRDBNAME: $DB2HADRDBNAME, VERBOSE: $VERBOSE"

if [[ "$VERBOSE" == "verbose" ]]; then
   typeset -ft $(typeset +f)
   set -x
else
   # Close stdout and stderr
   exec 2> /dev/null
   exec 1> /dev/null
   set +x
fi

CT_MANAGEMENT_SCOPE=2
export CT_MANAGEMENT_SCOPE
CLUSTER_APP_LABEL=db2_${DB2HADRINSTANCE1?}_${DB2HADRINSTANCE2?}_${DB2HADRDBNAME?}


###########################################################
# set_candidate_P_instance()
###########################################################
set_candidate_P_instance()
{
   candidate_P_instance=${DB2HADRINSTANCE1?}
   candidate_S_instance=${DB2HADRINSTANCE2?}

   localShorthost=$(hostname | tr "." " " | awk '{print $1}')

   log debug "$LINENO: candidate_P_instance: $candidate_P_instance, candidate_S_instance: $candidate_S_instance, localShorthost: $localShorthost"

   if [[ ${DB2HADRINSTANCE1?} != ${DB2HADRINSTANCE2?} ]]; then
      if [[ -f ${PROGPATH?}/.${CLUSTER_APP_LABEL?} ]]; then
         candidate_P_instance=$(cat ${PROGPATH?}/.${CLUSTER_APP_LABEL?})
         if [[ ${candidate_P_instance?} == ${DB2HADRINSTANCE1?} ]]; then
            candidate_S_instance=${DB2HADRINSTANCE2?}
         else
            candidate_S_instance=${DB2HADRINSTANCE1?}
         fi
      else
         nn1=$(lsequ -s "Name like '%${DB2HADRINSTANCE1?}#_${localShorthost?}%'" | grep "Resource:Node" | awk '{print $3}' | tr ":" " " | tr "{" " " | tr "}" " " | awk '{print $1}')
         if [[ -z "$nn1" ]] ; then
            nn1=$(lsequ -s "Name like '%${DB2HADRINSTANCE1?}#_0%'" | grep "Resource:Node" | awk '{print $3}' | tr ":" " " | tr "{" " " | tr "}" " " | awk '{print $1}')
         fi

         nn1s=$(echo $nn1 | tr "." " " | awk '{print $1}')
   
         if [[ ${nn1s?} == ${localShorthost?} ]]; then
            candidate_P_instance=${DB2HADRINSTANCE1?}
            candidate_S_instance=${DB2HADRINSTANCE2?}
         else
            candidate_P_instance=${DB2HADRINSTANCE2?}
            candidate_S_instance=${DB2HADRINSTANCE1?}
         fi
         echo ${candidate_P_instance?} > ${PROGPATH?}/.${CLUSTER_APP_LABEL?}
      fi
      log debug "$LINENO: $candidate_P_instance,$candidate_S_instance,$nn1,$nn1s"
   fi

   log debug "$LINENO: candidate_P_instance: $candidate_P_instance, candidate_S_instance: $candidate_S_instance, localShorthost: $localShorthost, nn1: $nn1, nn1s: $nn1s"

   return 0
}
###########################################################
# cleanupFlag()
###########################################################
cleanupFlag()
{
   if [[ $DB2HADRINSTANCE1 > $DB2HADRINSTANCE2 ]]; then
      clusterInitiatedMoveRG=db2_${DB2HADRDBNAME?}_ClusterInitiatedMove_${DB2HADRINSTANCE2?}_${DB2HADRINSTANCE1?}
   else
      clusterInitiatedMoveRG=db2_${DB2HADRDBNAME?}_ClusterInitiatedMove_${DB2HADRINSTANCE1?}_${DB2HADRINSTANCE2?}
   fi
   REMOVE_CLUSTER_MOVE_FAILED_RG=$(echo "remove.RG.$clusterInitiatedMoveRG.failed")
   # check if any IBM.Test flag resource for clusterInitiatedMove flag
   ciMoveExist=$(lsrsrc -s "Name like '$clusterInitiatedMoveRG'" IBM.Test | grep -v "test_rg" |  grep -c "^resource ")
   if [ $ciMoveExist -ne 0 ]; then
      log debug "$LINENO: removing $clusterInitiatedMoveRG IBM.Test"
      rmrsrc -s "Name like '$clusterInitiatedMoveRG'" IBM.Test
      lrc2=$?
      if [ $lrc2 -ne 0 ]; then
         log info "$LINENO: touching REMOVE_CLUSTER_MOVE_FAILED_RG: ${REMOVE_CLUSTER_MOVE_FAILED_RG}"
         touch /tmp/${REMOVE_CLUSTER_MOVE_FAILED_RG}
      fi
   fi

    # Check if the clusterInitiatedMove flag is on /tmp.
    # In some circumstances, when the ClusterInitiatedMove resource
    # cannot be created by TSA, we will create a flag file on /tmp.
    # Clean it up here in case it is left around if db2 crashed
    # or for some other reason it was never cleaned up.
    if [[ -e /tmp/$clusterInitiatedMoveRG ]]; then
       rm -f /tmp/$clusterInitiatedMoveRG ;
    fi
}

###########################################################
# cleanupFlag()
###########################################################
cleanupFlag()
{
   if [[ $DB2HADRINSTANCE1 > $DB2HADRINSTANCE2 ]]; then
      clusterInitiatedMoveRG=db2_${DB2HADRDBNAME?}_ClusterInitiatedMove_${DB2HADRINSTANCE2?}_${DB2HADRINSTANCE1?}
   else
      clusterInitiatedMoveRG=db2_${DB2HADRDBNAME?}_ClusterInitiatedMove_${DB2HADRINSTANCE1?}_${DB2HADRINSTANCE2?}
   fi
   REMOVE_CLUSTER_MOVE_FAILED_RG=$(echo "remove.RG.$clusterInitiatedMoveRG.failed")
   ## check if any IBM.Test flag resource for clusterInitiatedMove flag
   ciMoveExist=$(lsrsrc -s "Name like '$clusterInitiatedMoveRG'" IBM.Test | grep -v "test_rg" |  grep -c "^resource ")
   if [ $ciMoveExist -ne 0 ]; then
      log debug "$LINENO: removing $clusterInitiatedMoveRG IBM.Test"
      rmrsrc -s "Name like '$clusterInitiatedMoveRG'" IBM.Test
      lrc2=$?
      if [ $lrc2 -ne 0 ]; then
         log info "$LINENO: touching REMOVE_CLUSTER_MOVE_FAILED_RG: ${REMOVE_CLUSTER_MOVE_FAILED_RG}"
         touch /tmp/${REMOVE_CLUSTER_MOVE_FAILED_RG}
      fi
   fi
}

###########################################################
# starthadr()
###########################################################
starthadr()
{
   # Get path to home dir
   DB2INSTANCE=${candidate_P_instance?}
   export DB2INSTANCE
   if [[ -z ${INSTHOME} ]] ; then
      userhome=~${DB2INSTANCE?}
      eval userhome=$userhome
      INSTHOME=${userhome}
   fi

   firstChar=$(echo $INSTHOME | cut -c1-1)
   if [[ -z ${INSTHOME} || "$firstChar" != "/" ]] ; then
      INSTHOME=$(cat /etc/passwd | grep "^${DB2INSTANCE?}:" | cut -f 6 -d \:)
   fi

   if [[ -z ${INSTHOME} ]] ; then
      log error "$LINENO: There is no home directory defined for ${DB2INSTANCE?}"
      rc=1
      return $rc
   fi

   # Verify DB2 instance status.
   # rc=1 -- Online
   NN=0
   $PROGPATH/db2V115_monitor.ksh $DB2INSTANCE $NN
   rc=$?

   # If the DB2 instance is not online and one of the db2start/db2star2 executables do not exist,
   # then immediately fail this start to ensure a quicker failover to the standby.
   #
   # Note: The reason we specifically check for the case of db2start/db2star2 not existing is
   # due to the fact that the whitepaper docs specifically mention to test the validity of the
   # HADR solution by renaming the db2start/db2star2 binary and killing the instance on the primary.
   # We want to ensure a quick failover in this case.
   if [[ $rc -ne 1 &&
         ( ! -e "$INSTHOME/sqllib/adm/db2start" || ! -e "$INSTHOME/sqllib/adm/db2star2" ) ]] ; then
      log error "$LINENO: Cannot start HADR on this host, no db2start/db2star2 binary"
      rc=1
      return $rc
   fi

   log debug "$LINENO: su - ${candidate_P_instance?} -c $INSTHOME/sqllib/bin/db2gcf -t 3600 -u -i ${candidate_P_instance?} -i ${candidate_S_instance?} -h ${DB2HADRDBNAME?} -L ..."
   su - ${candidate_P_instance?} -c "$INSTHOME/sqllib/bin/db2gcf -t 3600 -u -i ${candidate_P_instance?} -i ${candidate_S_instance?} -h ${DB2HADRDBNAME?} -L"
   rc=$?
   if [[ $rc -ne 0 ]]; then
      # First attempt to online failed
      # Make sure the instance is online first 
      noffline=$(lsrg -s "Name like 'db2#_${candidate_P_instance?}#_${localShorthost?}%'" OpState | grep "OpState = 2" | wc -l )
      until [[ ${noffline?} -eq 0 ]]; do
         log notice "$LINENO: Wait for instance to be online before continuing... "
         noffline=$(lsrg -s "Name like 'db2#_${candidate_P_instance?}#_${localShorthost?}%'" OpState | grep "OpState = 2" | wc -l )
         sleep 1
      done

      # Activate db if it's not already activated
      deact=$(su - ${candidate_P_instance?} -c "db2pd -db ${DB2HADRDBNAME?}" | grep "not activated" | wc -l)
      if [[ ${deact?} -ne 0 ]]; then
         log debug "$LINENO: About to ACTIVATE ${DB2HADRDBNAME?}"
         su - ${candidate_P_instance?} -c "db2 activate database ${DB2HADRDBNAME?}"
      fi
                                    
      log debug "$LINENO: su - ${candidate_P_instance?} -c $INSTHOME/sqllib/bin/db2gcf -t 3600 -u -i ${candidate_P_instance?} -i ${candidate_S_instance?} -h ${DB2HADRDBNAME?} -L ..."
      su - ${candidate_P_instance?} -c "$INSTHOME/sqllib/bin/db2gcf -t 3600 -u -i ${candidate_P_instance?} -i ${candidate_S_instance?} -h ${DB2HADRDBNAME?} -L"
      rc=$?
   fi

   log debug "$LINENO: ...returns $rc"

}

#######################################################
# main()
#######################################################
main()
{
   set_candidate_P_instance
   starthadr
   # try to clean up clusterInitiatedMove flag in case it's failed to do so due to no Config Quorum
   cleanupFlag
   return $rc
}

if [[ "$VERBOSE" == "verbose" ]]; then
   typeset -ft $(typeset +f)
fi

#######################################################
main "${@:-}"
log notice "$LINENO: Returning $rc : $*"
exit $rc
