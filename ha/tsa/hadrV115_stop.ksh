#!/bin/ksh -p
#-----------------------------------------------------------------------
# (C) COPYRIGHT International Business Machines Corp. 2001-2009
# All Rights Reserved
#
# US Government Users Restricted Rights - Use, duplication or
# disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
#
# INPUT:  hadrV115_stop.ksh db2instp db2insts db2hadrdb [verbose]
#
# OUTPUT: 0 if stopped successfully
#
#-----------------------------------------------------------------------
PATH=/bin:/usr/bin:/sbin:/usr/sbin

# Determine RSCT installation location
if [ -d /opt/rsct/bin ]; then
   PATH=$PATH:/opt/rsct/bin
else
   PATH=$PATH:/usr/sbin/rsct/bin
fi

export PATH


#
# Write messages to the system log
#
log()
{
   priority=$1
   shift

   logger -p $priority -t $PROGNAME "$@"
}


LANG=C
export LANG

PROGNAME="$(basename $0)[$$]"
log notice "$LINENO: Entering : $*"

PROGPATH=$(dirname $0)
DB2HADRINSTANCE1=${1?}
DB2HADRINSTANCE2=${2?}
dbname=${3?}
DB2HADRDBNAME=$(echo ${dbname?} | tr "[a-z]" "[A-Z]")
VERBOSE=${4:-noverbose}

log debug "$LINENO: PROGRAME: $PROGNAME,PROGPATH: $PROGPATH,DB2HADRINSTANCE1: $DB2HADRINSTANCE1, DB2HADRINSTANCE2: $DB2HADRINSTANCE2, dbname: $dbname, DB2HADRDBNAME: $DB2HADRDBNAME, VERBOSE: $VERBOSE"

if [[ "$VERBOSE" == "verbose" ]]; then
   typeset -ft $(typeset +f)
   set -x
else
   # Close stdout and stderr
   exec 2> /dev/null
   exec 1> /dev/null
   set +x
fi

CT_MANAGEMENT_SCOPE=2
export CT_MANAGEMENT_SCOPE
CLUSTER_APP_LABEL=db2_${DB2HADRINSTANCE1?}_${DB2HADRINSTANCE2?}_${DB2HADRDBNAME?}

###########################################################
# set_candidate_P_instance()
###########################################################
set_candidate_P_instance()
{
   candidate_P_instance=${DB2HADRINSTANCE1?}
   candidate_S_instance=${DB2HADRINSTANCE2?}
   localShorthost=$(hostname | tr "." " " | awk '{print $1}')

   log debug "$LINENO: candidate_P_instance: $candidate_P_instance, candidate_S_instance: $candidate_S_instance, localShorthost: $localShorthost"

   if [[ ${DB2HADRINSTANCE1?} != ${DB2HADRINSTANCE2?} ]]; then
      if [[ -f ${PROGPATH?}/.${CLUSTER_APP_LABEL?} ]]; then
         candidate_P_instance=$(cat ${PROGPATH?}/.${CLUSTER_APP_LABEL?})
         if [[ ${candidate_P_instance?} == ${DB2HADRINSTANCE1?} ]]; then
            candidate_S_instance=${DB2HADRINSTANCE2?}
         else
            candidate_S_instance=${DB2HADRINSTANCE1?}
         fi
      else
         nn1=$(lsequ -s "Name like '%${DB2HADRINSTANCE1?}#_${localShorthost?}%'" | grep "Resource:Node" | awk '{print $3}' | tr ":" " " | tr "{" " " | tr "}" " " | awk '{print $1}')
         if [[ -z "$nn1" ]] ; then
            nn1=$(lsequ -s "Name like '%${DB2HADRINSTANCE1?}#_0%'" | grep "Resource:Node" | awk '{print $3}' | tr ":" " " | tr "{" " " | tr "}" " " | awk '{print $1}')
         fi

         nn1s=$(echo $nn1 | tr "." " " | awk '{print $1}')
   
         if [[ ${nn1s?} == ${localShorthost?} ]]; then
            candidate_P_instance=${DB2HADRINSTANCE1?}
            candidate_S_instance=${DB2HADRINSTANCE2?}
         else
            candidate_P_instance=${DB2HADRINSTANCE2?}
            candidate_S_instance=${DB2HADRINSTANCE1?}
         fi
         echo ${candidate_P_instance?} > ${PROGPATH?}/.${CLUSTER_APP_LABEL?}
      fi
      log debug "$LINENO: $candidate_P_instance,$candidate_S_instance,$nn1,$nn1s"
   fi

   log debug "$LINENO: candidate_P_instance: $candidate_P_instance, candidate_S_instance: $candidate_S_instance, localShorthost: $localShorthost, nn1: $nn1, nn1s: $nn1s"

   return 0
}


###########################################################
# stophadr()
###########################################################
stophadr()
{
   # Get path to home dir
   DB2INSTANCE=${candidate_P_instance?}
   export DB2INSTANCE
   if [[ -z ${INSTHOME} ]] ; then
      userhome=~${DB2INSTANCE?}
      eval userhome=$userhome
      INSTHOME=${userhome}
   fi

   firstChar=$(echo $INSTHOME | cut -c1-1)
   if [[ -z ${INSTHOME} || "$firstChar" != "/" ]] ; then
      INSTHOME=$(cat /etc/passwd | grep "^${DB2INSTANCE?}:" | cut -f 6 -d \:)
   fi

   if [[ -z ${INSTHOME} ]] ; then
      log error "$LINENO: There is no home directory defined for ${DB2INSTANCE?}"
      rc=0
      return $rc
   fi

   log debug "$LINENO: su - ${candidate_P_instance?} -c $INSTHOME/sqllib/bin/db2gcf -t 3600 -d -i ${candidate_P_instance?} -i ${candidate_S_instance?} -h ${DB2HADRDBNAME?} -L ... "
   su - ${candidate_P_instance?} -c "$INSTHOME/sqllib/bin/db2gcf -t 3600 -d -i ${candidate_P_instance?} -i ${candidate_S_instance?} -h ${DB2HADRDBNAME?} -L"
   rc=$?
   log debug "$LINENO: ...returns $rc"
}

#######################################################
# main()
#######################################################
main()
{
   set_candidate_P_instance
   stophadr
   return $rc
}

if [[ "$VERBOSE" == "verbose" ]]; then
   typeset -ft $(typeset +f)
fi

#######################################################
main "${@:-}"
log notice "$LINENO: Returning $rc : $*"
exit $rc
