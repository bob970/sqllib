#!/bin/ksh
#-----------------------------------------------------------------------
# (C) COPYRIGHT International Business Machines Corp. 2001-2009
# All Rights Reserved
#
# US Government Users Restricted Rights - Use, duplication or
# disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
#
# NAME: mountV115_start.ksh mount_point [ verbose ]
#
# FUNCTION: Start method for filesystem in TSA cluster
# Return 0 if mount point is mounted and accessible 
#          or if GPFS is still recoverying the file system
# Return non-zero otherwise
#
#-----------------------------------------------------------------------
#
# Write messages to the system log and mmfs.log.latest
#
log()
{
    priority=$1
    shift
    logger -p $priority -t $myNameAndPid "$@"

    # mmfs.log.latest must be a link. If it is not, then
    # do not write to it - the link is in process of being recreated.
    if [[ -L /var/adm/ras/mmfs.log.latest ]]; then
        echo "$(date): $myNameAndPid: $@" >> /var/adm/ras/mmfs.log.latest
    fi
}

#-----------------------------------------------------------------------

export PATH=/bin:/usr/bin:/sbin:$PATH
export LANG=C
export CT_MANAGEMENT_SCOPE=2

myname=$(basename $0)
dirname=$(dirname $0)
myNameAndPid="$(basename $0)[$$]"

MP=${1}
VERBOSE=${2:-noverbose}
typeset -i ANOTHER_NODE_VARIEDON=43

UNAME=$(uname)

# environment variable enabled by ISAS 
isVgManaged=$vgManaged

log notice "$LINENO: Entered ($MP)"

if [[ "$VERBOSE" == "verbose" ]]; then
   typeset -ft $(typeset +f)
   set -x
else
   # Close stdout and stderr
   exec 2> /dev/null
   exec 1> /dev/null
   set +x
fi

# Verify input...
if [[ -z $MP ]]; then
   log err "$LINENO: Error: Must specify mount point"
   rc=2
   log err "$LINENO: Returning $rc"
   return $rc
fi

function isMounted
{
   let mounted=0
   if [[ "${UNAME}" == "AIX" ]]; then
      mounted=$(mount | awk '{print $2}' | grep -c "^${MP}\$")
   else
      mounted=$(mount | awk '{print $3}' | grep -c "^${MP}\$")
   fi
   if [[ -z $mounted ]]; then
      mounted=0
   fi
   return $mounted
}

function varyon_VG
{
   varyonvg $1
   rc=$?
   log debug "$LINENO: varyonvg $1 returns $rc"

   if [[ $rc -ne 0 ]]; then
      if [[ $rc -eq $ANOTHER_NODE_VARIEDON ]]; then
         # Need to vary on the VG with the -O option in case multi-node varyon
         # protection is enabled. This option will force the VG to be varied on.
         varyonvg -O $1
         rc=$?
         log debug "$LINENO: varyonvg -O $1 returns $rc"
      else
         varyonvg -b $1
         rc=$?
         log debug "$LINENO: varyonvg -b $1 returns $rc"
      fi
   fi
}

skipSecondMountAttempt=0;

if [[ $UNAME == "AIX" ]]; then
   #AIX
   until [[ -r /etc/filesystems ]]; do 
      log err "$LINENO: Error: /etc/filesystems not readable"
      sleep 1
   done
   blockDevice=$(cat /etc/filesystems | grep -p ^${MP?}: | awk '{print $1 " " $2 " " $3}' | egrep "^dev = " | awk '{print $3}')
   fstype=$(cat /etc/filesystems | grep -p ^${MP?}: | awk '{print $1 " " $2 " " $3}' | egrep "^vfs = " | awk '{print $3}')
   if [[ "$fstype" == "mmfs" ]]; then
      fstype="gpfs"
   fi

   if [[ -z $blockDevice ]]; then
      # This is not a valid mount point, it may be an RLV
      blockDevice=${MP?}
      unset MP
      rlv=true
   fi

   lvname=${blockDevice#/dev/}
   vgname=$(lslv $lvname 2> /dev/null | grep "VOLUME GROUP" | awk '{print $6}')

   # For GPFS we will allow no VG/LV
   if [[ $fstype == "gpfs" ]]; then
      blockDevice="mmfs"
      lvname="mmfs"
      vgname="mmfs"
   else

      if [[ -z "$vgname" ]]; then
         log err "$LINENO: Cannot find input mount point or RLV.  Exiting."
         exit 2
      elif [[ -z $isVgManaged ]]; then
          majNum=$(ls -l /dev/$vgname | tr "," " " | awk '{print $5}')
          diskID=$(lspv | grep " $vgname " | awk '{print $2}')
          hdiskID=$(lspv | grep " $vgname " | awk '{print $1}')
          lvname=${blockDevice#/dev/}
          vgActive=$(lsvg -p ${vgname?} | grep -c "active")
          if [[ $vgActive == 0 ]]; then
             varyon_VG ${vgname} 
          else
             log debug "$LINENO: vg ${vgname} is already active"
          fi
      fi
   fi
elif [[ $UNAME == "Linux" ]] ; then

   blockDevice=$(cat /etc/fstab | awk '{print $1 " " $2}' | grep " ${MP}$" | awk '{print $1}')
   if [[ -z $blockDevice ]]; then
      log err "$LINENO: Mount point $MP not defined at this host"
      exit 2
   fi

   fstype=$(cat /etc/fstab | grep " ${MP} " | awk '{print $3}')

else
   blockDevice=$(cat /etc/fstab | awk '{print $1 " " $2}' | grep " ${MP}$" | awk '{print $1}')
   if [[ -z $blockDevice ]]; then
      if [[ -r /etc/fstab ]]; then
         log err "$LINENO: Mount point $MP not defined at this host"
         exit 2
      fi
   fi
fi

###################################################################
# Start main...
rc=0

# If VG only
if [[ $rlv == "true" ]]; then
   # RLV
   return 0
fi

# If already mounted correctly...
isMounted
if [ ${mounted} -gt 0 ]; then
   log info "$LINENO: $MP is already mounted"
   rm -f ${MP}/.mp_pseudo
   rm -f ${MP}/.mp_monitor*
   touch ${MP}/.mp_monitor 
   rc=$?
   if [ $rc -ne 0 ]; then
      log err "$LINENO: $MP not writeable: $rc"
      if [[ $fstype == "gpfs" ]] ; then
         # GPFS daemon may have been killed
         log warn "$LINENO: $MP GPFS may need to be manually restarted on this host. Ignoring write error rc set to 0"
         rc=0
      fi 
      # monitor will test for write-ability, no need to return a bad rc 
      rc=0
   fi
else
   if [[ $UNAME == "AIX" && $fstype != "gpfs" ]]; then
      vgActive=$(lsvg -p ${vgname?} | grep -c "active")
      if [[ $vgActive == 0 ]]; then
         varyon_VG ${vgname} 
      else
         log debug "$LINENO: vg ${vgname} is already active"
      fi
   fi

   if [[ $fstype == "gpfs" ]] ; then
      gpfsreadyScriptCount=$(ps -ef | grep -v grep | grep -c gpfsready)
      while [[ $gpfsreadyScriptCount -ne 0 ]] ;
      do
         log notice "$LINENO: gpfsready still running, sleeping for 5 seconds..."
         sleep 5
         gpfsreadyScriptCount=$(ps -ef | grep -v grep | grep -c gpfsready)
      done

      gpfsDaemon=$(ps -ef | grep -v grep | grep -c mmfsd)
      if [ $gpfsDaemon -gt 0 ] ; then
         # GPFS has been started, verify if disk is available
         deviceName=${blockDevice#/dev/}
         diskNotFound=$(/usr/lpp/mmfs/bin/mmlsnsd -f $deviceName -m | grep $(hostname -s) | grep -c "not found")

         if [[ $diskNotFound -eq 0 ]] ; then
            # Disk is available, attempt to mount the file system
            log notice "$LINENO: mounting GPFS file system: $MP"
            mount ${MP}
            rc=$?
         else
            # Disk is not available, set rc=1 to indicate file system is not mounted.
            log warn "$LINENO: GPFS disk is unavailable"
            rc=1

            # Loop for 15 minutes with checks every 10 seconds until it does become available.
            #
            # Note: A 15 minute timeout value was chosen here as it lines up with the mount
            #       resource StartCommandTimeout value. If this script is called from the
            #       mountV115_monitor.ksh script it needs to timeout eventually.
            retryCount=0
            while [[ ${retryCount} -lt 90 && $diskNotFound -gt 0 ]] ;
            do
               sleep 10
               diskNotFound=$(/usr/lpp/mmfs/bin/mmlsnsd -f $deviceName -m | grep $(hostname -s) | grep -c "not found")
               retryCount=$((${retryCount}+1))
            done

            if [[ $diskNotFound -eq 0 ]] ; then
               # Disk is now available, mount the file system.
               # Try 6 times with 5 second intervals before giving up.
               # If the file system is not successfully mounted here, it will remain unmounted.
               retryCount=0
               while [[ ${retryCount} -lt 6 && $rc -ne 0 ]] ;
               do
                  log notice "$LINENO: GPFS disk is now available; mounting file system: $MP. Try # ${retryCount}"
                  mount ${MP}
                  rc=$?
                  if [ $rc -ne 0 ]; then
                     sleep 5
                  fi
                  retryCount=$((${retryCount}+1))
               done

               if [ $rc -ne 0 ]; then
                  log warn "$LINENO: Giving up mount attempts on file system: $MP. rc=$rc"
               fi
            fi
         fi
      else
         # GPFS has not been started, set rc=1 to enter GPFS start up logic below
         log notice "$LINENO: GPFS has not been started"
         rc=1
      fi
   else
      # Not a GPFS file system, attempt to mount it
      log notice "$LINENO: mounting file system: $MP"
      mount ${MP}
      rc=$?
   fi

   if [ $rc -eq 0 ] ; then
      # filesystem mounted, check if writable

      rm -f ${MP}/.mp_pseudo
      rm -f ${MP}/.mp_monitor*
      touch ${MP}/.mp_monitor
      rc=$?
      if [ $rc -ne 0 ]; then
         log err "$LINENO: $MP not writeable: $rc"
         # monitor will test for write-ability, no need to return a bad rc 
         rc=0
      fi
   else 
      # file system not mounted
      if [[ $fstype == "gpfs" ]] ; then
            # no indent change to make review simpler
            # Will re-indent this block prior to deliver

            gpfsDaemon=$(ps -ef | grep -v grep | grep -c mmfsd)
            if [ $gpfsDaemon -eq 0 ] ; then
               retryCount=0
               while [[ ${retryCount} -lt 5 && $rc -ne 0 ]] ;
               do
                  log notice "$LINENO: GPFS has not been started; attempting to start it. Try # ${retryCount}"
                  if [ -f "/usr/lpp/mmfs/bin/mmstartup" ] ; then
                     mmstartupOut=$(/usr/lpp/mmfs/bin/mmstartup -N `hostname` 2>&1)
                     rc=$?
                     #sleep for 2 seconds to give GPFS time to start up
                     sleep 2;
                  fi 
               
                  if [ $rc -eq 0 ] ; then
                     # Wait until the daemon says it's active.
                     # mmgetstate reports the 'state' field which we
                     # want to return 'active', it may report 'arbitrating' before mounts
                     # are possible,
                     # sample output is -> mmgetstate
                     #
                     # Node number  Node name        GPFS state 
                     # ------------------------------------------
                     #       2      coralxib11       active
                     #
                     typeset -i waitForActive=12
                     state="$(/usr/lpp/mmfs/bin/mmgetstate | grep $(hostname -s) | awk '{print $3}')"
                     until [[ "$state" == "active" ]]; do
                        log debug "$LINENO: The gpfs daemon is $state - delay 5 seconds and check again"
                        sleep 5

                        (( waitForActive = waitForActive - 1 ))

                        if (( ! waitForActive )); then
                            # GPFS did not become active. Set rc=1 so a retry will occur
                            log err "$LINENO: timeout waiting for GPFS to become active"
                            rc=1 # Set rc so that retry will occur
                            break
                        fi

                        state="$(/usr/lpp/mmfs/bin/mmgetstate | grep $(hostname -s) | awk '{print $3}')"
                    done

                     # If mmstartup is successful do not call mount again as 
                     # the individual mount commands could interfere with the 
                     # GPFS automount attempts
                     log notice "$LINENO: GPFS has been started: $rc"
                     skipSecondMountAttempt=1
                  else
                     # GPFS did not start, do not re-map error
                     log warn "$LINENO: GPFS could not be started: $rc"
                     log warn "$LINENO: mmstartup failed: $mmstartupOut"
                  fi
               
                  if [[ $rc -ne 0 ]] ; then
                     log notice "$LINENO: GPFS was not started, sleeping for 30seconds before retrying"
                     sleep 30
                     retryCount=$((${retryCount}+1))
                  fi
               done
            else
               # GPFS is already running so do not try and start it again. 
               # Also, skip subsequent mount commands so as not to interfere 
               # with the automount daemon
               log notice "$LINENO: GPFS already started"
               skipSecondMountAttempt=1
               rc=0
            fi
      else
         log err "$LINENO: $MP not mountable: $rc"

         #set rc to 1 to try the fsck.
         rc=1

         # Safely fsck the disk
         #
         # As per Galileo defect wsdbu00909753, the retry loop is put in place
         # because fsck has to have exclusive access to a device and there are
         # times when exclusive access to device may not be available.  For
         # example, when starting aixmibd daemon or running varyonvg around
         # the same time as running this script. The recommended action is
         # to retry fsck.  Since there is already a built-in limit of 18
         # minutes in running this script, we will simply retry infinitely
         # here.
         #
         fsckRetryCount=1
         while [[ $rc -ne 0 ]]; do

            log notice "$LINENO: Attempt $fsckRetryCount: Will fsck ${blockDevice?}"
            if [[ $UNAME == "AIX" && -z $isVgManaged ]]; then
               fsck -f -p ${blockDevice?}
               rc=$?
            else
               fsck -p ${blockDevice?}
               rc=$?
            fi
            log notice "$LINENO: Attempt $fsckRetryCount: fsck ${blockDevice?} returns $rc"

            if [ $rc -ne 0 ]; then
               sleep 1 ;
            fi

            fsckRetryCount=`expr $fsckRetryCount + 1`
         done

         if [[ $UNAME == "AIX" && -z $isVgManaged ]]; then
            varyon_VG ${vgname} 
            rc=0
         fi
      fi

      # try the mount a second time
      if [[ $rc -eq 0 && $skipSecondMountAttempt == 0 ]] ; then
         mount ${MP}
         rc=$?
         if [[ $fstype != "gpfs" ]]; then
            while [[ $rc -ne 0 ]];
            do 
               log err "$LINENO: $MP not mountable: mount attempt fails: $rc"
               sleep 1
               mount ${MP}
               rc=$?
            done
         fi

         if [ $rc -eq 0 ] ; then
            log notice "$LINENO: $MP mountable: mount succeeds: $rc"
            rm -f ${MP}/.mp_pseudo
            rm -f ${MP}/.mp_monitor*
            touch ${MP}/.mp_monitor
            rc=$?
            if [ $rc -ne 0 ]; then
               log err "$LINENO: $MP not writeable: $rc"
               # monitor will test for write-ability, no need to return a bad rc 
               rc=0
            fi
         else
            if [[ $fstype == "gpfs" ]] ; then
         
               # if GPFS is still returning 16, we need TSA to retry this mount 
               if [[ $rc -eq 16 || $rc -eq 1 ]] ; then 
                  log notice "$LINENO: $MP is still being recovered by GPFS: $rc"
                  rc=0
               else
                  log notice "$LINENO: $MP is has failed a second mount attempt, Sleep then reset rc to 0: $rc"
                  sleep 5 
                  rc=0
               fi
            fi
         fi
      fi
   fi
fi

log notice "$LINENO: Returning $rc for $MP"
return $rc
