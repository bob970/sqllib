#!/bin/ksh -p
#-----------------------------------------------------------------------
# (C) COPYRIGHT International Business Machines Corp. 2001-2009
# All Rights Reserved
#
# US Government Users Restricted Rights - Use, duplication or
# disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
#
# NAME: db2V115_monitor.ksh db2instance [NN] [verbose]
#
# (%W%) %E%  %U%
#
# FUNCTION: Monitor method for DB2 in TSA HA cluster
#
# Return 1 if specified DB2 instance is online
# Return 2 otherwise
#
#-----------------------------------------------------------------------
PATH=/bin:/usr/bin:/sbin:/usr/sbin

# Determine RSCT installation location
if [ -d /opt/rsct/bin ]; then
   PATH=$PATH:/opt/rsct/bin
else
   PATH=$PATH:/usr/sbin/rsct/bin
fi

export PATH
export LANG=C

export CT_MANAGEMENT_SCOPE=2

PROGNAME="$(basename $0)[$$]"
DB2INSTANCE=${1}
NN=${2:-0}
VERBOSE=${3:-noverbose}

MAX_RETRIES=5

if [[ "$VERBOSE" == "verbose" ]]; then
   :
   exec  2> /dev/null
else
   # Close stdout and stderr
   exec  2> /dev/null
   exec  1> /dev/null
   set   +x
fi

###################################################################
# Functions ...
#
function log
{
    priority=$1
    shift
    logger -p $priority -t ${PROGNAME} "$@"
}

###################################################################
# Start main error checking...

if [[ -z $DB2INSTANCE ]]; then
   log err "$LINENO: Error: Must specify instance name"
   echo "rc=2"
   return 2
fi

if [[ -z ${INSTHOME} ]] ; then
   userhome=~${DB2INSTANCE?}
   eval userhome=$userhome
   INSTHOME=${userhome}
fi

firstChar=$(echo $INSTHOME | cut -c1-1)
if [[ -z ${INSTHOME} || "$firstChar" != "/" ]] ; then
   INSTHOME=$(cat /etc/passwd | grep "^${DB2INSTANCE?}:" | cut -f 6 -d \:)
fi

if [[ -z ${INSTHOME} ]] ; then
   # Let's still make an attempt to detect the state via the ps command
   log warn "$LINENO: There is no home directory defined for ${DB2INSTANCE?}"
else
   INSTHOME=${INSTHOME#*=}
   INSTHOME=${INSTHOME%%*( )}
fi

###################################################################
# Start main...

# Comment following to not use ps
bypassHomeDir=TRUE
if [[ -z $bypassHomeDir && ! -z ${INSTHOME} ]]; then
   cd ${INSTHOME?}/sqllib
   cd /
   if [[ -x $INSTHOME/sqllib/bin/db2gcf ]]; then
      su - ${DB2INSTANCE} -c "$INSTHOME/sqllib/bin/db2gcf -s -p ${NN?} -i ${DB2INSTANCE?} > /dev/null 2> /dev/null"
      rc=$?
   else
      p_pid=$(su - ${DB2INSTANCE?} -c "db2nps ${NN?}" | grep -c db2sys)
      if [[ $p_pid == 0 ]]; then
         rc=1
      else
         rc=0
      fi
   fi
else
   # If home dir not accessible, use plain old ps to monitor db2sysc
   retryCount=0
   retry_ps=1
   while [[ $retry_ps -eq 1 && $retryCount -lt $MAX_RETRIES ]];
   do
      retry_ps=0
      p_out=$(ps -u ${DB2INSTANCE?} -o args | egrep "^db2sysc ${NN?}[ ]*$|^db2sysc[ ]*$|^\[db2sysc\]"| grep -v defunct)

      p_pid=$(echo $p_out | grep -c "db2sysc ${NN?}")
      if [[ $p_pid == 0 && $NN -eq 0 ]]; then
         p_pid=$(echo $p_out | grep -v "\[db2sysc\]" | grep -c "db2sysc")
      fi

      if [[ $p_pid == 0 ]]; then
         p_pid=$(echo $p_out | grep -c "\[db2sysc\]")
         if [[ $p_pid != 0 ]]; then
            log warn "$LINENO: ps returns [db2sysc], retrying ps: retryCount=$retryCount out of $MAX_RETRIES"
            retryCount=$((${retryCount}+1))
            retry_ps=1
            sleep 5
         fi
         rc=1
      else
         rc=0
      fi

   done

   if [[ $retryCount -eq $MAX_RETRIES ]]; then
      # Report Unknown state to TSA
      log err "$LINENO: ps returns [db2sysc]: returning 0"
      echo "rc=0"
      return 0
   fi
fi

# Logic for rc is 0 if db2 is ok, non-zero otherwise
# Change logic to conform to cluster's expectations...
if [[ $rc == 0 ]]; then
   # Report Online state to TSA
   cluster_rc=1
else
   # Report Offline state to TSA
   cluster_rc=2
fi

# Allow DPF cleanup if required
if [[ $cluster_rc == 2 ]]; then
   p_pid=$(ps -e -o args | grep -c "^db2wdog [0-9]\+ [0-9]\+")
   if [[ $p_pid > 0 ]]; then
      DB2_CLEANUP_TIME=30
      log notice "$LINENO: DB2 cleaning up: sleep $DB2_CLEANUP_TIME seconds ($DB2INSTANCE, $NN)"
      sleep $DB2_CLEANUP_TIME
   fi
fi

log debug "$LINENO: Returning $cluster_rc ($DB2INSTANCE, $NN)"
echo "rc=$cluster_rc"
return $cluster_rc
