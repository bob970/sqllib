#!/bin/ksh -p
#-----------------------------------------------------------------------
# (C) COPYRIGHT International Business Machines Corp. 2001-2009
# All Rights Reserved
#
# US Government Users Restricted Rights - Use, duplication or
# disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
#
# NAME: db2V115_start.ksh db2instance [NN] [verbose]
#
# (%W%) %E%  %U%
#
# FUNCTION: Start method for DB2 in TSA HA cluster
#
# Return 0 if successful
# Return non-zero value otherwise
#
#-----------------------------------------------------------------------

# Determine RSCT installation location
typeset RSCT="/opt/rsct"
if [ ! -d $RSCT/bin ]; then
   RSCT="/usr/sbin/rsct"
fi

PATH=/bin:/usr/bin:/sbin:/usr/sbin:$RSCT/bin

export PATH
export LANG=C

export CT_MANAGEMENT_SCOPE=2

DB2INSTANCE=${1}
NN=${2:-0}
VERBOSE=${3:-noverbose}

PROGNAME="$(basename $0)[$$]"
PROGPATH=$(dirname $0)

TAGDIR=/db2home/${DB2INSTANCE}/ha_setup/tags
ROVING_STANDBY_ENABLED=false
MAX_RETRIES=10

if [[ "$VERBOSE" == "verbose" ]]; then
   typeset -ft $(typeset +f)
   set -x
else
   # Close stdout and stderr
   exec 2> /dev/null
   exec 1> /dev/null
   set +x
fi

###################################################################
# Functions ...
#
function log
{
    priority=$1
    shift
    logger -p $priority -t ${PROGNAME} "$@"
}

function startPartitionOnThisNode
{
   # copy SSD temp tags
   if [ -d ${TAGDIR} ]; then
      for tag in $(ls ${TAGDIR}/*.${NN}); do
         tempfile=$(print $tag | awk -F. '{print $2}' | tr '_' '/')
         dd bs=512 if=${tag} of=${tempfile} count=1 seek=1 conv=notrunc
      done
   fi
   if [[ -x $INSTHOME/sqllib/bin/db2gcf ]]; then
      ((sato=nln*2+60))
      su - ${DB2INSTANCE} -c "$INSTHOME/sqllib/bin/db2gcf -t $sato -u -p ${NN?} -i ${DB2INSTANCE?} -L"
      rcg=$?

   else
      log err "$LINENO: Cannot find executable $INSTHOME/sqllib/bin/db2gcf"
      sleep 30
      rcg=1
   fi
}

function runRemoteHADRMonitor
{
   monitorCmd=$(lsrsrc -s "Name like 'db2#_%${DB2INSTANCE?}%#_${db?}-rs'" IBM.Application MonitorCommand \
                | grep MonitorCommand | uniq | awk '{print $3 " " $4 " " $5 " " $6}' | tr -d '"')

   if [[ ! -z $monitorCmd ]]; then
      # Retrieve the two host names
      hostname_list=$(lsrsrc -s "Name like 'db2#_%${DB2INSTANCE?}%#_${db?}-rs' && ResourceType=1" IBM.Application NodeNameList \
                    | grep NodeNameList | awk '{print $3}' | tr -d '{}"')
      hostname1=$(echo $hostname_list | awk 'BEGIN{FS=",";}{print $1}' | tr "." " " | awk '{print $1}')
      hostname2=$(echo $hostname_list | awk 'BEGIN{FS=",";}{print $2}' | tr "." " " | awk '{print $1}')

      if [[ "$hostname1" == "$localShorthost" ]]; then
         reint_file_exists=$(rcecmd -n $hostname2 "ls /tmp/db2_* | grep db2_${db?}_${localShorthost?}_Reintegrate_ | grep -c ${DB2INSTANCE?}")

         # Reintegration flag exists on remote host, run remote HADR monitor command to re-create it as an IBM.Test resource
         if [[ $reint_file_exists -gt 0 ]]; then 
            log notice "$LINENO: Running HADR monitor command for database $db on host $hostname2"
            rcecmd -n $hostname2 "$monitorCmd"
         else
            log debug "$LINENO: Remote HADR monitor command run is not required for database $db"
         fi
      elif [[ "$hostname2" == "$localShorthost" ]]; then
         reint_file_exists=$(rcecmd -n $hostname1 "ls /tmp/db2_* | grep db2_${db?}_${localShorthost?}_Reintegrate_ | grep -c ${DB2INSTANCE?}")

         # Reintegration flag exists on remote host, run remote HADR monitor command to re-create it as an IBM.Test resource
         if [[ $reint_file_exists -gt 0 ]]; then 
            log notice "$LINENO: Running HADR monitor command for database $db on host $hostname1"
            rcecmd -n $hostname1 "$monitorCmd"
         else
            log debug "$LINENO: Remote HADR monitor command run is not required for database $db"
         fi
      else
         # this should not happen, but log the fact that it did.
         log err "$LINENO: The HADR resource NodeNameList for database $db does not include this host"
         reint_file_exists=0
      fi

      # Reintegration flag file exists on remote host, ensure that it gets re-created as an IBM.Test resource
      # before exiting out of this function
      if [[ $reint_file_exists -gt 0 ]]; then
         count=0
         while [ $count -lt $MAX_RETRIES ];
         do
            # Loop until IBM.Test reintegration resource is created
            nn=$(lsrsrc -s "Name like 'db2#_${db?}#_${localShorthost?}#_Reintegrate#_%#_${DB2INSTANCE?}' OR Name like 'db2#_${db?}#_${localShorthost?}#_Reintegrate#_${DB2INSTANCE?}#_%'" IBM.Test Name | grep -c Name)
    
            if [[ $nn -gt 0 ]] ; then
               # IBM.Test reintegration resource has been created
               break # out of loop
            else
               # sleep and retry
               log debug "$LINENO: The IBM.Test reintegration resource does not exist for database $db, sleep and retry - count=$count"
               (( count += 1 ))
               sleep 1 
            fi
         done

         if [[ $count -eq $MAX_RETRIES ]] ; then
            log err "$LINENO: The IBM.Test reintegration resource for database $db failed to be created after $MAX_RETRIES seconds."
         else
            log info "$LINENO: The IBM.Test reintegration resource for database $db was created after $count seconds."
         fi
      fi
   fi 
}

function activateDatabase
{
   # Verify whether or not reintegration flag exists
   nn=$(lsrsrc -s "Name like 'db2#_${db?}#_${localShorthost?}#_Reintegrate#_%#_${DB2INSTANCE?}' OR Name like 'db2#_${db?}#_${localShorthost?}#_Reintegrate#_${DB2INSTANCE?}#_%'" IBM.Test Name | grep -c Name)

   # Reintegrate standby database
   if [[ $nn -gt 0 ]]; then
      log debug "$LINENO: Forcing apps off before reintegration"
      su - ${DB2INSTANCE?} -c "/bin/ksh $RSCT/sapolicies/db2/forceAllApps $DB2INSTANCE $db"
      log debug "$LINENO: After forcing, starting to reintegrate"
      su - ${DB2INSTANCE?} -c "db2 start hadr on db ${db?} as standby"
      rc=$? 
      sleep 1

      if [[ $rc -eq 0 ]]; then
         # Delete reintegration flag since database has successfully reintegrated
         rmrsrc -s "Name like 'db2#_${db?}#_${localShorthost?}#_Reintegrate#_%#_${DB2INSTANCE?}' OR Name like 'db2#_${db?}#_${localShorthost?}#_Reintegrate#_${DB2INSTANCE?}#_%'" IBM.Test
      else
         # Log warning for the database since reintegration failed
         log warn "$LINENO: $DB2INSTANCE, $db: HADR database was not successfully reintegrated as standby."
      fi 
   fi

   # Activate database
   if [[ $nn -eq 0 ]]; then
      deact=1
      while [ $deact -eq 1 ];
      do
         log info "$LINENO: su - ${DB2INSTANCE?} -c db2 activate database ${db?}"
         su - ${DB2INSTANCE?} -c "db2 activate database ${db?}" &
         sleep 5
         deact=$(su - ${DB2INSTANCE?} -c "db2pd -db ${db?}" | grep "not activated" | wc -l)
      done
   fi
}

function resolveHomeDir
{
   if [[ -z ${INSTHOME} ]] ; then
      userhome=~${DB2INSTANCE?}
      eval userhome=$userhome
      INSTHOME=${userhome}
   fi

   firstChar=$(echo $INSTHOME | cut -c1-1)
   if [[ -z ${INSTHOME} || "$firstChar" != "/" ]] ; then
      INSTHOME=$(cat /etc/passwd | grep "^${DB2INSTANCE?}:" | cut -f 6 -d \:)
   fi

   if [[ -z ${INSTHOME} ]] ; then
      log warn "$LINENO: There is no home directory defined for ${DB2INSTANCE?}"
   else
      INSTHOME=${INSTHOME#*=}
      INSTHOME=${INSTHOME%%*( )}
   fi
}

###################################################################
# Start main error checking...

# Do not start up instance if tmp file exists
if [[ -r /tmp/do_not_start.${DB2INSTANCE?}.${NN?} ]]; then
   log notice "$LINENO: Bypassing startup for $DB2INSTANCE,$NN..."
   return 2
fi

# Verify inputs ...
if [[ -z $DB2INSTANCE ]]; then
   log err "$LINENO: Error: Must specify instance name"
   return 2
fi

###################################################################
# Start main...
log notice "$LINENO: Entered $0, $DB2INSTANCE, $NN"

# Ensure that we can resolve the home directory and that sqllib is accessible
while :
do
   resolveHomeDir
   if [[ ! -z ${INSTHOME} ]] ; then
      # Ensure sqllib is accessible
      while :
      do
         if [[ ! -d ${INSTHOME?}/sqllib ]]; then
            log warn "$LINENO: Cannot access $INSTHOME/sqllib"
            cd ${INSTHOME?}/sqllib
            cd /
            sleep 10
         else
            break # out of inner loop
         fi
      done

      break # out of outer loop
   else
      # Can't resolve home directory... sleep and retry.
      sleep 10
   fi
done

# If single partition, clean IPCs first
DB2NODES_CFG=${INSTHOME?}/sqllib/db2nodes.cfg
if test -r $DB2NODES_CFG ; then
   nln=$(grep -v "^[ ]*$" $DB2NODES_CFG | wc -l | awk '{print $1}')
else
   nln=1
fi

log debug "$LINENO: Able to cd to $INSTHOME/sqllib : $0, $DB2INSTANCE, $NN"

$PROGPATH/db2V115_monitor.ksh $DB2INSTANCE $NN
if [ $? -eq 1 ]; then
   log info "$LINENO: $0 is already up... "
else

log debug "$LINENO: $nln partitions total: $0, $DB2INSTANCE, $NN"
if [ $nln -eq 1 ]; then
   kill -9 $(ps -o pid,comm -u $DB2INSTANCE | grep "db2[a-z]" | egrep -v "db2haicu|db2gcf" | grep -v \.ks | awk '{print $1}') 2> /dev/null
   su - ${DB2INSTANCE} -c "ipclean -a"
fi

# Need to start this partition...
rc=2
rcg=2
while [ $rc -ne 1 -o $rcg -ne 0 ]; do
   
   # Setup $rcg
   startPartitionOnThisNode
   
   $PROGPATH/db2V115_monitor.ksh $DB2INSTANCE $NN
   rc=$? 
   if [ $rc -ne 1 -o $rcg -ne 0 ]; then
      log notice "$LINENO: *************** Attempted to start partition $DB2INSTANCE,$NN and failed - rc=$rc, rcg=$rcg"

      log notice "$LINENO: Killing db2 processes : $DB2INSTANCE, $NN"
      kill -9 $(ps -o pid,comm -u $DB2INSTANCE | grep "db2[a-z]" | egrep -v "db2haicu|db2gcf" | awk '{print $1}') 2> /dev/null

      nip=$(ps -fu $DB2INSTANCE | grep -c "ipclean" )
      if [[ $nip -eq 0 ]] ; then
         log notice "$LINENO: Removing IPCs"
         su - ${DB2INSTANCE} -c "ipclean -a"
      fi

      typeset -Z4 nnnn
      nnnn=$NN
      rm -f $INSTHOME/sqllib/ctrl/db2stst.${nnnn?}
      # Will retry start ... sleep for a bit to prevent pegging CPU
      sleep 30
   fi
done
fi
rc=0
log info "$LINENO: Partition was successfully started."

# Partition has started. Run the rovingV115_failover.ksh script if
# roving standby is enabled
if [[ $ROVING_STANDBY_ENABLED = "true" ]]; then
   $PROGPATH/rovingV115_failover.ksh $DB2INSTANCE $NN
fi

localShorthost=$(hostname | tr "." " " | awk '{print $1}')

# In BigSQL, this RG always exists with this name:
bigSqlFlag=$(lsrg | grep "db2_${DB2INSTANCE}_${DB2INSTANCE}_BIGSQL-rg" | wc -l)

# Activate HADR databases here
if [ $nln -eq 1 -o $bigSqlFlag -eq 1 ]; then
   dbFN=/tmp/dblist.${DB2INSTANCE?}.${NN?}.out
   touch ${dbFN?}
   chmod 777 ${dbFN?}
   # exclude the remote catalog databases
   su - ${DB2INSTANCE?} -c "/bin/ksh LANG=en_US db2 list db directory" | awk 'BEGIN{FS="=";} \
   {if($1 ~ /Database name/)tmp=$2;else if(($1 ~ /Directory entry type/) && ($2 !~ /Remote/)) \
   print tmp}' > ${dbFN?}
   while read db; do
      :
      # run HADR monitor script on other node (if required) to ensure Reintegration resource
      # is created before we check for it
      runRemoteHADRMonitor
      # comment following line to not restart or activate database at instance start time
      activateDatabase
   done < ${dbFN?}
   rm -f ${dbFN?}

   # Ensure instance is still up
   np=$(ps -fu $DB2INSTANCE | grep -c "db2sysc" )

   if [[ $np -eq 0 ]]; then
      log err "$LINENO: $0, $DB2INSTANCE: db2sysc process is not found. Failing start: $rc"
      rc=1
   else
      rc=0
   fi
fi

if [[ $rc -ne 0 ]]; then
    log err "$LINENO: $0, $DB2INSTANCE, $NN: If the resource is in \"Failed Offline\" state, run \"resetrsrc\" to recover."
fi

log notice "$LINENO: Returning $rc from $0 ( $DB2INSTANCE, $NN)"
return $rc
