#!/bin/ksh
#-----------------------------------------------------------------------
# (C) COPYRIGHT International Business Machines Corp. 2011
# All Rights Reserved
#
# US Government Users Restricted Rights - Use, duplication or
# disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
#
# NAME: condrespV115_resp.ksh
#
# FUNCTION: Response script for processing CFS adapter down/up events
#
#-----------------------------------------------------------------------
export LANG=C

#-----------------------------------------------------------------------
#
# Write messages to the system log
#
log()
{
    priority=$1
    shift
    logger -p $priority -t $myNameAndPid "$eventType $@"

    # mmfs.log.latest must be a link. If it is not, then
    # do not write to it - the link is in process of being recreated.
    if [[ -L /var/adm/ras/mmfs.log.latest ]]; then
        echo "$(date): $myNameAndPid: $eventType $@" >> /var/adm/ras/mmfs.log.latest
    fi

    if [[ "$priority" == "err" ]]; then
        logger -p $priority -t $myNameAndPid "$LINENO: 'db2cluster -list -alert' and 'db2cluster -clear -alert' may be required"
        exit -1 # This will produce an error record in lsaudrec output.
    fi
}

#-----------------------------------------------------------------------
#
# If two or more instances of this script are running
# then it is likely an adapter down event performing mmshutdown / mmexpel 
# is in progress. Wait for the others to complete. 
# This will wait for previously running scripts to complete
# so that the order of execution is maintained
# This will also verify the adapter state is as expected after each sleep.
#
waitForOthers()
{
    typeset -i stableAdapterCount=0
    typeset -i previousAdapterState=-1
    typeset -i stableSleepTime=180
    typeset -i sleepTime=5

   # Get the initial state of the adapter
    getAdapterState
    previousAdapterState=$adapterState
    if (( expectedState != adapterState )); then
        log err "$LINENO: adapter state is not as expected '$expectedState', actual '$adapterState' - exiting"
        # No return
    fi

    # Delay 10 seconds in the adapter up code path to ensure the
    # adapter down event runs first in near simultaneous events.
    #
    # There is still a timing hole: if multiple adapter up events
    # occur at the same time, then they will all sleep 10 and then
    # wait for each other. However, if this occurs, the first ones will
    # eventually timeout (240 seconds) And the last one will then
    # process the event.

    if (( 1 == ${ERRM_VALUE} )); then
        log notice "$LINENO: Delay 10 seconds"
        sleep 10
    fi

    # Pre initialization
    runCount=0

    # Wait for the previous scripts to complete.
    if [[ -n $waitPids ]]; then
        log notice "$LINENO: Wait for pid(s) $waitPids to complete"

        while (( waitCount < maxRetries )); do
            # Get the number of previously started response scripts still running
            runCount=$(ps -fp $waitPids | grep -cw $myName)

            if (( ! runCount )); then
                break
            fi

            # Log at 30 second intervals
            if (( (( runCount )) && ! (( waitCount % 30 )) )); then
                log debug "$LINENO: There is still $runCount other response script(s) running"
            fi

            sleep 5
            (( waitCount += 5 ))

            # Check adapter state after sleep
            getAdapterState
            if (( expectedState != adapterState )); then
                log err "$LINENO: adapter state is not as expected after sleep: expected '$expectedState', actual '$adapterState', waitCount 'waitCount' - exiting"
                # No return

                # When rebooting a host, there is a tendency for the condresp to
                # be called twice, once with adapter up and the other with
                # adapter down. The condresp processes will often block each
                # other for 4 minutes until the offline condresp will timeout
                # and offline the GPFS regardless of the adapter state. During
                # SVT testing it has often been observed that a member will
                # successfully start in under 4 minutes, only to be killed by
                # the condresp timing out. The following will allow the condresp
                # to exit peacefully if the adapter has stayed in the online
                # state for 180 seconds.

                if (( (( expectedState == ADAPTER_OFFLINE )) && (( adapterState == ADAPTER_ONLINE )) && (( stableAdapterCount >= stableSleepTime )) )); then
                    log notice "$LINENO: Adapter has been detected to be online and stable for $stableSleepTime seconds - exiting..."
                    exit 0
                fi
            fi

            if (( previousAdapterState != adapterState )); then
                (( stableAdapterCount = 0 ))
            else
                (( stableAdapterCount += sleepTime ))
            fi
            previousAdapterState=$adapterState
        done
    fi

    if (( runCount )); then
        log err "$LINENO: $runCount response scripts are still running after $maxRetries seconds."
        # No return, exit -1 and produce an error record in lsaudrec
    elif (( waitCount )); then
        log notice "$LINENO: wait for others to exit took $waitCount seconds"
    fi
}

#-----------------------------------------------------------------------
#
# It is possible that a bouncing adapter can cause multiple events to
# be triggered.  When it is their turn to run, callers wll get the
# adapter state and abort the operation if the state is not as
# expected.
#
# The adapter state is obtained by getting the selection string and
# resource class from IBM.Condition.  Then the OpState of the adapter
# is obtained and returned to the caller in $adapterState.
#
# This assumes exactly one condrespV115 condition exists on this host.
# If the condition does not exist, a warning is logged and the state
# is set to UNKNOWN.
#
# During restart, the following OpStates must be checked for online (1):
# - IBM.ConfigRM and IBM.ERRM are running
# - IBM.PeerDomain       - Peer domain is online
# - IBM.PeerNode         - This node is online in the domain
# - IBM.NetworkInterface - The adapter is online
#
# globals so we only have to check once
typeset -i daemonsRunning=0
typeset -i domainOnline=0
typeset -i nodeOnline=0

# Set to PEER domain scope
export CT_MANAGEMENT_SCOPE=2

getAdapterState()
{
    typeset class=""
    typeset selectString=""
    typeset -i tryAgain=60 # Maximum wait 5 minutes
    typeset -i sleepTime=5 # checking every 5 seconds
    typeset    output=""
    typeset    previousOutput=""

    while (( tryAgain )); do
        # Verify ConfigRM is running
        output="$(lssrc -a | egrep "^ IBM.ConfigRM" | grep -v grep)"
        configRmActive=$(echo "$output" | grep -c active)

        # It is possible ERRM in went idle and shutdown
        # This will make ERRM active if not already active
        lsrsrc IBM.Condition > /dev/null

        #Verify ERRM is running
        output="$(lssrc -a | egrep "^ IBM.ERRM" | grep -v grep)"
        errmActive=$(echo "$output" | grep -c active)
        if (( 1 == configRmActive && 1 == errmActive)); then
            daemonsRunning=1
        fi  

        if (( daemonsRunning && ! domainOnline )); then
            # There can be multiple domains defined but only 1 can be active.
            # Look for any resource in IBM.PeerDomain that contains 'OpState = 1'
            # Note that this breaks if lsrsrc output format changes.
            output="$(lsrsrc -Ab IBM.PeerDomain OpState)"
            #log debug "IBM.PeerDomain: $output"
            domainOnline=$(echo "$output" | grep -c "OpState = 1")
        fi

        if (( domainOnline && ! nodeOnline )); then
            # There will be exactly 1 entry in IBM.PeerNode for this host.
            # Get the OpState for it and set nodeOnline.
            # Note that this breaks if lsrsrc output format changes.
            output="$(lsrsrc -Ab -s "Name == '$myHostname'" IBM.PeerNode OpState)"
            #log debug "IBM.PeerNode: $output"
            nodeOnline=$(echo "$output" | grep -c "OpState = 1")
        fi

        if (( nodeOnline )); then

            # start with an invalid adapter state
            adapterState=ADAPTER_UNKNOWN
            foundOneCondResp=0

            #
            # Now we can reliably get the adapter state
            # Get the select string and resource class from the condition
            # log debug "lsrsrc ... IBM.Condition"
            #
            # The IFS line is important below so that the for loop will tokenize
            # the output by newline only, not by the default space.  The second
            # IFS line looks odd, but don't attempt to replace it with IFS='\n',
            # it won't work.
            #
            IFS_orig=$IFS
            IFS='
'
            for line in $( lsrsrc -s "Name like 'condrespV115%' AND NodeNameList = {'$myHostname'}" IBM.Condition | egrep "SelectionString|ResourceClass" | tr '"' ' ' ) ; do

                #
                # The output for each line looked as follow :
                #
                #    ResourceClass               =  IBM.NetworkInterface
                #    SelectionString             =  IPAddress == '9.26.120.224'
                #
                # The format is simply <tag> = <val>.  We will now tokenized
                # this with " = ", and assign the the first token as <tag>,
                # and the second token as <val>.  In the process, trim the
                # beginning and trailing white space.
                #
                tag=$( echo $line | awk -F" = " '{print $1}' | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//' )
                val=$( echo $line | awk -F" = " '{print $2}' | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//' )

                if [[ "ResourceClass" == "$tag" ]]; then
                    class=$val
                fi
                if [[ "SelectionString" == "$tag" ]]; then
                    selectString=$val
                fi

                # There could be multiple condition/responses for the host,
                # for example, one for the private network interface (defined 
                # in GPFS with the subnet) and one for the public network
                # interface (the one associated with the hostname)
                # So, go through each of the adapters with a condition response
                # associated with it

                # If we have both the selectString and the class defined in this
                # iteration then
                if [[ -n $selectString && -n $class ]]; then
                    log notice "$LINENO: IBM.Condition: \"$selectString\" \"$class\""
              
                    # set a flag to indicate at least one cond/resp was 
                    # found for the host
                    foundOneCondResp=1

                    # Read the OpState for the adapter associated with the
                    # current  condition response
                    output="$(lsrsrc -s "$selectString" $class OpState )"
                    RES=$?
                    #log debug "lsrsrc response '$output'"
                
                    adapterState=$(echo "$output" | awk '/OpState/{print $3}')

                    # RSCT should only report online or offline.
                    if (( adapterState == ADAPTER_ONLINE  ||
                          adapterState == ADAPTER_OFFLINE ));
                    then
                        # The adapter state was successfully read from configRM
                        # If it is found offline, then we do not need to continue
                        # looping, otherwise keep going to see if there 
                        # is another cond/resp and adapter that may be offline
                        if (( adapterState == ADAPTER_OFFLINE ));
                        then
                             log notice "$LINENO: Adapter associated with $selectString $class is OFFLINE - $adapterState"
                             break
                        else
                             log notice "$LINENO: Adapter associated with $selectString $class is ONLINE - $adapterState"
                        fi
                    else
                        # something went wrong - it is neither online nor offline
                        log notice "$LINENO: lsrsrc -s \"$selectString\" $class OpState returned an invalid OpState == '$adapterState' - RES=$RES"
                        if [[ "$previousOutput" != "$output" ]]; then
                            # log lsrsrc output anytime it changes from the previous round
                            log notice "$LINENO: lsrsrc response '$output'"
                            previousOutput="$output"
                        else
                            # Set adapter state to a bogus value so we sleep and try again.
                            adapterState=ADAPTER_UNKNOWN
                            log notice "$LINENO: Unable to get adapter information from IBM.Condition"
                        fi
                    fi
                    
                    # reset these for the next one
                    class=""
                    selectString=""
                fi # [[ -n $selectString && -n $class ]]
            done
            IFS=$IFS_orig
           
            # if we were able to determine whether all adapters are online
            # or at least one adapter is offline, we can exit the loop now
            # otherwise keep going until we exit the loop due to a timeout
            if (( adapterState == ADAPTER_ONLINE  ||
                  adapterState == ADAPTER_OFFLINE ));
            then
                log notice "$LINENO: Successfully determined adapter state - $adapterState"
                break
            else
                if (( remoteMode && !foundOneCondResp )); 
                then
                    # This host is in the cluster but has no condresp resource
                    # which means this is most likely a tiebreaker host.
                    # Fake online so the mmexpel can be performed
                    log debug "$LINENO: No condresp resource. Set adapterState = ADAPTER_ONLINE"
                    (( adapterState = ADAPTER_ONLINE ))
                else 
                    log notice "$LINENO: Unable to determine adapter state - $remoteMode - $foundOneCondResp - $adapterState"
                fi
            fi
        else
            # This will be hit in the case where RSCT is just starting up (reboot)
            # This should not affect the critical time of adapter down as the RSCT will be up.
            # We will sleep and try again below.
            log notice "$LINENO: RSCT is not ready to process requests. Sleep $sleepTime seconds before retrying again $tryAgain more times. daemonsRunning:$daemonsRunning, domainOnline:$domainOnline, nodeOnline:$nodeOnline "
        fi

        # Unable to get a valid OpState or
        # RSCT is starting up
        # Let's sleep and try again.
        sleep $sleepTime
        (( tryAgain = tryAgain - 1 ))
        ((tryAgain < 0)) && ((tryAgain = 0))
    done

    if (( ! tryAgain )); then
        log err "$LINENO: Unable to get the adapter state - daemonsRunning:$daemonsRunning, domainOnline:$domainOnline, nodeOnline:$nodeOnline - exiting"
        # No deposit no return.
    fi

    if (( adapterState != ADAPTER_ONLINE  &&
          adapterState != ADAPTER_OFFLINE ));
    then
        log err "$LINENO: The state of the adapter is neither online or offline: $adapterState - daemonsRunning:$daemonsRunning, domainOnline:$domainOnline, nodeOnline:$nodeOnline - exiting"
        # No deposit no return.
    fi

    if (( ! remoteMode )); then
        log debug "$LINENO: lsrsrc -s \"$selectString\" $class returned Opstate == $adapterState"
    fi
}

#-----------------------------------------------------------------------
#
# In certain instances, RSCT is invoking the response script
# without setting the ERRM_VALUE. Check the state of the
# adapter and set ERRM_VALUE accordingly
#
setERRM_VALUE()
{
    # This is likely a reboot and it is going to take a long time
    # sleep 30 seconds before doing anyting
    log debug "$LINENO: Wait 60 seconds to give RSCT time to start daemons"
    sleep 60
    getAdapterState
    ERRM_VALUE=$adapterState
}

#-----------------------------------------------------------------------
#
# Tell the other nodes in the cluster to expel this node from the cluster
#
expelMeFromCluster()
{
    typeset -i retries=12 # retry for a maximum of 12 attempts for each host
    typeset -i attempt=1
    typeset -i expelled=0
    typeset -i e_noconnect=50
    typeset -i e_nolink=126 # the link has been severed
    typeset -i rc=0

    typeset hostList=""
    typeset newHostList=""
    typeset result=""
    typeset cmd=""
    typeset host=""
    typeset mgr=""
    typeset notMgr=""

    # Use the list of hosts returned from RSCT.
    for rceHost in $( lsrpnode -d | grep Online | cut -d: -f1 ) ; do
        if [[ -n $rceHost && "$rceHost" != "$myHostname" ]]; then
            hostList="$hostList $rceHost"
        fi
    done

    #
    # Build a list of hosts that are not supposed to be cluster managers.
    # They will be skipped on the first round of expel
    # It is possible that there are hosts in this list but not the in the
    # hostList created above.
    #
    for line in $( /usr/lpp/mmfs/bin/mmsdrquery sdrq_node_info sdrq_daemon_interface:sdrq_manager_node norefresh | cut -d ':' -f7,8 ) ; do

        host=$( echo $line | awk -F':' '{print $1}' )
        mgr=$( echo $line | awk -F':' '{print $2}' )

        if [[ "false" == "$mgr" ]]; then
            host=$(echo $host | cut -d '.' -f1)
            notMgr="$notMgr $host"
        fi

    done

    # Now loop through the list of hosts, requesting each to attempt 
    # mmexpelnode until one successfully expels me.
    while (( retries && ! expelled )); do
        newHostList=""
        for rceHost in $hostList; do

            # If this is not a callback from mmshutdown and
            # if the forced offline file has been removed, 
            # then an adapter up event must have occurred.
            # stop trying to expel the host from the cluster.
            if [[ "$eventType" != "gpfsShutdown" &&  ! -e $FORCED_OFFLINE_FILE ]]; then
                log notice "$LINENO: $FORCED_OFFLINE_FILE has been removed while trying to get expelled."
                expelled=1
                break ;
            fi

            # On the first round, skip hosts that are not intended to be cluster manager.
            # Sending an mmexpelnode request to such a host can result in that host becoming the manager.
            if (( attempt == 1 )); then
                if (( $( echo $notMgr | grep -wc $rceHost ) )); then
                    log debug "$LINENO: Do not attempt mmexpelnode on $rceHost"
                    # Make sure the host can be used on subesequent rounds.
                    newHostList="$newHostList $rceHost"
                    continue
                fi
            fi
                
            # RCE timeout is set to 75 seconds per recommendation from
            # the GPFS team.  If the node being expelled was the
            # cluster manager, then there is an election for a new
            # manager in progress.  If mmexpelnode gets killed too
            # soon, the end result could be longer failover time.
            typeset -i TIMEOUT=75
            typeset -i REMOTE_CMD=${REQUEST_EXPEL}

            # Run the RCE command
            log notice "$LINENO: Request mmexpelnode on $rceHost - attempt $attempt"
            result="$($RSCT/bin/rcecmd -t $TIMEOUT -n $rceHost $RSCT/sapolicies/db2/condrespV115_resp.ksh run_mmexpelnode $rceHost $REMOTE_CMD $myHostname $attempt )"

            # Parse the result which should be mmexpelnode:$myHostname:0
            # Remove anything up to but not including mmexpelnode
            cmd="$(echo $result | cut -d ':' -f 1 | sed -e 's/.*mmexpelnode/mmexpelnode/')"

            if [[ -n $cmd ]]; then
                host=$(echo $result | cut -d ':' -f 2)
                rc=$(echo $result | cut -d ':' -f 3)
            else
                # This happens if rce times out, result=""
                # set some values to prevent crash later
                cmd="noCommand"
                host="noHost"
                rc=999 # Arbitrary value != e_noconnect
            fi

            if [[ "$cmd" == "mmexpelnode" ]]; then
                if [[ "$host" == "$myHostname" ]]; then
                    if (( ! rc )); then
                        log notice "$LINENO: mmexpelnode success on host $rceHost during attempt $attempt"
                        expelled=1
                        break ;
                    else
                        log warn "$LINENO: mmexpelnode on host '$rceHost' failed with $rc"
                    fi
                else
                    log warn "$LINENO: incorrect host in result: '$host'"
                fi
            else
                if [[ -z $result ]]; then
                    log warn "$LINENO: no result from rce command on '$rceHost' - probably rce timeout"
                else
                    log warn "$LINENO: Result from $rceHost incorrect: '$result'"
                fi
            fi

            # If a host fails with rc == e_noconnect,
            # then it has lost connectivity or mmfsd is not running.
            if (( rc != e_noconnect )); then
                # Put this host back in for the next round of attempts
                newHostList="$newHostList $rceHost"
            else
                # Received e_noconnect, do not try mmexpelnode on that host anymore.
                log warn "$LINENO: $rceHost returned $rc for mmexpelnode - removing from hostList"
            fi
        done

        if (( ! expelled )); then
            # Setup the host list for the next round
            hostList="$newHostList"
            if [[ -z $hostList ]]; then
                # mmexpelnode is the last operation and is best effort.
                # There are no hosts to try so log an error and exit now.
                log err "$LINENO: No hosts left to attempt mmexpelnode"
                # No return, exit -1 and produce an error record in lsaudrec
            fi

            sleep 5
            ((retries -= 1))
            ((attempt += 1))
        fi
    done
}

#-----------------------------------------------------------------------
#
checkIfDaemonIsUp()
{
    # Get the current state of the GPFS subsystem
    # mmgestate -Y can hang for a long period of time. This can result
    # in the adaptor offline hanging, adapter online occurs and completes
    # without starting GPFS. Then the adaptor offline will complete
    # and create the forced offline file. The only remedy is to 
    # manually remove the forced offline file and restart GPFS.

    # mmgetstate <no options> will work, however, it may not report accurately.

    # The fix is to look to see if the mmfsd is running.
    # We don't care about it's state, just that if it exists.

    if [[ "Linux" == "$OS" ]]; then
        # ps -ef can hang for an extended period
        # Minimize this risk on Linux by only looking for mmfsd command.
        isup=$(ps -C mmfsd | grep -c mmfsd)
        # Not checking rc as 0 for some and 1 for none is returned
    else
        # Unfortuantely, no option to speicfy command name to ps on AIX
        isup="$(ps -ef | grep mmfsd | grep -v grep | wc -l)"
        rc=$?

        if (( rc )); then
            log err "$LINENO: ps failed: rc=$rc - Unable to process event"
            # No return, exit -1 and produce an error record in lsaudrec
        fi
    fi
}

#-----------------------------------------------------------------------
#
startGpfsDaemon()
{
    while (( startRetry < maxRetries )); do
        (( startRetry += 1 ))
        log notice "$LINENO: issue mmstartup -N $myHostname - attempt $startRetry"
        ${CFSBIN}/mmstartup -N $myHostname
        rc=$?
        if (( ! rc )); then
            sleep 2 # Wait while mmfs.log.latest link is changed
            log notice "$LINENO: mmstartup returned success"
            break
        else
            log warn "$LINENO: mmstartup returned rc=$rc on attempt $startRetry"
            sleep 1

            # check on the state of the adapter. If it goes down while waiting
            # then exit so the adapter down can kill the daemon.
            getAdapterState
            if (( expectedState != adapterState )); then
                log err "$LINENO: The adapter went down while waiting - exiting"
                # No return
            fi
            # Check to see if the daemon is still running
        fi
    done

    if (( startRetry >= maxRetries )); then
        log warn "$LINENO: Manual intervention required to restart CFS"
        log warn "$LINENO: NOT removing $FORCED_OFFLINE_FILE"
        log err "$LINENO: mmstartup failed $maxRetries times"
        # No return, exit -1 and produce an error record in lsaudrec
    fi
}

#-----------------------------------------------------------------------
#
# strong quorum used to always be optained on entry.
# But it was never checked on the remote node performing mmexpelnode.
# And it creates problems for adapter down events (it can hang and GPFS lease expiration occurs)
# This has changed such that strong quorum is only obtained if this is an adapter up event.
#
getStrongQuorum()
{
    # Details of the runact -c IBM.PeerDomain VerifyStrongQuorumState action return codes
    #
    #  0 - The local node is online in a peer domain which supports the verification action,
    #      and the domain currently has majority quorum (HAS_QUORUM).
    #  1 - The local node is not online in a peer domain.
    #  2 - The RSCT active version level of the cluster does not support the VerifyQuorumState action.
    # 10 - The local node is online in a peer domain which supports the VerifyQuorumState action,
    #      but is in subset of the cluster which does not have a cluster quorum of members (NO_QUORUM).
    # 11 - The local node is online in a peer domain which supports the VerifyQuorumState action,
    #      but is in subset of the cluster which is pending a quorum change  (PEND_QUORUM).
    # 12 - The local node is online in a peer domain which supports the VerifyQuorumState action,
    #      but encountered a protocol error while processing the action. The action should be retried.

    strongQuorum=$(runact -c IBM.PeerDomain VerifyStrongQuorumState 2>&1 | awk '/ResultCode/{print $3}')

    if [[ -z $strongQuorum ]]; then
        log notice "$LINENO: quorum not detected - setting to 1"
        strongQuorum=1
    fi

    log debug "$LINENO: strongQuorum=$strongQuorum"
}

#-----------------------------------------------------------------------
#
# main() entry for CFS response script
#
{
    # Use VNNN to prevent invoking any commands prior to logging script entry
    typeset myNameAndPid="condrespVNNN_resp.ksh[$$]"
    typeset eventType="event:"

    typeset -i ADAPTER_ONLINE=1
    typeset -i ADAPTER_OFFLINE=2
    typeset -i ADAPTER_UNKNOWN=99
    typeset -i REQUEST_EXPEL=99
    typeset -i GPFS_SHUTDOWN=98

    # Log script entry prior to invoking any other commands so that there
    # is a record of when the script was first invoked.
    if (( 5 == $# )); then
        # RSCT strips options (like -v) before invoking the command.
        # Additionally, RSCT may fail if this syntax is used:
        # rcecmd -t 10 -n hostname ENV_VAR_1=1 ENV_VAR_2=2 command
        # So, if the response script is invoked with 5 args, and the first is 'run_mmexpelnode',
        # then it was a remote host requesting this host to invoke mmexpelnode ${EXITING_NODE}
        eventType="CFS remote_mmexpelnode:"
        log debug "$LINENO: script entry $@"
        if [[ "run_mmexpelnode" == "$1" ]]; then
            ERRM_NODE_NAME=$2 # Name of the host invoking mmexpelnode
            ERRM_VALUE=$3     # should be ${REQUEST_EXPEL} for the case statement in the main code path
            EXITING_NODE=$4   # target of the mmexpelnode
            ATTEMPT=$5        # How many times an attempt has been made on this node
        fi
    elif [[ "gpfs_shutdown" == "$1" ]]; then
        eventType=gpfsShutdown
        ERRM_NODE_NAME="NO_NODE_NAME"
        ERRM_VALUE=${GPFS_SHUTDOWN}
        log debug "$LINENO: script entry $@"
    else
        if   (( ERRM_VALUE == ADAPTER_ONLINE )); then
            eventType="CFS Adapter up:"
        elif (( ERRM_VALUE == ADAPTER_OFFLINE )); then
            eventType="CFS Adapter down:"
        else
            eventType="UNKNOWN EVENT:" # This should never happen
        fi
        log debug "$LINENO: script entry '$ERRM_NODE_NAME' '$ERRM_VALUE'"
    fi

    typeset OS="$(uname)"
    typeset CFSBIN="/usr/lpp/mmfs/bin"
    typeset RSCT="/opt/rsct"
    typeset myName="$(basename $0)"
    typeset myPath="$(dirname $0)"
    typeset myHostname="noHostname"
    typeset waitPids=""

    # Determine if RSCT installation is NOT in the new location
    if [ ! -d $RSCT/bin ]; then
        RSCT="/usr/sbin/rsct"
    fi

    typeset -i isup=0
    typeset -i pendingActive=0
    typeset -i startRetry=0
    typeset -i waitCount=0
    typeset -i maxRetries=240   # 4 minute timeout
    typeset -i runCount=0       # Not counting $$
    typeset -i adapterState=999 # Set to uninitialzed value
    typeset -i expectedState=99 # Set to uninitialzed value
    typeset -i remoteMode=0     # Set to 1 when requested to perform mmexpel 

    typeset -i REMOTE_ERROR_DAEMON_DOWN=97
    typeset -i REMOTE_ERROR_ADAPTER_DOWN=98
    typeset -i REMOTE_ERROR_FORCED_OFFLINE=99

    myNameAndPid="$(basename $0)[$$]"

    # Get the hostname
    myHostname=$(hostname -s 2>&1)
    rc=$?
    if (( rc != 0 )); then
        log warn "$LINENO: hostname -s returned $rc - '$myHostname' - is the current hostname in /etc/hosts?"
        if [[ -n ${ERRM_NODE_NAME} ]]; then
            # If the hostname from RMC contains a domain, remove it.
            log warn "$LINENO: Using the hostname provided by RSCT"
            myHostname="$(echo ${ERRM_NODE_NAME} | cut -d '.' -f 1)"
        fi
    fi

    log warn "$LINENO: Using hostname $myHostname"

    typeset FORCED_OFFLINE_FILE="/var/db2/db2_gpfs_forced_offline_$myHostname"

    # Get the list of PIDs which are already running when this script started
    if [[ "Linux" == "$OS" ]]; then
        # ps -ef can hang for an extended period
        # Minimize this risk on Linux by only looking for $myName. $myPath is used to exclude masqueraders
        # See wsdbu00952095 for reasons to ignore the local invocation. (i.e.
        # why we need "grep -v "$myhostname ${REQUEST_EXPEL}" below.
        waitPids=$(ps -C $myName h | grep -v $$ | grep $myPath | grep -v grep | grep -v "$myHostname ${REQUEST_EXPEL}" | awk '{ print $1}' | tr '\n' ',' | sed -s 's/,$//')
    else
        # Unfortunately, no option to specify command name to ps on AIX        
        waitPids=$(ps -ef | grep $myName | grep -v $$ | grep $myPath | grep -v grep | grep -v "$myHostname ${REQUEST_EXPEL}" | awk '{ print $2}' | tr '\n' ',' | sed -e 's/,$//')
    fi

    #log debug "waitPids set to '$waitPids'"

    # gpfs_shutdown will not set ERRM_NODE_NAME
    if [[ "NO_NODE_NAME" == "${ERRM_NODE_NAME}" ]]; then
        ERRM_NODE_NAME=${myHostname}
    else
        # If the hostname from RMC contains a domain, remove it.
        errmHostname="$(echo ${ERRM_NODE_NAME} | cut -d '.' -f 1)"
        if [[ "$myHostname"  != "$errmHostname" ]]; then
            log notice "$LINENO: Name mismatch: myHostname=$myHostname, ERRM_NODE_NAME=${ERRM_NODE_NAME}"
        fi
    fi

    if [[ -z ${ERRM_VALUE} ]]; then
        log debug "$LINENO: Error: ERRM_VALUE is not set: determine correct value"
        setERRM_VALUE
    fi

    case ${ERRM_VALUE} in
 
        #================ case online:
 
       (${ADAPTER_ONLINE})

        # Wait for any other invocations to complete.  This includes a
        # minimum 10 second wait and will also verify that RSCT is
        # completely initialized and the adapter is really online,
        # preventing simutaneous up/down race 
        expectedState=ADAPTER_ONLINE
        waitForOthers

        if (( ADAPTER_ONLINE != adapterState )); then
            log err "$LINENO: adapter state changed from online to offline - exiting"
            # No return
        fi

        # Do nothing if the forced offline file does not exist.
        if [[ -e $FORCED_OFFLINE_FILE ]]; then
            # The GPS daemon was stopped by an adapter down event
            # Check quorum state != 1 means all is good
            getStrongQuorum
            if (( strongQuorum != 1 )); then
                # Check if the GPFS daemon is running, if not then start it
                checkIfDaemonIsUp # Sets isup to number of mmfsd processes
                
                if (( isup )); then
                    # Further verify that the daemon state reports 'active' via mmgetstate.
                    CFS_STATE="$(${CFSBIN}/mmgetstate | grep $myHostname | awk '{print $3}')"
                    if [[ "active" != "$CFS_STATE" ]]; then
                        log notice "$LINENO: CFS daemon is up, but not active - daemon state = $CFS_STATE"
                        isup=0
                        pendingActive=1
                    fi
                fi

                if (( ! isup )); then
                    if (( ! pendingActive )); then
                        startGpfsDaemon
                    fi

                    # Even after the daemon is up, it may not be ready to mount filesystems.
                    # Continue looping until we've exceeded maxRetries, or the daemon says
                    # it's ready to mount.  mmgetstate reports the 'state' field which we
                    # want to return 'active', it may report 'arbitrating' before mounts
                    # are possible, sample output is:
                    # mmgetstate::HEADER:version:reserved:reserved:nodeName:nodeNumber:state:quorum:nodesUp:totalNodes:remarks:
                    # mmgetstate::0:1:::coral18.torolab.ibm.com:7:active:1*:2:4::
                    # Intentionally continue where the previous loop left off, we want
                    # to limit total retries of the entire operation to a sane limit
                    while (( startRetry < maxRetries )); do
                        (( startRetry += 1 ))

                        # mmgetstate -Y goes offnode - use mgetstate instead
                        CFS_STATE="$(${CFSBIN}/mmgetstate | grep $myHostname | awk '{print $3}')"
                        if [[ "active" == "$CFS_STATE" ]]; then
                            log notice "$LINENO: mmgetstate shows daemon is active on attempt $startRetry"
                            break
                        else
                            log debug "$LINENO: mmgetstate shows daemon state=$CFS_STATE on attempt $startRetry"
                            sleep 1

                            # check on the state of the adapter. If it goes down while waiting
                            # then exit so the adapter down can kill the daemon.
                            getAdapterState
                            if (( expectedState != adapterState )); then
                                log err "$LINENO: The adapter went down while waiting - exiting"
                                # No return
                            fi

                            # Verify that the daemon is still running.
                            # If it is not, then restart it.
                            checkIfDaemonIsUp # Sets isup to number of mmfsd processes
                            if (( ! isup )); then
                                log notice "$LINENO: The daemon stopped - restarting it"
                                startGpfsDaemon
                            fi
                        fi
                    done

                    if (( startRetry >= maxRetries )); then
                        log warn "$LINENO: CFS daemon started, but not ready to mount"
                        # We've logged the warning condition, remove the forced-offline to
                        # let the mount monitors return offline so TSA can restart them
                    fi
                else
                    # This should not occur, but log the fact that it did.
                    log notice "$LINENO: CFS deamons are already running"
                fi
            else
                log notice "$LINENO: Not part of a peer domain, do nothing."
            fi
            log notice "$LINENO: rm -f $FORCED_OFFLINE_FILE"
            rm -f $FORCED_OFFLINE_FILE
        else
            # This case means that the GPFS daemons were not stopped by an adapter down event.
            # Assume the admin wishes to keep GPFS down and do not start the daemons
            # They must be started by some other action.
            log notice "$LINENO: $FORCED_OFFLINE_FILE does not exist. Not restarting CFS daemons"
        fi
        break
        ;;

        #================ case offline:

        (${ADAPTER_OFFLINE})
        expectedState=ADAPTER_OFFLINE
        waitForOthers

        # No need to check strong quorum on adapter down
 
        # Check if the GPFS daemon is running. if it is then stop it
        checkIfDaemonIsUp # Sets isup to number of mmfsd processes
        if (( isup )); then
            # The adapter went down, create the forced offline file and stop the GPFS daemons
            log notice "$LINENO: touch $FORCED_OFFLINE_FILE"
            touch $FORCED_OFFLINE_FILE
            log notice "$LINENO: issue ${CFSBIN}/mmshutdown --force"
            output="$(${CFSBIN}/mmshutdown --force 2>&1)"
            rc=$?
            if (( ! rc )); then
                log notice "$LINENO: mmshutdown returned success"
                expelMeFromCluster
            else
                log notice "$LINENO: mmshutdown returned rc=$rc"
                log err "$LINENO: output: '$output'"
                # No return, exit -1 and produce an error record in lsaudrec
                # The forced offline file will be left in place so lssam reports failed offline
                # The admin may have to run db2cluster -list -alert and db2cluster -clear -alert
            fi
        else
            log notice "$LINENO: The CFS daemon is not running"
        fi
        break
        ;;

        #================ case expel:

        (${REQUEST_EXPEL}) # issue mmexpelnode $EXITING_NODE

        # For tiebreaker nodes in GDPC cluster, adapters are not monitored
        # so there is no condresp resource on the node.
        # However, they can be asked to perform mmexpel by another node 
        # We still need to invoke getAdapterState to verify the the daemons are up.
        # And in the case of a tiebreaker, it is possible that ERRM has been stopped.
        # Set remote node so that getAdapterState will restart ERRM and return online
        remoteMode=1

        if [[ ! -e $FORCED_OFFLINE_FILE ]]; then
            getAdapterState
            if (( ADAPTER_ONLINE == adapterState )); then
                checkIfDaemonIsUp # Sets isup to number of mmfsd processes
                if (( isup )); then
                    # all is good - honor the request and expel the node
                    log debug "$LINENO: mmexpelnode -o -f -N $EXITING_NODE"
                    ${CFSBIN}/mmexpelnode -o -f -N $EXITING_NODE
                    rc=$?
                    if (( rc )); then
                        log warn "$LINENO: mmexpelnode -o -f -N $EXITING_NODE failed with $rc"
                    else
                        log notice "$LINENO: mmexpelnode -o -f -N $EXITING_NODE was successful"
                    fi
                else
                    rc=${REMOTE_ERROR_DAEMON_DOWN} # Daemon not running, return an error so another node is tried
                    log notice "$LINENO: CFS daemon is not running"
                fi
            else
                # This will catch a race where the adapter is in the process of failing
                rc=${REMOTE_ERROR_ADAPTER_DOWN} # adapter down, return an error so another node is tried
                log warn "$LINENO: adapter down - this node is failing"
                # No return
            fi

        else
            rc=${REMOTE_ERROR_FORCED_OFFLINE} # Forced offline file exists, return an error so another node is tried
            log notice "$LINENO: $FORCED_OFFLINE_FILE exists, this node is failing"
        fi

        # Send the result back to the caller
        log notice "$LINENO: response 'mmexpelnode:$EXITING_NODE:$rc'"
        echo "mmexpelnode:$EXITING_NODE:$rc"
        break
        ;;

        #================ case mmshutdown callback

        (${GPFS_SHUTDOWN})

        # If the forced offline file exists, then this callback will
        # likely be coming from mmshutdown invoked by condrespV115_resp.ksh
        # during an adapter down event. The adapter down processing
        # will then perform the expel so do not do this one.

        if [[ ! -e $FORCED_OFFLINE_FILE ]]; then
            expelMeFromCluster
        else
            log debug "$LINENO: $FORCED_OFFLINE_FILE exists - expel is being deferred"
        fi
        break
        ;;

        #================ Unknown ERRM_VALUE

        (*) # something else
        log err "$LINENO: Unknown ERRM_VALUE: ${ERRM_VALUE}"
        # No return, exit -1 and produce an error record in lsaudrec 
        break
        ;;
    esac

    exit 0
}
