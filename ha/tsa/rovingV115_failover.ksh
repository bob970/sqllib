#!/bin/ksh -p
#-----------------------------------------------------------------------
# (C) COPYRIGHT International Business Machines Corp. 2001-2009
# All Rights Reserved
#
# US Government Users Restricted Rights - Use, duplication or
# disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
#
# NAME: rovingV115_failover db2instance [NN] [verbose]
#
# (%W%) %E%  %U%
#
# FUNCTION: Update the idle node
#
# Pre-reqs: can only be called from db2V115_start.ksh
#-----------------------------------------------------------------------
PATH=/bin:/usr/bin:/sbin:/usr/sbin

# Determine RSCT installation location
if [ -d /opt/rsct/bin ]; then
   PATH=$PATH:/opt/rsct/bin
else
   PATH=$PATH:/usr/sbin/rsct/bin
fi

export PATH
export LANG=C

export CT_MANAGEMENT_SCOPE=2

DB2INSTANCE=${1}
NN=${2:-0}
VERBOSE=${3:-noverbose}
typeset myNameAndPid="$(basename $0)[$$]" 

PROGNAME=$(basename $0)
PROGPATH=$(dirname $0)


if [[ -z ${INSTHOME} ]] ; then
   userhome=~${DB2INSTANCE?}
   eval userhome=$userhome
   INSTHOME=${userhome}
fi

firstChar=$(echo $INSTHOME | cut -c1-1)
if [[ -z ${INSTHOME} || "$firstChar" != "/" ]] ; then
   INSTHOME=$(cat /etc/passwd | grep "^${DB2INSTANCE?}:" | cut -f 6 -d \:)
fi

if [[ -z ${INSTHOME} ]] ; then
   logger -i -p err -t $PROGNAME "Error: There is no home directory defined for ${DB2INSTANCE?}"
   exit 1
fi


if [[ "$VERBOSE" == "verbose" ]]; then
   typeset -ft $(typeset +f)
   set -x
else
   # Close stdout and stderr
   exec 2> /dev/null
   exec 1> /dev/null
   set +x
fi

logger -p info -t $myNameAndPid "Entering $0 ($DB2INSTANCE, $NN)"

#######
set -x
thisHN=$(hostname)
thisRSName=db2_${DB2INSTANCE?}_${NN?}-rs
thisRGName=db2_${DB2INSTANCE?}_${NN?}-rg
thisRGEquivName=db2_${DB2INSTANCE?}_${NN?}-rg_group-equ

lsrsrc -s "Name like '${thisRSName?}' AND ResourceType=1" IBM.Application NodeNameList | grep NodeNameList | read NNL
echo $NNL | tr -d '{}\"' | awk '{print $3}' | tr "," " " | read currentActive currentIdle
currentActive=$(echo $currentActive | tr "." " " | awk '{print $1}')
currentIdle=$(echo $currentIdle | tr "." " " | awk '{print $1}')
thisHN=$(echo $thisHN | tr "." " " | awk '{print $1}')


echo "thisHN=$thisHN"
echo "currentActive=$currentActive"
echo "currentIdle=$currentIdle"

if [[ $thisHN == $currentActive ]] ; then
   :
   exit 1
fi

#change the resources that just failed over
echo "change FAILED resources: NNL list from/to"
echo "$currentActive,$thisHN"
echo "$thisHN,$currentActive"

# change IBM.Application resources
chrsrc -s "NodeNameList IN {'${currentActive}'} AND NodeNameList IN {'${thisHN}'}" \
        IBM.Application NodeNameList="{'${thisHN}','${currentActive}'}"

# change IBM.ServiceIP resources
chrsrc -s "NodeNameList IN {'${currentActive}'} AND NodeNameList IN {'${thisHN}'}" \
        IBM.ServiceIP NodeNameList="{'${thisHN}','${currentActive}'}"

lsrpnode -x | sort -n | awk '{print  $1}' > /tmp/cluster_nodelist
# Read temp file and construct variable
let indexi=0
while read myline; do
        candidateHost=$(echo $myline | tr "." " " | awk '{print $1}')

        # if this isn't the failover and isn't the node that just failed
        if [[ $candidateHost != $currentActive && $candidateHost != $thisHN ]]; then
                echo "change OTHER resources: NNL list from/to"
                echo "$candidateHost,$currentIdle"
                echo "$candidateHost,$currentActive"
		              # change IBM.Application resources
                chrsrc -s "NodeNameList IN {'${candidateHost}'} AND NodeNameList IN {'${currentIdle?}'}" \
                        IBM.Application NodeNameList="{'${candidateHost}','${currentActive}'}"
              		# change IBM.ServiceIP resources
                chrsrc -s "NodeNameList IN {'${candidateHost}'} AND NodeNameList IN {'${currentIdle?}'}" \
                        IBM.ServiceIP NodeNameList="{'${candidateHost}','${currentActive}'}"
        fi
        let indexi+=1
done < /tmp/cluster_nodelist
rm /tmp/cluster_nodelist


DB2NODES_CFG=${INSTHOME?}/sqllib/db2nodes.cfg
if [[ ! -r $DB2NODES_CFG ]]; then
   DB2NODES_CFG=/tmp/db2nodes.cfg
fi

if test -r $DB2NODES_CFG ; then
   let nodes_cfg_line_no=0
   while read line; do
      db2nodes_array[nodes_cfg_line_no]=$(echo $line)
      let nodes_cfg_line_no+=1
   done < $DB2NODES_CFG
else
   echo "Cannot read your db2nodes.cfg!"
   exit 1
fi

let nodes_cfg_line_no=0
while [ $nodes_cfg_line_no -lt ${#db2nodes_array[*]} ];
do
        line=$(echo ${db2nodes_array[$nodes_cfg_line_no]})
        NODENUM=$(echo $line | awk '{print $1}')
        NODENAME=$(echo $line | awk '{print $2}' | tr "\." " " | awk '{print $1}')
        PORT=$(echo $line | awk '{print $3}')
        NETNAME=$(echo $line | awk '{print $4}')

        RGGroupEquivName=db2_${DB2INSTANCE?}_${NODENUM?}-rg_group-equ

        lsequ -e ${RGGroupEquivName} | grep "Resource:Node" | awk '{print $3}' | \
                tr -s '[:punct:]' '[ *]'  | read name1a name1b name2a name2b


        # check if this is the right HA group
        if [[ ${name2a} == ${currentIdle} ]]; then

                # check if this is a rg that just failed over
                if [[ ${name1a} == ${currentActive} ]]; then
                        echo "failed RG: chequ: $RGGroupEquivName - $name2a:$name2b,$name1a:$name1b"
                        chequ -u r ${RGGroupEquivName?} IBM.PeerNode:${name2a}:${name2b},${name1a}:${name1b}
                else
                        echo "other RG: chequ: $RGGroupEquivName - $name1a:$name1b,$currentActive:$currentActive"
                        chequ -u r ${RGGroupEquivName} IBM.PeerNode:${name1a}:${name1b},${currentActive}:${currentActive}
                fi
        fi

        let nodes_cfg_line_no+=1
done

logger -p notice -t $myNameAndPid "Returning $rc from $0 ($DB2INSTANCE, $NN)"
return $rc
