#!/bin/ksh -p
#------------------------------------------------------------------------
# (C) COPYRIGHT International Business Machines Corp. 2001-2009
# All Rights Reserved
#
# US Government Users Restricted Rights - Use, duplication or
# disclosure restricted by GSA ADP Schedule Contract with IBM Corp.
#
# FUNCTION: Monitor method for ROS VIP address in a TSA HADR cluster
#
# INPUT:  hadrV115_monitor_shadow.ksh db2instp db2insts hadrdb
#
# OUTPUT: 1 if online, 2 if offline
#
#------------------------------------------------------------------------

VIPhere=2

su - ${1?} -c "db2pd -hadr -db ${3?}" | egrep 'HADR_ROLE' | awk '{print $3}' | uniq | read Role
su - ${1?} -c "db2pd -hadr -db ${3?}" | egrep 'HADR_STATE' | awk '{print $3}' | uniq | read State

# ROS VIP is up when Standby is in Peer state
if [[ X${Role} == 'XSTANDBY' && X${State} == 'XPEER' ]]; then
   VIPhere=1
fi

# ROS VIP is also up when Standby is in Disconnected Peer state
if [[ X${Role} == 'XSTANDBY' && X${State} == 'XDISCONNECTED_PEER' ]]; then
   VIPhere=1
fi

# ROS VIP is also up when Standby is in Remote Catchup state
if [[ X${Role} == 'XSTANDBY' && X${State} == 'XREMOTE_CATCHUP' ]]; then
   VIPhere=1
fi

# ROS VIP is also up when Standby is in Remote Catchup Pending state
if [[ X${Role} == 'XSTANDBY' && X${State} == 'XREMOTE_CATCHUP_PENDING' ]]; then
   VIPhere=1
fi

logger -i -p notice -t $0 "$1,$2,$3: Role=$Role, State=$State, Returning $VIPhere"
return $VIPhere

